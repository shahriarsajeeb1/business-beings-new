import bcrypt from 'bcryptjs'

const userData =[
    {
    name:"Ramasamy",
    mobile:9688047266,
    password: bcrypt.hashSync('123456', 10),
    pincode:641677,
    plan:'PR03',
    image:'https://i0.wp.com/www.theresaglennphotography.com/wp-content/uploads/2018/09/022.jpg',
    admin:true,

},
{
    name:"Ram",
    mobile:9688047288,
    password: bcrypt.hashSync('123456', 10),
    pincode:641677,
    plan:'VI03',
    image:'https://i0.wp.com/www.theresaglennphotography.com/wp-content/uploads/2018/09/022.jpg',
    admin:false,

}
]


export default userData;
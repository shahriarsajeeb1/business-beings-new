const streamData =[
    {
    title:'How To Sell on Facebook',
    videos:[
        {
            id:'1',
            thumbnail:'http://i3.ytimg.com/vi/VkhZrZmlaK0/maxresdefault.jpg',                  
        },
        {
            id:'2',
            thumbnail:'http://i3.ytimg.com/vi/VkhZrZmlaK0/maxresdefault.jpg',                  
        },
        {
            id:'3',
            thumbnail:'http://i3.ytimg.com/vi/VkhZrZmlaK0/maxresdefault.jpg',                  
        }
    ],
    description:'It is really easy to sell your items or products in Facebook Marketplace and you don\'t need to spend a dime to sell on Facebook. We planned to continue this as a Facebook selling series so that you can clearly understand selling on Facebook is easy and budget friendly.',
    likes:267,
    type:'Ideas',
    budget:'0-25k',
    category:'Online Business',
    interest:'Farming',
    thumbnail:'http://i3.ytimg.com/vi/VkhZrZmlaK0/maxresdefault.jpg',
},
{
    title:'How To Sell on Amazon',
    videos:[
        {
            id:'1',
            thumbnail:'http://i3.ytimg.com/vi/MsvIAVIyuQ4/maxresdefault.jpg',                  
        },
        {
            id:'2',
            thumbnail:'http://i3.ytimg.com/vi/MsvIAVIyuQ4/maxresdefault.jpg',                  
        },
        {
            id:'3',
            thumbnail:'http://i3.ytimg.com/vi/MsvIAVIyuQ4/maxresdefault.jpg',                  
        }
    ],
    description:'It is really easy to sell your items or products in Facebook Marketplace and you don\'t need to spend a dime to sell on Facebook. We planned to continue this as a Facebook selling series so that you can clearly understand selling on Facebook is easy and budget friendly.',
    likes:367,
    type:'Interviews',
    budget:'50k-100k',
    category:'Manufacturing',
    interest:'Logistics',
    thumbnail:'http://i3.ytimg.com/vi/MsvIAVIyuQ4/maxresdefault.jpg',
},
{
    title:'How To Sell on Twitter',
    videos:[
        {
            id:'1',
            thumbnail:'http://i3.ytimg.com/vi/ujZwS35FYdA/maxresdefault.jpg',                  
        },
        {
            id:'2',
            thumbnail:'http://i3.ytimg.com/vi/ujZwS35FYdA/maxresdefault.jpg',                  
        },
        {
            id:'3',
            thumbnail:'http://i3.ytimg.com/vi/ujZwS35FYdA/maxresdefault.jpg',                  
        }
    ],
    description:'It is really easy to sell your items or products in Facebook Marketplace and you don\'t need to spend a dime to sell on Facebook. We planned to continue this as a Facebook selling series so that you can clearly understand selling on Facebook is easy and budget friendly.',
    likes:467,
    type:'Interviews',
    budget:'100k-500k',
    category:'Homemade',
    interest:'Pharmaceutical',
    thumbnail:'http://i3.ytimg.com/vi/ujZwS35FYdA/maxresdefault.jpg',
},
{
    title:'How To Sell on LinkedIn',
    videos:[
        {
            id:'1',
            thumbnail:'http://i3.ytimg.com/vi/VkhZrZmlaK0/maxresdefault.jpg',                  
        },
        {
            id:'2',
            thumbnail:'http://i3.ytimg.com/vi/VkhZrZmlaK0/maxresdefault.jpg',                  
        },
        {
            id:'3',
            thumbnail:'http://i3.ytimg.com/vi/VkhZrZmlaK0/maxresdefault.jpg',                  
        }
    ],
    description:'It is really easy to sell your items or products in Facebook Marketplace and you don\'t need to spend a dime to sell on Facebook. We planned to continue this as a Facebook selling series so that you can clearly understand selling on Facebook is easy and budget friendly.',
    likes:567,
    type:'Case',
    budget:'Above 1000k',
    category:'Franchise',
    interest:'Electrical',
    thumbnail:'http://i3.ytimg.com/vi/VkhZrZmlaK0/maxresdefault.jpg',
}
]

export default streamData;

import asyncHandler from "express-async-handler";
import axios from "axios";
import jwt from "jsonwebtoken";
import dayjs from "dayjs";
import User from "../Models/User.js";

export const streamData = asyncHandler(async (req, res) => {
  const stream = await axios.get(
    `https://api.jwplayer.com/v2/sites/${process.env.JW_PLAYER_PROPERTY_ID}/playlists/`,
    {
      headers: {
        Authorization: `Bearer ${process.env.JW_PLAYER_AUTH}`,
      },
    }
  );

  if (stream) {
    const customPlaylists = [];
    const playlists = stream.data.playlists.filter(
      (playlist) => playlist.playlist_type == "manual"
    );
    await Promise.all(
      playlists.map(async (list, index) => {
        await axios
          .get(`https://cdn.jwplayer.com/v2/playlists/${list.id}`)
          .then(function (data) {
            const playlistItem = [];
            data.data.playlist.map((item) => {
              playlistItem.push({
                id: item.mediaid,
                title: item.title,
                image: item.image,
                duration: item.duration,
                description: item.description,
              });
            });
            customPlaylists.push({
              id: list.id,
              title: list.metadata.title,
              created: list.created,
              params: list.metadata.custom_params,
              image: playlistItem[0].image,
              description: list.metadata.description,
              playlistItems: playlistItem,
            });
          })
          .catch(function (error) {
            res.status(500);
            throw new Error("Server cannot reach Media resources");
          });
      })
    );
    if (customPlaylists) {
      res.json({
        customPlaylists,
      });
    }
  }
});

export const mediaData = asyncHandler(async (req, res) => {
  const user = req.user;
  const API_SECRET = process.env.JW_PLAYER_AUTH;
  const mediaId = req.body.mediaId;
  const playerId = "A6zh6Exr";

  if (user) {
    if (user.planType == "VIP" || "PRE") {
      const token = jwt.sign(
        {
          exp: Math.ceil((dayjs().valueOf() + 3600) / 300) * 300,
          resource: `/v2/media/${mediaId}`,
        },
        API_SECRET
      );
      res.json({
        // mediaUrl: `https://cdn.jwplayer.com/v2/media/${mediaId}?token=${token}`,
        mediaUrl: `https://cdn.jwplayer.com/v2/media/${mediaId}`,
        playerUrl: `https://cdn.jwplayer.com/libraries/ekbdoSbe.js`,
      });
    } else {
      res.status(404);
      throw new Error("User does not have required privilege");
    }
  } else {
    res.status(404);
    throw new Error("User not found");
  }
});

export const likeUpdate = asyncHandler(async (req, res) => {
  const user = req.user;
  const playlist_id = req.body.id; 
  let like; 
  if (user && playlist_id){
    user.liked.push(playlist_id);    
   await axios.get(
      `https://api.jwplayer.com/v2/sites/${process.env.JW_PLAYER_PROPERTY_ID}/playlists/${playlist_id}/manual_playlist/`,
      {
        headers: {
          Authorization: `Bearer ${process.env.JW_PLAYER_AUTH}`,
        },
      }
    ).then(function(data){
      like = Number(data.data.metadata.custom_params.like)+1;    
      
    }) .catch(function (error) {
      res.status(500);
      throw new Error("Server cannot reached");
    });
    await axios.patch(
      `https://api.jwplayer.com/v2/sites/${process.env.JW_PLAYER_PROPERTY_ID}/playlists/${playlist_id}/manual_playlist/`,
      {metadata: {custom_params: {like: {like}}}},
      {
        headers: {
          Authorization: `Bearer ${process.env.JW_PLAYER_AUTH}`,
        },
      }
    ).then(function(data){
      res.json({
        like:like,
      })
    }) .catch(function (error) {
      res.status(500);
      throw new Error("Server cannot reached.");
    });
  
  }else{
    res.status(404);
        throw new Error("User Not found");
  }
})


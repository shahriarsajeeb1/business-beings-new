import asyncHandler from 'express-async-handler'
import User from '../Models/User.js'

export const createUserAdmin = asyncHandler(async(req,res) => {
    const {name,mobile,email,password,pincode} = req.body.userData

    const userExists = await User.findOne({mobile})

    if(userExists){
        res.status(400)
        throw new Error('User already exists')
    }

    const user = await User.create({
        name,mobile,email,password,pincode
    })

    if (user){
        res.status(201).json({            
            messege:"Account Successfully Created",
        })
    }
        else{
            res.status(404)
            throw new Error('Data Incorrect')
        }
    

})

export const getAllUserAdmin = asyncHandler(async(req,res) => {

    const users = await User.find({})

    if(users){
        res.json(
           users,
        )
    }
    else{
        res.status(401)
        throw new Error('No User Found')
    }
})  

export const updateUserAdmin = asyncHandler(async (req, res) => {
    const user = await User.findById(req.body.updatedData._id)
     
    if (user) {
      user.name = req.body.updatedData.name || user.name
      user.mobile = req.body.updatedData.mobile || user.mobile
      user.email = req.body.updatedData.email || user.email
      user.pincode = req.body.updatedData.pincode || user.pincode
      user.plan = req.body.updatedData.plan || user.plan
      if (req.body.updatedData.password !== "") {
        user.password = req.body.updatedData.password
      }
  
      const updatedUser = await user.save()
  
      res.json({
        message:'Account Data Updated'
      })
    } else {
      res.status(404)
      throw new Error('User not found')
    }
  })

export const deleteUserAdmin = asyncHandler(async(req,res) => {

    const user =await User.findOne(req.params.id)

    if(user){
        await user.remove()
        res.json({
           "messege":"User deleted"
        })
    }
    else{
        res.status(401)
        throw new Error('No User Found')
    }
})



import asyncHandler from 'express-async-handler'
import Plan from '../Models/Plans.js'

export const createPlanAdmin = asyncHandler(async(req,res) => {
    const {name,type,duration,price} = req.body.planData
    
    const planExists = await Plan.findOne({name})

    if(planExists){
        res.status(400)
        throw new Error('Plan already exists')
    }

    const plan = await Plan.create({
        name,type,duration,price
    })

    if (plan){
        res.status(201).json({            
            messege:"Plan Successfully Created",
        })
    }
        else{
            res.status(404)
            throw new Error('Data Incorrect')
        }
    

})

export const getAllPlanAdmin = asyncHandler(async(req,res) => {

    const plans = await Plan.find({})

    if(plans){
        res.json(
           plans,
        )
    }
    else{
        res.status(401)
        throw new Error('No Plan Found')
    }
})  

export const updatePlanAdmin = asyncHandler(async (req, res) => {
    const plan = await Plan.findById(req.body.updatedData._id)
     
    if (plan) {
      plan.name = req.body.updatedData.name || plan.name
      plan.type = req.body.updatedData.type || plan.type
      plan.duration = req.body.updatedData.duration || plan.duration
      plan.price = req.body.updatedData.price || plan.price
      plan.description = req.body.updatedData.description || plan.description
        
      const updatedPlan = await plan.save()
  
      res.json({
        message:'Plan Data Updated'
      })
    } else {
      res.status(404)
      throw new Error('Plan not found')
    }
})

export const deletePlanAdmin = asyncHandler(async(req,res) => {
    const plan =await Plan.findById(req.body.id)   

    if(plan){
        await plan.remove()
        res.json({
           "messege":"Plan deleted"
        })
    }
    else{
        res.status(401)
        throw new Error('No Plan Found')
    }
})
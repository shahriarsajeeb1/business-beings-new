import aws from "aws-sdk";
import asyncHandler from "express-async-handler";
import User from "../Models/User.js";
import { generateToken } from "../Utils/TokenGeneration.js";
// import { sesConfig,paramsFunc } from "../Utils/transporter.js";
// accessKeyId:process.env.AWS_ACCESS_KEY,
// secretAccessKey: AWS_SECRET_KEY,

const sesConfig = {
  apiVersion: "2010-12-01",
  region: "us-east-1",
};

const params = {
  Source: "hello@businessbeings.com",
  Destination: {
    ToAddresses: ["pkgowthamsanathan@gmail.com"],
  },
  ReplyToAddresses: ["hello@businessbeings.com"],
  Message: {
    Body: {
      Html: {
        Charset: "UTF-8",
        Data: `<h3>Its good</h3>`,
      },
    },
    Subject: {
      Charset: "UTF-8",
      Data: "Its good",
    },
  },
};

export const findUser = asyncHandler(async (req, res) => {
  const { mobile } = req.body;

  const user = await User.findOne({ mobile });

  if (user) {
    res.json({
      isUser: true,
    });
  } else {
    res.json({
      newUser: true,
    });
  }
});

export const loginUser = asyncHandler(async (req, res) => {
  const { mobile, password } = req.body;

  const user = await User.findOne({ mobile });
  // && (await User.macthPassword(password))

  if (user) {
    res.json({
      token: generateToken(user._id),
    });
  } else {
    res.status(401);
    throw new Error("Password is incorrect");
  }
});

export const createUser = asyncHandler(async (req, res) => {
  const { name, mobile, email, password, pincode } = req.body.userData;

  const userExists = await User.findOne({ mobile });

  if (userExists) {
    res.status(400);
    throw new Error("User already exists");
  }

  const user = await User.create({
    name,
    mobile,
    email,
    password,
    pincode,
  });

  if (user) {
    res.status(201).json({
      token: generateToken(user._id),
    });
  } else {
    res.status(404);
    throw new Error("Data Incorrect");
  }
});

export const getUser = asyncHandler(async (req, res) => {
  const user = req.user;

  if (user) {
    res.json({
      user,
    });
  } else {
    res.status(401);
    throw new Error("No User Found");
  }
});

export const updateUser = asyncHandler(async (req, res) => {
  const user = await User.findById(req.user._id);

  if (user) {
    user.name = req.body.updatedData.name || user.name;
    user.mobile = req.body.updatedData.mobile || user.mobile;
    user.email = req.body.updatedData.email || user.email;
    user.pincode = req.body.updatedData.pincode || user.pincode;
    user.image = req.body.updatedData.image || user.image;
    user.liked = req.body.updatedData.liked || user.liked;
    user.bookMarked = req.body.updatedData.bookMarked || user.bookMarked;
    if (req.body.updatedData.password !== "") {
      user.password = req.body.updatedData.password;
    }

    await user.save(function (err) {
      if (err) {
        res.status(404);
        throw new Error("Database Error.Contact Admin.");
      }
      res.json({
        token: generateToken(user._id),
      });
    });
  } else {
    res.status(404);
    throw new Error("User not found");
  }
});

export const deleteUser = asyncHandler(async (req, res) => {
  const user = await User.findOne(req.params.id);

  if (user) {
    await user.remove();
    res.json({
      messege: "User deleted",
    });
  } else {
    res.status(401);
    throw new Error("No User Found");
  }
});

export const contactForm = (req, res) => {  
  const params = {
    Source: `hello@businessbeings.com`,
    Destination: {
      ToAddresses: ["hello@businessbeings.com"],
    },
    ReplyToAddresses: [`${req.body.contactData.email}`],
    Message: {
      Body: {
        Html: {
          Charset: "UTF-8",
          Data: `<p>You have a new contact request.</p>
          <h3>Contact Details</h3>
          <ul>
            <li>Name: ${req.body.contactData.name}</li>
            <li>Email: ${req.body.contactData.email}</li>
            <li>Mobile: ${req.body.contactData.mobile}</li>
            <li>City: ${req.body.contactData.city}</li>
            <li>Description: ${req.body.contactData.description}</li>          
          </ul>
          `,
        },
      },
      Subject: {
        Charset: "UTF-8",
        Data: "Contact Enquiry",
      },
    },
  };

  try {
    new aws.SES(sesConfig)
      .sendEmail(params)
      .promise()
      .then((data) => {
        res.send({
          success: true,
          message: "Thanks for contacting us. We will get back to you shortly",
        });
      })
      .catch((err) => {        
        res.status(500).send({
          success: false,
          message: "Something went wrong. Try again later",
        });
      });
  } catch (error) {    
    throw new Error("Something went wrong. Try again later");
  }
};

export const serviceForm = (req, res) => {  
  const params = {
    Source: `hello@businessbeings.com`,
    Destination: {
      ToAddresses: ["hello@businessbeings.com"],
    },
    ReplyToAddresses: [`${req.body.serviceData.email}`],
    Message: {
      Body: {
        Html: {
          Charset: "UTF-8",
          Data: `<p>You have a new Service request.</p>
          <h3>Contact Details</h3>
          <ul>
            <li>Name: ${req.body.serviceData.name}</li>
            <li>Email: ${req.body.serviceData.email}</li>
            <li>Mobile: ${req.body.serviceData.mobile}</li>
            <li>City: ${req.body.serviceData.city}</li>
            <li>service: ${req.body.serviceData.service}</li>          
          </ul>
          `,
        },
      },
      Subject: {
        Charset: "UTF-8",
        Data: "Service Enquiry",
      },
    },
  };

  try {    
    new aws.SES(sesConfig)
      .sendEmail(params)
      .promise()
      .then((data) => {
        res.send({
          success: true,
          message: "Thanks for contacting us. We will get back to you shortly",
        });
      })
      .catch((err) => {       
        res.status(500).send({
          success: false,
          message: "Something went wrong. Try again later",
        });
      });
  } catch (error) {    
    throw new Error("Something went wrong. Try again later");
  }
};

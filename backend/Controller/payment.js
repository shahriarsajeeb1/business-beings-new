import asyncHandler from "express-async-handler";
import Razorpay from "razorpay";
import crypto from "crypto";
import dayjs from "dayjs";
import Plan from "../Models/Plans.js";
import User from "../Models/User.js";

export const createOrder = asyncHandler(async (req, res) => {
  const { _id } = req.body.plan;
  const plan = await Plan.findById({ _id });

  var instance = new Razorpay({
    key_id: "rzp_test_4zqfJYaZbadTlg",
    key_secret: "1llkRPejtcNuaDOheA4Mwx12",
  });
  var options = {
    amount: plan.price * 100, // amount in the smallest currency unit
    currency: "INR",
    receipt: "order_rcptid_11",
    payment_capture: 1,
  };
  instance.orders.create(options, function (err, order) {
    if (err) {
      return res.send(err);
    } else {
      return res.json(order);
    }
  });
});

export const verifyPayment = asyncHandler(async (req, res) => {
  const generated_signature = crypto.createHmac(
    "sha256",
    "1llkRPejtcNuaDOheA4Mwx12"
  );
  generated_signature.update(
    req.body.orderDetails.razorpay_order_id +
      "|" +
      req.body.orderDetails.transactionid
  );
  if (
    generated_signature.digest("hex") ===
    req.body.orderDetails.razorpay_signature
  ) {
    const user = await User.findById(req.user._id);
    const { plan_id } = req.body.orderDetails;
    const plan = await Plan.findById(plan_id);

    if (user && plan) {
      user.lastTransactionId =
        req.body.orderDetails.transactionid || user.lastTransactionId || "";
      user.lastOrderId =
        req.body.orderDetails.OrderId || user.lastOrderId || "";
      user.lastTransactionAmount =
          plan.price || 0;
      user.planName = plan.name;  
      user.planType = plan.type;      
      user.planStartingDate = dayjs().toString();
      user.planExpiryDate = dayjs().add(plan.duration, "month").toString();

      const updatedUser = await user.save();

      if (updatedUser) {
        res.json({         
          message: "User Payment Successful",
        });
      } else {
        res.status(404);
        throw new Error("Database Error.Contact Admin.");
      }
    } else {
      res.status(404);
      throw new Error("User not found.Contact Admin.");
    }
  } else {
    res.status(404);
    throw new Error("Payment Failed");
  }
});

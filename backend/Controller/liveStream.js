import asyncHandler from 'express-async-handler'
import LiveStream from '../Models/liveStream.js'

export const createLiveStreamAdmin = asyncHandler(async(req,res) => {
    const {name,startingDate,endingDate,category,interest,image,description,link} = req.body.liveStreamData
    
    const liveStream = await LiveStream.create({
        name,startingDate,endingDate,category,interest,image,description,link
    })

    if (liveStream){       
        res.status(201).json({            
            message:liveStream._id,
        })
    }
        else{
            res.status(404)
            throw new Error('Data Incorrect')
        }
    

})

export const getAllLiveStreamAdmin = asyncHandler(async(req,res) => {

    const liveStream = await LiveStream.find({})

    if(liveStream){
        res.json(
           liveStream,
        )
    }
    else{
        res.status(401)
        throw new Error('No LiveStream Found')
    }
})  

export const updateLiveStreamAdmin = asyncHandler(async (req, res) => {
    console.log(req.body.updatedData)
    const liveStream = await LiveStream.findById(req.body.updatedData._id)
     
    if (liveStream) {
      liveStream.name = req.body.updatedData.name || liveStream.name
      liveStream.startingDate = req.body.updatedData.startingDate || liveStream.startingDate
      liveStream.endingDate = req.body.updatedData.endingDate || liveStream.endingDate
      liveStream.category = req.body.updatedData.category || liveStream.category
      liveStream.interest = req.body.updatedData.interest || liveStream.interest
      liveStream.image = req.body.updatedData.image || liveStream.image
      liveStream.description = req.body.updatedData.description || liveStream.description
      liveStream.link = req.body.updatedData.link || liveStream.link
        
      const updatedPlan = await liveStream.save()
  
      res.json({
        message:'LiveStream Data Updated'
      })
    } else {
      res.status(404)
      throw new Error('LiveStream not found')
    }
})

export const deleteLiveStreamAdmin = asyncHandler(async(req,res) => {
    const liveStream =await LiveStream.findById(req.body.id)   

    if(liveStream){
        await liveStream.remove()
        res.json({
           "messege":"LiveStream deleted"
        })
    }
    else{
        res.status(401)
        throw new Error('No LiveStream Found')
    }
})
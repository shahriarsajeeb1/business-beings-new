import mongoose from "mongoose";
import bcrypt from 'bcryptjs'

const userSchema = mongoose.Schema({
    name:{
        type:String,
        required:true,
    },
    mobile:{
        type:Number,
        required:true,
        unique:true
    },
    email:{
        type:String,
        required:true,
    },
    password:{
        type:String,
        required:true,
    },
    pincode:{
        type:Number,        
        required:true,
    },
    lastTransactionId:{
        type:String,       
    },
    lastTransactionAmount:{
        type:String,        
    },
    lastOrderId:{
        type:String,        
    },
    planStartingDate:{
        type:String,
    },
    planExpiryDate:{
        type:String,
    },
    planName:{
        type:String,
        default:'free',
        required:true,
    },
    planType:{
        type:String,
        default:'free',
        required:true,
    },
    image:{
        type:String,        
       default:'https://www.pngitem.com/pimgs/m/30-307416_profile-icon-png-image-free-download-searchpng-employee.png',       
    },
    bookMarked:[
        {
            type:String,
        }
    ],
    liked:[
        {
            type:String,
        }
    ],
    admin:{
        type:Boolean,
        required:true,
        default:false,
    },

},{timestamps:true})

userSchema.methods.macthPassword = async function(password){
    return await bcrypt.compare(password,this.password);
}

userSchema.pre('save', async function (next) {
    if (!this.isModified('password')) {
      next()
    }
  
    const salt = await bcrypt.genSalt(10)
    this.password = await bcrypt.hash(this.password, salt)
  })

const User = mongoose.model('User',userSchema);

export default User;


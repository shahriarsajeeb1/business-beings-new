import mongoose from "mongoose";

const liveStreamSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    startingDate: {
      type: String,
      required: true,
    },
    endingDate: {
      type: String,
      required: true,
    },
    category: {
      type: String,
      
    },
    interest: {
      type: String,
      
    },
    image: {
      type: String,
      
    },    
    description: {
      type: String,
      required: true,
    },
    link: {
        type: String,
        required: true,
      },
  },
  { timestamps: true }
);

const LiveStream = mongoose.model("LiveStream", liveStreamSchema);

export default LiveStream;

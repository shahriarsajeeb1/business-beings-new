import mongoose from 'mongoose'

const ConnectDB =()=> mongoose.connect ('mongodb://localhost:27017/BusinessBeings',
{
    useNewUrlParser: true,
    useUnifiedTopology: true
  }
  )

const db = mongoose.connection;

db.on("error",console.error.bind(console,"cnnection error:"));
db.once("open", function () {
    console.log("Connected successfully");
  });

export default ConnectDB;
import mongoose from "mongoose";
import ConnectDB from "./Config/ConnectDB.js";
import User from './Models/user.js'
import Stream from './Models/Stream.js'
import Plan from './Models/Plans.js'
import streamData from './InitialData/Stream.js'
import userData from './InitialData/User.js'

ConnectDB()

const createData = async () => {
  try {
    await Stream.deleteMany()
    await Plan.deleteMany()
    await User.deleteMany()

    await User.insertMany(userData)
    await Stream.insertMany(streamData)

    console.log('Data Imported!')
    process.exit()

  } catch (error) {
    console.error(`${error}`)
    process.exit(1)
  }
}

const destroyData = async () => {
  try {
    await Stream.deleteMany()
    await Plan.deleteMany()
    await User.deleteMany()

    console.log('Data Destroyed!')
    process.exit()
  } catch (error) {
    console.error(`${error}`)
    process.exit(1)
  }
}

if (process.argv[2] === '-d') {
  destroyData()
} else {
  createData()
}
import express from "express";
import cors from "cors";
import dotenv from "dotenv";
import schedule from "node-schedule";
import ConnectDB from "./Config/ConnectDB.js";
import streamRoutes from "./Routes/Stream.js";
import planRoutes from "./Routes/plan.js";
import liveStreamRoutes from "./Routes/liveStream.js";
import payment from "./Routes/payment.js";
import userRoutes from "./Routes/User.js";
import adminRoutes from "./Routes/admin.js";
import { errorHandler, notFoundError } from "./Middeware/Error.js";
import User from "./Models/User.js";

dotenv.config();

const app = express();
app.use(express.json());
// app.use(cors());
ConnectDB();


const job = schedule.scheduleJob("* * 1 * * *", function () {
  const users = User.find({});
  users.map((user) => {
    if (dayjs().isAfter(dayjs(user.planExpiryDate))) {
      user.plan = "free";
    }
  });
});

job();

app.use("/user", userRoutes);
app.use("/admin", adminRoutes);
app.use("/stream", streamRoutes);
app.use("/plan", planRoutes);
app.use("/livestream", liveStreamRoutes);
app.use("/payment", payment);



app.use(notFoundError);
app.use(errorHandler);

app.listen(5000, console.log("Im working"));

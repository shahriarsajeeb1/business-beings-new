export const sesConfig = () => {
  return({
    apiVersion:'2010-12-01',
    accessKeyId:process.env.AWS_ACCESS_KEY,
    secretAccessKey:process.env.AWS_SECRET_KEY,
    region:process.env.AWS.REGION,
  })
}


export const paramsFunc = ()=>{
  return({
    Source:'hello@businessbeings.com',
    Destination:{
      ToAddresses:['pkgowthamsanathan@gmail.com']
    },
    ReplyToAddresses:[
      'hello@businessbeings.com',
    ],
    Message:{
      Body:{
        Html:{
          Charset:'UTF-8',
          Data:`<h3>Its good</h3>`
        }
      },
      Subject:{
        Charset:'UTF-8',
        Data:'Its good',
      }
    }
  })
}


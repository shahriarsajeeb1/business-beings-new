import express from 'express'
import { createLiveStreamAdmin,getAllLiveStreamAdmin,updateLiveStreamAdmin, deleteLiveStreamAdmin} from '../Controller/liveStream.js';
import { protect,admin } from '../Middeware/Protect.js';

const router = express.Router()


router.route('/createlivestreamadmin').post(protect,admin,createLiveStreamAdmin)
router.route('/getalllivestreamadmin').get(getAllLiveStreamAdmin)
router.route('/updatelivestreamadmin').put(protect,admin,updateLiveStreamAdmin)
router.route('/deletelivestreamadmin').delete(protect,admin,deleteLiveStreamAdmin)



export default router;
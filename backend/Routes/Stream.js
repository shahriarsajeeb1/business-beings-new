import express from 'express'
import { streamData,mediaData,likeUpdate } from '../Controller/stream.js';
import { protect } from '../Middeware/Protect.js';

const router = express.Router()


router.route('/streamdata').get(streamData)
router.route('/mediadata').post(protect,mediaData)
router.route('/like').post(protect,likeUpdate)



export default router;
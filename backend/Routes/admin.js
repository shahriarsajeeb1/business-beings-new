import express from 'express'
import { createUserAdmin,getAllUserAdmin,updateUserAdmin, deleteUserAdmin} from '../Controller/admin.js';
import { protect,admin } from '../Middeware/Protect.js';

const router = express.Router()


router.route('/createuseradmin').post(protect,admin,createUserAdmin)
router.route('/getalluseradmin').get(protect,admin,getAllUserAdmin)
router.route('/updateuseradmin').put(protect,admin,updateUserAdmin)
router.route('/deleteuseradmin').delete(protect,admin,deleteUserAdmin)



export default router;
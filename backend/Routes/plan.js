import express from 'express'
import { createPlanAdmin,getAllPlanAdmin,updatePlanAdmin, deletePlanAdmin} from '../Controller/plan.js';
import { protect,admin } from '../Middeware/Protect.js';

const router = express.Router()


router.route('/createplanadmin').post(protect,admin,createPlanAdmin)
router.route('/getallplanadmin').get(getAllPlanAdmin)
router.route('/updateplanadmin').put(protect,admin,updatePlanAdmin)
router.route('/deleteplanadmin').delete(protect,admin,deletePlanAdmin)



export default router;
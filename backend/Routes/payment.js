import express from 'express'
import { protect } from '../Middeware/Protect.js';
import { createOrder,verifyPayment } from '../Controller/payment.js';

const router = express.Router()


router.route('/order').post(protect,createOrder)
router.route('/payment').post(protect,verifyPayment)


export default router;
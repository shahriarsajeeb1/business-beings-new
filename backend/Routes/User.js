import express from 'express'
import { findUser,loginUser,createUser,updateUser,getUser,deleteUser,contactForm,serviceForm } from '../Controller/user.js';
import { protect } from '../Middeware/Protect.js';

const router = express.Router()


router.post('/finduser',findUser)
router.post('/loginuser',loginUser)
router.post('/createuser',createUser)
router.route('/getuser').get(protect,getUser)
router.route('/updateuser').put(protect,updateUser)
router.route('/deleteuser').delete(protect,deleteUser)
router.post('/contact',contactForm)
router.post('/service',serviceForm)



export default router;
import jwt from 'jsonwebtoken'
import asyncHandler from 'express-async-handler'
import User from '../Models/User.js'

export const protect = asyncHandler(async(req,res,next) => {
    if(req.headers.authorization && 
        req.headers.authorization.startsWith('Bearer')){
            let token = req.headers.authorization.split(' ')[1]
            const decodedId = jwt.verify(token,'123abc')
            req.user = await User.findById(decodedId.id)           
            next()
    }
    if(!req.headers.authorization.split(' ')[1]){
        res.status(401)
        throw new Error('Not authorized, no token')
    }
})

export const admin = (req, res, next) => {
    if (req.user && req.user.admin) {
      next()
    } else {
      res.status(401)
      throw new Error('Not authorized as an admin')
    }
  }
import { initializeApp } from "firebase/app";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyCHbQm6TmQgptj3R8_pKZeYStCNi5suV6A",
  authDomain: "businessbeing-e7dd1.firebaseapp.com",
  projectId: "businessbeing-e7dd1",
  storageBucket: "gs://businessbeing-e7dd1.appspot.com",
  messagingSenderId: "449520090943",
  appId: "1:449520090943:web:31a3c177e535b07aa41d8a",
  measurementId: "G-B02BX7X039"
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
const storage = getStorage(firebaseApp);

export default storage

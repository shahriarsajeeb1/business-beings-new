import { ACTION_TYPE } from "../../../Constant";

const initState = {
    isLoadingCreate: false,
    isSuccessCreate: false, 
    isErrorCreate: false,
    successMessageCreate:'', 
    errorMessageCreate:'', 
    token:'',
    

    isLoadingGet: false,
    isSuccessGet: false, 
    isErrorGet: false,
    successMessageGet:'', 
    errorMessageGet:'',
    user:{},

    isLoadingUpdate: false,
    isSuccessUpdate: false, 
    isErrorUpdate: false,
    successMessageUpdate:'', 
    errorMessageUpdate:'', 
    

    isLoadingDelete: false,
    isSuccessDelete: false, 
    isErrorDelete: false,
    successMessageDelete:'', 
    errorMessageDelete:'',

    isLoadingContact: false,
    isSuccessContact: false, 
    isErrorContact: false,
    successMessageContact:'', 
    errorMessageContact:'',

    isLoadingService: false,
    isSuccessService: false, 
    isErrorService: false,
    successMessageService:'', 
    errorMessageService:'',
};

const userDataReducer = (state=initState, action) => {
    switch(action.type) {

        case ACTION_TYPE.CREATE_USER_PENDING:{
            return {...state, isLoadingCreate: true, isSuccessCreate: false,  isErrorCreate: false,successMessageCreate:'',errorMessageCreate:''}
        }

        case ACTION_TYPE.CREATE_USER_FULFILLED: {
            return {...state, isLoadingCreate: false, isSuccessCreate: true, isErrorCreate: false,successMessageCreate:action.payload.data.message, errorMessageCreate:'',token:action.payload.data.token,}
        }

        case ACTION_TYPE.CREATE_USER_REJECTED: {
            return {...state, isLoadingCreate: false, isSuccessCreate: false,isErrorCreate: true,successMessageCreate:'', errorMessageCreate:action.payload.error }
        }

        case ACTION_TYPE.CREATE_RETURN_INITIAL_STATE_USER: {
            return {...state, isLoadingCreate: false, isSuccessCreate: false,isErrorCreate: false,successMessageCreate:'', errorMessageCreate:'' }
        }




        case ACTION_TYPE.GET_USER_PENDING:{
            return {...state, isLoadingGet: true, isSuccessGet: false,  isErrorGet: false,successMessageGet:'',errorMessageGet:''}
        }

        case ACTION_TYPE.GET_USER_FULFILLED: {
            return {...state, isLoadingGet: false, isSuccessGet: true, isErrorGet: false,successMessageGet:'Users Received',errorMessageGet:'',user:action.payload.data.user}
        }

        case ACTION_TYPE.GET_USER_REJECTED: {
            return {...state, isLoadingGet: false, isSuccessGet: false,isErrorGet: true,successMessageGet:'', errorMessageGet:action.payload.error }
        }

        case ACTION_TYPE.GET_RETURN_INITIAL_STATE_USER: {
            return {...state, isLoadingGet: false, isSuccessGet: false,isErrorGet: false,successMessageGet:'', errorMessageGet:'' }
        }




        case ACTION_TYPE.UPDATED_USER_PENDING:{
            return {...state, isLoadingUpdate: true, isSuccessUpdate: false,  isErrorUpdate: false,successMessageUpdate:'',errorMessageUpdate:''}
        }

        case ACTION_TYPE.UPDATED_USER_FULFILLED: {
            return {...state, isLoadingUpdate: false, isSuccessUpdate: true, isErrorUpdate: false,successMessageUpdate:action.payload.data.message,errorMessageUpdate:'', token:action.payload.data.token}
        }

        case ACTION_TYPE.UPDATED_USER_REJECTED: {
            return {...state, isLoadingUpdate: false, isSuccessUpdate: false,isErrorUpdate: true,successMessageUpdate:'', errorMessageUpdate:action.payload.error }
        }

        case ACTION_TYPE.UPDATED_RETURN_INITIAL_STATE_USER: {
            return {...state, isLoadingUpdate: false, isSuccessUpdate: false,isErrorUpdate: false,successMessageUpdate:'', errorMessageUpdate:'' }
        }





        case ACTION_TYPE.DELETE_USER_PENDING:{
            return {...state, isLoadingDelete: true, isSuccessDelete: false,  isErrorDelete: false,successMessageDelete:'',errorMessageDelete:''}
        }

        case ACTION_TYPE.DELETE_USER_FULFILLED: {
            return {...state, isLoadingDelete: false, isSuccessDelete: true, isErrorDelete: false,successMessageDelete:action.payload.data.message,errorMessageDelete:''}
        }

        case ACTION_TYPE.DELETE_USER_REJECTED: {
            return {...state, isLoadingDelete: false, isSuccessDelete: false,isErrorDelete: true,successMessageDelete:'', errorMessageDelete:action.payload.error }
        }
        case ACTION_TYPE.DELETE_RETURN_INITIAL_STATE_USER: {
            return {...state, isLoadingDelete: false, isSuccessDelete: false,isErrorDelete: false,successMessageDelete:'', errorMessageDelete:'' }
        }

       

        case ACTION_TYPE.SEND_CONTACT_FORM_USER_PENDING:{
            return {...state, isLoadingContact: true, isSuccessContact: false,  isErrorContact: false,successMessageContact:'',errorMessageContact:''}
        }

        case ACTION_TYPE.SEND_CONTACT_FORM_USER_FULFILLED: {
            return {...state, isLoadingContact: false, isSuccessContact: true, isErrorContact: false,successMessageContact:action.payload.data.message,errorMessageContact:''}
        }

        case ACTION_TYPE.SEND_CONTACT_FORM_USER_REJECTED: {
            return {...state, isLoadingContact: false, isSuccessContact: false,isErrorContact: true,successMessageContact:'', errorMessageContact:action.payload.error }
        }
        case ACTION_TYPE.SEND_CONTACT_FORM_RETURN_INITIAL_STATE_USER: {
            return {...state, isLoadingContact: false, isSuccessContact: false,isErrorContact: false,successMessageContact:'', errorMessageContact:'' }
        }

        

        case ACTION_TYPE.SEND_SERVICE_FORM_USER_PENDING:{
            return {...state, isLoadingService: true, isSuccessService: false,  isErrorService: false,successMessageService:'',errorMessageService:''}
        }

        case ACTION_TYPE.SEND_SERVICE_FORM_USER_FULFILLED: {
            return {...state, isLoadingService: false, isSuccessService: true, isErrorService: false,successMessageService:action.payload.data.message,errorMessageService:''}
        }

        case ACTION_TYPE.SEND_SERVICE_FORM_USER_REJECTED: {
            return {...state, isLoadingService: false, isSuccessService: false,isErrorService: true,successMessageService:'', errorMessageService:action.payload.error }
        }
        case ACTION_TYPE.SEND_SERVICE_FORM_RETURN_INITIAL_STATE_USER: {
            return {...state, isLoadingService: false, isSuccessService: false,isErrorService: false,successMessageService:'', errorMessageService:'' }
        }

        case ACTION_TYPE.USER_RETURN_INITIAL_STATE_USER: {
            return {...initState}
        }
        
        
        default: {
            return state;
        }
    }
};

export default userDataReducer;
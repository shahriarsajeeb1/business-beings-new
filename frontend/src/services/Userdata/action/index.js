import { ACTION_TYPE } from "../../../Constant";

export const createUser = (userData)=>({
    type:ACTION_TYPE.CREATE_USER_SAGA,
    payload:{
        userData:userData,
    }
})

export const getUser = (token)=>({
    type:ACTION_TYPE.GET_USER_SAGA,
    payload:{
        token:token,
    }
})

export const updatedUser = (updatedData)=>({
    type:ACTION_TYPE.UPDATED_USER_SAGA,
    payload:{
        updatedData:updatedData,
    }
})

export const deleteUser = ()=>({
    type:ACTION_TYPE.UPDATED_USER_SAGA,    
})

export const sendContactForm = (contactData)=>({
    type:ACTION_TYPE.SEND_CONTACT_FORM_USER_SAGA, 
    payload:{
        contactData:contactData,
    }  
})

export const sendServiceForm = (serviceData)=>({
    type:ACTION_TYPE.SEND_SERVICE_FORM_USER_SAGA,
    payload:{
        serviceData:serviceData,
    }   
})

export const sendContactReturnInitialState = ()=>({
    type:ACTION_TYPE.SEND_CONTACT_FORM_RETURN_INITIAL_STATE_USER,    
})

export const sendServiceReturnInitialState = ()=>({
    type:ACTION_TYPE.SEND_SERVICE_FORM_RETURN_INITIAL_STATE_USER,    
})

export const userReturnInitialState = ()=>({
    type:ACTION_TYPE.USER_RETURN_INITIAL_STATE_USER,    
})


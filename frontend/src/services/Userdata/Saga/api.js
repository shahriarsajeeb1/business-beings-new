import {axio} from '../../../axios'


export const createUserApi = (userData) => {
    return (
        axio.post('/user/createuser',{userData:userData})
    )
}

export const getUserApi = (token) => {
    return (
        axio.get('/user/getuser',{headers: { Authorization: `Bearer ${token}` }})
    )
}

export const updatedUserApi = (updatedData) => {
    return (
        axio.put('/user/updateuser',{updatedData:updatedData})
    )
}

export const deleteUserApi = () => {
    return (
        axio.delete('/user/deleteuser')
    )
}

export const sendContactFormApi = (contactData) => {
    return (
        axio.post('/user/contact',{contactData:contactData})
    )
}

export const sendServiceFormApi = (serviceData) => {
    return (
        axio.post('/user/service',{serviceData:serviceData})
    )
}
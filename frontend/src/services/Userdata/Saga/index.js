import { put,call,takeLatest } from 'redux-saga/effects'
import { ACTION_TYPE } from '../../../Constant'
import {updatedUserApi,createUserApi,getUserApi,deleteUserApi,sendContactFormApi,sendServiceFormApi} from './api'

function* workerCreateUser(action){
    yield put({type:ACTION_TYPE.CREATE_USER_PENDING})

    try {
        const data = yield call(createUserApi,action.payload.userData);
        yield put({type:ACTION_TYPE.CREATE_USER_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.CREATE_USER_REJECTED,payload:{error:error}})
    }
}

function* workerGetUser(action){

    yield put({type:ACTION_TYPE.GET_USER_PENDING})

    try {
        const data = yield call(getUserApi,action.payload.token);
        yield put({type:ACTION_TYPE.GET_USER_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.GET_USER_REJECTED,payload:{error:error}})
    }
}

function* workerUpdatedUser(action){
    yield put({type:ACTION_TYPE.UPDATED_USER_PENDING})

    try {
        const data = yield call(updatedUserApi,action.payload.updatedData);
        yield put({type:ACTION_TYPE.UPDATED_USER_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.UPDATED_USER_REJECTED,payload:{error:error}})
    }
}

function* workerDeleteUser(action){
    yield put({type:ACTION_TYPE.DELETE_USER_PENDING})

    try {
        const data = yield call(deleteUserApi);
        yield put({type:ACTION_TYPE.DELETE_USER_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.DELETE_USER_REJECTED,payload:{error:error}})
    }
}

function* workerContactUser(action){
    yield put({type:ACTION_TYPE.SEND_CONTACT_FORM_USER_PENDING})

    try {
        const data = yield call(sendContactFormApi,action.payload.contactData);
        yield put({type:ACTION_TYPE.SEND_CONTACT_FORM_USER_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.SEND_CONTACT_FORM_USER_REJECTED,payload:{error:error}})
    }
}

function* workerServiceUser(action){  
    yield put({type:ACTION_TYPE.SEND_SERVICE_FORM_USER_PENDING})

    try {
        const data = yield call(sendServiceFormApi,action.payload.serviceData);
        yield put({type:ACTION_TYPE.SEND_SERVICE_FORM_USER_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.SEND_SERVICE_FORM_USER_REJECTED,payload:{error:error}})
    }
}

export function* watchCreateUser(){
    yield takeLatest(ACTION_TYPE.CREATE_USER_SAGA,workerCreateUser)
}

export function* watchGetUser(){
    yield takeLatest(ACTION_TYPE.GET_USER_SAGA,workerGetUser)
}

export function* watchUpdatedUser(){
    yield takeLatest(ACTION_TYPE.UPDATED_USER_SAGA,workerUpdatedUser)
}

export function* watchDeleteUser(){
    yield takeLatest(ACTION_TYPE.DELETE_USER_SAGA,workerDeleteUser)
}

export function* watchContactUser(){
    yield takeLatest(ACTION_TYPE.SEND_CONTACT_FORM_USER_SAGA,workerContactUser)
}

export function* watchServiceUser(){
    yield takeLatest(ACTION_TYPE.SEND_SERVICE_FORM_USER_SAGA,workerServiceUser)
}
import { ACTION_TYPE } from "../../../Constant";

const initState = {
    isLoadingCreate: false,
    isSuccessCreate: false, 
    isErrorCreate: false,
    successMessageCreate:'', 
    errorMessageCreate:'', 

    isLoadingGet: false,
    isSuccessGet: false, 
    isErrorGet: false,
    successMessageGet:'', 
    errorMessageGet:'',
    plans:[],

    isLoadingUpdate: false,
    isSuccessUpdate: false, 
    isErrorUpdate: false,
    successMessageUpdate:'', 
    errorMessageUpdate:'', 

    isLoadingDelete: false,
    isSuccessDelete: false, 
    isErrorDelete: false,
    successMessageDelete:'', 
    errorMessageDelete:'', 
   
};

const planReducer = (state=initState, action) => {
    switch(action.type) {

        case ACTION_TYPE.CREATE_PLAN_ADMIN_PENDING:{
            return {...state, isLoadingCreate: true, isSuccessCreate: false,  isErrorCreate: false,successMessageCreate:'',errorMessageCreate:''}
        }

        case ACTION_TYPE.CREATE_PLAN_ADMIN_FULFILLED: {
            return {...state, isLoadingCreate: false, isSuccessCreate: true, isErrorCreate: false,successMessageCreate:action.payload.data.message, errorMessageCreate:''}
        }

        case ACTION_TYPE.CREATE_PLAN_ADMIN_REJECTED: {
            return {...state, isLoadingCreate: false, isSuccessCreate: false,isErrorCreate: true,successMessageCreate:'', errorMessageCreate:action.payload.error }
        }

        case ACTION_TYPE.CREATE_PLAN_RETURN_INITIAL_STATE_ADMIN: {
            return {...state, isLoadingCreate: false, isSuccessCreate: false,isErrorCreate: false,successMessageCreate:'', errorMessageCreate:'' }
        }



       case ACTION_TYPE.GET_ALL_PLAN_ADMIN_PENDING:{
            return {...state, isLoadingGet: true, isSuccessGet: false,  isErrorGet: false,successMessageGet:'',errorMessageGet:''}
        }

        case ACTION_TYPE.GET_ALL_PLAN_ADMIN_FULFILLED: {
            return {...state, isLoadingGet: false, isSuccessGet: true, isErrorGet: false,successMessageGet:'Users Received',errorMessageGet:'',plans:action.payload.data}
        }

        case ACTION_TYPE.GET_ALL_PLAN_ADMIN_REJECTED: {
            return {...state, isLoadingGet: false, isSuccessGet: false,isErrorGet: true,successMessageGet:'', errorMessageGet:action.payload.error }
        }

        case ACTION_TYPE.GET_PLAN_RETURN_INITIAL_STATE_ADMIN: {
            return {...state, isLoadingGet: false, isSuccessGet: false,isErrorGet: false,successMessageGet:'', errorMessageGet:'' }
        }




        case ACTION_TYPE.UPDATED_PLAN_ADMIN_PENDING:{
            return {...state, isLoadingUpdate: true, isSuccessUpdate: false,  isErrorUpdate: false,successMessageUpdate:'',errorMessageUpdate:''}
        }

        case ACTION_TYPE.UPDATED_PLAN_ADMIN_FULFILLED: {
            return {...state, isLoadingUpdate: false, isSuccessUpdate: true, isErrorUpdate: false,successMessageUpdate:action.payload.data.message,errorMessageUpdate:''}
        }

        case ACTION_TYPE.UPDATED_PLAN_ADMIN_REJECTED: {
            return {...state, isLoadingUpdate: false, isSuccessUpdate: false,isErrorUpdate: true,successMessageUpdate:'', errorMessageUpdate:action.payload.error }
        }

        case ACTION_TYPE.UPDATED_PLAN_RETURN_INITIAL_STATE_ADMIN: {
            return {...state, isLoadingUpdate: false, isSuccessUpdate: false,isErrorUpdate: false,successMessageUpdate:'', errorMessageUpdate:'' }
        }





        case ACTION_TYPE.DELETE_PLAN_ADMIN_PENDING:{
            return {...state, isLoadingDelete: true, isSuccessDelete: false,  isErrorDelete: false,successMessageDelete:'',errorMessageDelete:''}
        }

        case ACTION_TYPE.DELETE_PLAN_ADMIN_FULFILLED: {
            return {...state, isLoadingDelete: false, isSuccessDelete: true, isErrorDelete: false,successMessageDelete:action.payload.data.message,errorMessageDelete:''}
        }

        case ACTION_TYPE.DELETE_PLAN_ADMIN_REJECTED: {
            return {...state, isLoadingDelete: false, isSuccessDelete: false,isErrorDelete: true,successMessageDelete:'', errorMessageDelete:action.payload.error }
        }
        case ACTION_TYPE.DELETE_PLAN_RETURN_INITIAL_STATE_ADMIN: {
            return {...state, isLoadingDelete: false, isSuccessDelete: false,isErrorDelete: false,successMessageDelete:'', errorMessageDelete:'' }
        }

        default: {
            return state;
        }
    }
};
export default planReducer;
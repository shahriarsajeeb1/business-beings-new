import { ACTION_TYPE } from "../../../Constant";

export const getAllPlanAdmin = ()=>({
    type:ACTION_TYPE.GET_ALL_PLAN_ADMIN_SAGA,   
})

export const createPlanAdmin = (createData)=>({
    type:ACTION_TYPE.CREATE_PLAN_ADMIN_SAGA,
    payload:{
        planData:createData,
    }
})

export const updatedPlanAdmin = (updatedData)=>({
    type:ACTION_TYPE.UPDATED_PLAN_ADMIN_SAGA,
    payload:{
        updatedData:updatedData,
    }
})

export const deletePlanAdmin = (planId)=>({
    type:ACTION_TYPE.DELETE_PLAN_ADMIN_SAGA,
    payload:{
        planId:planId
    }    
})

export const getPlanReturnInitialStateAdmin = ()=>({
    type:ACTION_TYPE.GET_PLAN_RETURN_INITIAL_STATE_ADMIN,     
})

export const createPlanReturnInitialStateAdmin = ()=>({
    type:ACTION_TYPE.CREATE_PLAN_RETURN_INITIAL_STATE_ADMIN,     
})

export const updatePlanReturnInitialStateAdmin = ()=>({
    type:ACTION_TYPE.UPDATED_PLAN_RETURN_INITIAL_STATE_ADMIN,     
})


export const deletePlanReturnInitialStateAdmin = ()=>({
    type:ACTION_TYPE.DELETE_PLAN_RETURN_INITIAL_STATE_ADMIN,     
})




import {axio} from '../../../axios'


export const createPlanAdminApi = (planData) => {
    return (
        axio.post('/plan/createplanadmin',{planData:planData})
    )
}

export const getAllPlanAdminApi = () => {
    return (
        axio.get('/plan/getallplanadmin')
    )
}

export const updatedPlanAdminApi = (updatedData) => {
    return (
        axio.put('/plan/updateplanadmin',{updatedData:updatedData})
    )
}

export const deletePlanAdminApi = (planId) => {
    return (
        axio.delete('/plan/deleteplanadmin',{data:{id:planId}})
    )
}
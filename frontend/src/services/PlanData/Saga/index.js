import { put,call,takeLatest } from 'redux-saga/effects'
import { ACTION_TYPE } from '../../../Constant'
import {updatedPlanAdminApi,createPlanAdminApi,getAllPlanAdminApi,deletePlanAdminApi} from './api'

function* workerCreatePlanAdmin(action){
    yield put({type:ACTION_TYPE.CREATE_PLAN_ADMIN_PENDING})

    try {
        const data = yield call(createPlanAdminApi,action.payload.planData);
        yield put({type:ACTION_TYPE.CREATE_PLAN_ADMIN_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.CREATE_PLAN_ADMIN_REJECTED,payload:{error:error}})
    }
}

function* workerGetAllPlanAdmin(action){
    yield put({type:ACTION_TYPE.GET_ALL_PLAN_ADMIN_PENDING})
    
    try {
        const data = yield call(getAllPlanAdminApi);
        yield put({type:ACTION_TYPE.GET_ALL_PLAN_ADMIN_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.GET_ALL_PLAN_ADMIN_REJECTED,payload:{error:error}})
    }
}

function* workerUpdatedPlanAdmin(action){
    yield put({type:ACTION_TYPE.UPDATED_PLAN_ADMIN_PENDING})

    try {
        
        const data = yield call(updatedPlanAdminApi,action.payload.updatedData);
        yield put({type:ACTION_TYPE.UPDATED_PLAN_ADMIN_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.UPDATED_PLAN_ADMIN_REJECTED,payload:{error:error}})
    }
}

function* workerDeletePlanAdmin(action){
    yield put({type:ACTION_TYPE.DELETE_PLAN_ADMIN_PENDING})

    try {
        console.log(action.payload.planId)
        const data = yield call(deletePlanAdminApi,action.payload.planId);
        yield put({type:ACTION_TYPE.DELETE_PLAN_ADMIN_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.DELETE_PLAN_ADMIN_REJECTED,payload:{error:error}})
    }
}

export function* watchCreatePlanAdmin(){
    yield takeLatest(ACTION_TYPE.CREATE_PLAN_ADMIN_SAGA,workerCreatePlanAdmin)
}

export function* watchGetAllPlanAdmin(){
    yield takeLatest(ACTION_TYPE.GET_ALL_PLAN_ADMIN_SAGA,workerGetAllPlanAdmin)
}


export function* watchUpdatedPlanAdmin(){
    yield takeLatest(ACTION_TYPE.UPDATED_PLAN_ADMIN_SAGA,workerUpdatedPlanAdmin)
}

export function* watchDeletePlanAdmin(){
    yield takeLatest(ACTION_TYPE.DELETE_PLAN_ADMIN_SAGA,workerDeletePlanAdmin)
}
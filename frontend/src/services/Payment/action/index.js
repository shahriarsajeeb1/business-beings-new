import { ACTION_TYPE } from "../../../Constant";


export const createPaymentOrder = (plan)=>({
    type:ACTION_TYPE.CREATE_PAYMENT_ORDER_SAGA,
    payload:{        
        plan:plan,
    }
})

export const verifyPayment = (orderDetails)=>({
    type:ACTION_TYPE.VERIFY_PAYMENT_SAGA,
    payload:{
        orderDetails:orderDetails,
    }   
})

export const createPaymentOrderInitialState = ()=>({
    type:ACTION_TYPE.CREATE_PAYMENT_ORDER_INITIAL_STATE,     
})

export const verifyPaymentIntialState = ()=>({
    type:ACTION_TYPE.VERIFY_PAYMENT_INITIAL_STATE,     
})







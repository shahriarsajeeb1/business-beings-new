import {axio} from '../../../axios'


export const createPaymentOrderApi = (plan) => {
    return (
        axio.post('/payment/order',{plan:plan},{timeout:2500})
    )
}


export const verifyPaymentApi = (orderDetails) => {
    return (
        axio.post('/payment/payment',{orderDetails:orderDetails})
    )
}


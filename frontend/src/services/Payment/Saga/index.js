import { put,call,takeLatest } from 'redux-saga/effects'
import { ACTION_TYPE } from '../../../Constant'
import {createPaymentOrderApi,verifyPaymentApi} from './api'

function* workerCreatePaymentOrder(action){
    yield put({type:ACTION_TYPE.CREATE_PAYMENT_ORDER_PENDING})

    try {
        const data = yield call(createPaymentOrderApi,action.payload.plan);
        yield put({type:ACTION_TYPE.CREATE_PAYMENT_ORDER_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.CREATE_PAYMENT_ORDER_REJECTED,payload:{error:error}})
    }
}



function* workerVerifyPayment(action){
    yield put({type:ACTION_TYPE.VERIFY_PAYMENT_PENDING})

    try {
        
        const data = yield call(verifyPaymentApi,action.payload.orderDetails);
        yield put({type:ACTION_TYPE.VERIFY_PAYMENT_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.VERIFY_PAYMENT_REJECTED,payload:{error:error}})
    }
}



export function* watchCreatePaymentOrder(){
    yield takeLatest(ACTION_TYPE.CREATE_PAYMENT_ORDER_SAGA,workerCreatePaymentOrder)
}

export function* watchVerifyPayment(){
    yield takeLatest(ACTION_TYPE.VERIFY_PAYMENT_SAGA,workerVerifyPayment)
}


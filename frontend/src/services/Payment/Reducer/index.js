import { ACTION_TYPE } from "../../../Constant";

const initState = {
    isLoadingCreate: false,
    isSuccessCreate: false, 
    isErrorCreate: false,
    successMessageCreate:'', 
    errorMessageCreate:'', 
    order:{},
    
    isLoadingVerify: false,
    isSuccessVerify: false, 
    isErrorVerify: false,
    successMessageVerify:'', 
    errorMessageVerify:'',    
   
};

const paymentReducer = (state=initState, action) => {
    switch(action.type) {

        case ACTION_TYPE.CREATE_PAYMENT_ORDER_PENDING:{
            return {...state, isLoadingCreate: true, isSuccessCreate: false,  isErrorCreate: false,successMessageCreate:'',errorMessageCreate:''}
        }

        case ACTION_TYPE.CREATE_PAYMENT_ORDER_FULFILLED: {
            return {...state, isLoadingCreate: false, isSuccessCreate: true, isErrorCreate: false,successMessageCreate:action.payload.data.message, errorMessageCreate:'',order:action.payload.data}
        }

        case ACTION_TYPE.CREATE_PAYMENT_ORDER_REJECTED: {
            return {...state, isLoadingCreate: false, isSuccessCreate: false,isErrorCreate: true,successMessageCreate:'', errorMessageCreate:action.payload.error }
        }

        case ACTION_TYPE.CREATE_PAYMENT_ORDER_INITIAL_STATE: {
            return {...state, isLoadingCreate: false, isSuccessCreate: false,isErrorCreate: false,successMessageCreate:'', errorMessageCreate:'' }
        }

      
        case ACTION_TYPE.VERIFY_PAYMENT_PENDING:{
            return {...state, isLoadingUpdate: true, isSuccessUpdate: false,  isErrorUpdate: false,successMessageUpdate:'',errorMessageUpdate:''}
        }

        case ACTION_TYPE.VERIFY_PAYMENT_FULFILLED: {
            return {...state, isLoadingUpdate: false, isSuccessUpdate: true, isErrorUpdate: false,successMessageUpdate:action.payload.data.message,errorMessageUpdate:''}
        }

        case ACTION_TYPE.VERIFY_PAYMENT_REJECTED: {
            return {...state, isLoadingUpdate: false, isSuccessUpdate: false,isErrorUpdate: true,successMessageUpdate:'', errorMessageUpdate:action.payload.error }
        }

        case ACTION_TYPE.VERIFY_PAYMENT_INITIAL_STATE: {
            return {...state, isLoadingUpdate: false, isSuccessUpdate: false,isErrorUpdate: false,successMessageUpdate:'', errorMessageUpdate:'' }
        }

        default: {
            return state;
        }
    }
};
export default paymentReducer;
import { ACTION_TYPE } from "../../../Constant";

export const getStreamData = () => ({
    type:ACTION_TYPE.GET_STREAM_DATA_SAGA,
})

export const getMediaData = (mediaId) => ({
    type:ACTION_TYPE.GET_MEDIA_DATA_SAGA,
    payload:{
        mediaId:mediaId,
    }
})

export const updateLike = () => ({
    type:ACTION_TYPE.UPDATE_LIKE_SAGA,    
})

export const getStreamReturnInitialState = () => ({
    type:ACTION_TYPE.GET_STREAM_RETURN_INITIAL_STATE,
})

export const getMediaReturnInitialState = () => ({
    type:ACTION_TYPE.GET_MEDIA_RETURN_INITIAL_STATE,
})

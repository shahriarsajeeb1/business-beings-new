import { ACTION_TYPE } from "../../../Constant";

const initialState = {
    isLoadingStream: false,
    isSuccessStream: false, 
    isErrorStream: false,
    successMessageStream:'', 
    errorMessageStream:'',
    streams:[], 

    isLoadingMedia: false,
    isSuccessMedia: false, 
    isErrorMedia: false,
    successMessageMedia:'', 
    errorMessageMedia:'',
    mediaUrl:'', 
    playerUrl:'', 

    isLoadingLike: false,
    isSuccessLike: false, 
    isErrorLike: false,
    successMessageLike:'', 
    errorMessageLike:'',
    like:0,
   
};

const dataReducer = (state=initialState,action) =>{
    switch(action.type) {

        case ACTION_TYPE.GET_STREAM_DATA_PENDING:{
            return {...state, isLoadingStream: true, isSuccessStream: false,  isErrorStream: false,successMessageStream:'',errorMessageStream:''}
        }

        case ACTION_TYPE.GET_STREAM_DATA_FULFILLED: {
            return {...state, isLoadingStream: false, isSuccessStream: true, isErrorStream: false,successMessageStream:action.payload.data.message, errorMessageStream:'',streams:action.payload.data.customPlaylists}
        }

        case ACTION_TYPE.GET_STREAM_DATA_REJECTED: {
            return {...state, isLoadingStream: false, isSuccessStream: false,isErrorStream: true,successMessageStream:'', errorMessageStream:action.payload.error }
        } 
        
        case ACTION_TYPE.GET_STREAM_RETURN_INITIAL_STATE: {
            return {...state, isLoadingStream: false, isSuccessStream: false,isErrorStream: false,successMessageStream:'', errorMessageStream:'' }
        } 



       case ACTION_TYPE.GET_MEDIA_DATA_PENDING:{
            return {...state, isLoadingMedia: true, isSuccessMedia: false,  isErrorMedia: false,successMessageMedia:'',errorMessageMedia:''}
        }

        case ACTION_TYPE.GET_MEDIA_DATA_FULFILLED: {
            return {...state, isLoadingMedia: false, isSuccessMedia: true, isErrorMedia: false,successMessageMedia:'URL Received',errorMessageMedia:'',mediaUrl:action.payload.data.mediaUrl,playerUrl:action.payload.data.playerUrl}
        }

        case ACTION_TYPE.GET_MEDIA_DATA_REJECTED: {
            return {...state, isLoadingMedia: false, isSuccessMedia: false,isErrorMedia: true,successMessageMedia:'', errorMessageMedia:action.payload.error }
        }

        case ACTION_TYPE.GET_MEDIA_RETURN_INITIAL_STATE: {
            return {...state, isLoadingMedia: false, isSuccessMedia: false, isErrorMedia: false,successMessageMedia:'',errorMessageMedia:'',mediaUrl:'',playerUrl:''}
        }


        
        
        case ACTION_TYPE.UPDATE_LIKE_PENDING:{
            return {...state, isLoadingMedia: true, isSuccessMedia: false,  isErrorMedia: false,successMessageMedia:'',errorMessageMedia:''}
        }

        case ACTION_TYPE.UPDATE_LIKE_FULFILLED: {
            return {...state, isLoadingMedia: false, isSuccessMedia: true, isErrorMedia: false,successMessageMedia:'Like Received',errorMessageMedia:'',like:Number(action.payload.data.like)}
        }

        case ACTION_TYPE.UPDATE_LIKE_REJECTED: {
            return {...state, isLoadingMedia: false, isSuccessMedia: false,isErrorMedia: true,successMessageMedia:'', errorMessageMedia:action.payload.error }
        }

        case ACTION_TYPE.UPDATE_LIKE_N_INITIAL_STATE: {
            return {...state, isLoadingMedia: false, isSuccessMedia: false, isErrorMedia: false,successMessageMedia:'',errorMessageMedia:'',like:0,}
        }

        default: {
            return state;
        }
    }

}

export default dataReducer;
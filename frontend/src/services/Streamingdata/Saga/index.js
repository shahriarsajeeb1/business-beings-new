import { put,call,takeLatest } from 'redux-saga/effects'
import { ACTION_TYPE } from '../../../Constant'
import { getStreamDataApi,getMediaDataApi,updateLikeApi} from  './api'

function* workerGetStreamData(){

    yield put({type:ACTION_TYPE.GET_STREAM_DATA_PENDING})

    try {
        const data = yield call(getStreamDataApi);
        yield put({type:ACTION_TYPE.GET_STREAM_DATA_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.GET_STREAM_DATA_REJECTED,payload:{error:error}})
    }
}

function* workerGetMediaData(action){

    yield put({type:ACTION_TYPE.GET_MEDIA_DATA_PENDING})

    try {
        const data = yield call(getMediaDataApi,action.payload.mediaId);
        yield put({type:ACTION_TYPE.GET_MEDIA_DATA_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.GET_MEDIA_DATA_REJECTED,payload:{error:error}})
    }
}

function* workerUpdateLike(){

    yield put({type:ACTION_TYPE.UPDATE_LIKE_PENDING})

    try {
        const data = yield call(updateLikeApi);
        yield put({type:ACTION_TYPE.UPDATE_LIKE_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.UPDATE_LIKE_REJECTED,payload:{error:error}})
    }
}



export function* watchGetStreamData(){
    yield takeLatest(ACTION_TYPE.GET_STREAM_DATA_SAGA,workerGetStreamData)
}

export function* watchGetMediaData(){
    yield takeLatest(ACTION_TYPE.GET_MEDIA_DATA_SAGA,workerGetMediaData)
}

export function* watchUpdateLike(){
    yield takeLatest(ACTION_TYPE.UPDATE_LIKE_SAGA,workerUpdateLike)
}
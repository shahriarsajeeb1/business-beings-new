import {axio} from '../../../axios'


export const getStreamDataApi = () => {
    return (
        axio.get('/stream/streamdata',{timeout:30000})
    )
}

export const getMediaDataApi = (mediaId) => {
    return (
        axio.post('/stream/mediadata',{mediaId:mediaId})
    )
}

export const updateLikeApi = () => {
    return (
        axio.post('/stream/like')
    )
}


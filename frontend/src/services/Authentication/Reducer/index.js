import { ACTION_TYPE } from "../../../Constant";

const initState = {
    isLoading: false,
    isSuccess: false, 
    isError: false,
    errorMessage:'', 
    successMessage:'', 
    isUser: false, 
    newUser:false,
    token:'',
};

const authenticateReducer = (state=initState, action) => {
    switch(action.type) {

        case ACTION_TYPE.FIND_USER_PENDING:{
            return {...state, isLoading: true, isSuccess: false,  isError: false}
        }

        case ACTION_TYPE.FIND_USER_FULFILLED: {
            return {...state, isLoading: false, isSuccess: true, isError: false,errorMessage:'',isUser:action.payload.data.isUser,newUser:action.payload.data.newUser}
        }

        case ACTION_TYPE.FIND_USER_REJECTED: {
            return {...state, isLoading: false, isSuccess: false,isError: true,errorMessage:action.payload.error }
        }



        case ACTION_TYPE.LOGIN_USER_PENDING:{
            return {...state, isLoading: true, isSuccess: false,  isError: false}
        }

        case ACTION_TYPE.LOGIN_USER_FULFILLED: {
            return {...state, isLoading: false, isSuccess: true, isError: false,errorMessage:'',token:action.payload.data.token}
        }

        case ACTION_TYPE.LOGIN_USER_REJECTED: {
            return {...state, isLoading: false, isSuccess: false,isError: true,errorMessage:action.payload.error }
        }        
        
        case ACTION_TYPE.AUTH_RETURN_INITIAL_STATE: {
            return {...state, isLoading: false,isSuccess: false,isError: false,errorMessage:'',successMessage:'',isUser: false,newUser:false}
        } 

        case ACTION_TYPE.AUTH_RETURN_INITIAL_STATE_LOGOUT: {
            return {...initState}
        } 
        

        case ACTION_TYPE.UPDATE_AUTH_TOKEN_FROM_CREATE_USER: {
            return {...state,token:action.payload.token,}
        } 
        

     default: {
            return state;
        }
    }
};

export default authenticateReducer;
import {axio} from '../../../axios'

export const findUserApi = (mobile) => { 
    return (
        axio.post('/user/finduser',{mobile:mobile})
    )
}

export const loginUserApi = (mobile,password) => {
    return (
        axio.post('/user/loginuser',{mobile:mobile,password:password})
    )
}
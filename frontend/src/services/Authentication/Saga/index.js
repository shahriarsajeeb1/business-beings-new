import { put,call,takeLatest } from 'redux-saga/effects'
import { ACTION_TYPE } from '../../../Constant'
import {findUserApi,loginUserApi} from './api'

function* workerFindUser(action){
    yield put({type:ACTION_TYPE.FIND_USER_PENDING})

    try {
        const data = yield call(findUserApi,action.payload.mobile);
        yield put({type:ACTION_TYPE.FIND_USER_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.FIND_USER_REJECTED,payload:{error:error}})
    }
}

function* workerLoginUser(action){
    yield put({type:ACTION_TYPE.LOGIN_USER_PENDING})

    try {
        const data = yield call(loginUserApi,action.payload.mobile);
        yield put({type:ACTION_TYPE.LOGIN_USER_FULFILLED,payload:{data:data.data}})
        yield put({type:ACTION_TYPE.GET_USER_SAGA,payload:{token:data.data.token}})
    } catch (error) {
        yield put({type:ACTION_TYPE.LOGIN_USER_REJECTED,payload:{error:error}})
    }
}


export function* watchFindUser(){
    yield takeLatest(ACTION_TYPE.FIND_USER_SAGA,workerFindUser)
}

export function* watchLoginUser(){
    yield takeLatest(ACTION_TYPE.LOGIN_USER_SAGA,workerLoginUser)
}
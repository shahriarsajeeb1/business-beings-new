import { ACTION_TYPE } from "../../../Constant";

export const updateAuthTokenFromCreateUser = (token) => ({
   type: ACTION_TYPE.UPDATE_AUTH_TOKEN_FROM_CREATE_USER,
   payload:{
       token:token,
   },
   
}
)

export const findUser = (mobile) => ({
   type: ACTION_TYPE.FIND_USER_SAGA,
   payload:{
       mobile:mobile,
   },
   
}
)

export const loginUser = (mobile,password) => ({
    type: ACTION_TYPE.LOGIN_USER_SAGA,
    payload:{
        mobile:mobile,
        password:password,
    },
    
 }
 )

 export const logOutAuth = () => ({
    type: ACTION_TYPE.AUTH_RETURN_INITIAL_STATE_LOGOUT,
      
 }
 )

 export const authInitialState = () => ({
    type: ACTION_TYPE.AUTH_RETURN_INITIAL_STATE,      
 }
 )

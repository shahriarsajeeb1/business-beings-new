import { ACTION_TYPE } from "../../../Constant";

export const getAllUserAdmin = ()=>({
    type:ACTION_TYPE.GET_ALL_USER_ADMIN_SAGA,   
})

export const createUserAdmin = (userData)=>({
    type:ACTION_TYPE.CREATE_USER_ADMIN_SAGA,
    payload:{
        userData:userData,
    }
})

export const updatedUserAdmin = (updatedData)=>({
    type:ACTION_TYPE.UPDATED_USER_ADMIN_SAGA,
    payload:{
        updatedData:updatedData,
    }
})

export const deleteUserAdmin = (userId)=>({
    type:ACTION_TYPE.DELETE_USER_ADMIN_SAGA,
    payload:{
        userId:userId
    }    
})

export const getReturnInitialStateAdmin = ()=>({
    type:ACTION_TYPE.GET_RETURN_INITIAL_STATE_ADMIN,     
})

export const createReturnInitialStateAdmin = ()=>({
    type:ACTION_TYPE.CREATE_RETURN_INITIAL_STATE_ADMIN,     
})

export const updateReturnInitialStateAdmin = ()=>({
    type:ACTION_TYPE.UPDATED_RETURN_INITIAL_STATE_ADMIN,     
})


export const deleteReturnInitialStateAdmin = ()=>({
    type:ACTION_TYPE.DELETE_RETURN_INITIAL_STATE_ADMIN,     
})




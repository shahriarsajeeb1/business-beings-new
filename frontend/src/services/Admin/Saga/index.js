import { put,call,takeLatest } from 'redux-saga/effects'
import { ACTION_TYPE } from '../../../Constant'
import {updatedUserAdminApi,createUserAdminApi,getAllUserAdminApi,deleteUserAdminApi} from './api'

function* workerCreateUserAdmin(action){
    yield put({type:ACTION_TYPE.CREATE_USER_ADMIN_PENDING})

    try {
        const data = yield call(createUserAdminApi,action.payload.userData);
        yield put({type:ACTION_TYPE.CREATE_USER_ADMIN_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.CREATE_USER_ADMIN_REJECTED,payload:{error:error}})
    }
}

function* workerGetAllUserAdmin(action){
    yield put({type:ACTION_TYPE.GET_ALL_USER_ADMIN_PENDING})
    
    try {
        const data = yield call(getAllUserAdminApi);
        yield put({type:ACTION_TYPE.GET_ALL_USER_ADMIN_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.GET_ALL_USER_ADMIN_REJECTED,payload:{error:error}})
    }
}

function* workerUpdatedUserAdmin(action){
    yield put({type:ACTION_TYPE.UPDATED_USER_ADMIN_PENDING})

    try {
        const data = yield call(updatedUserAdminApi,action.payload.updatedData);
        yield put({type:ACTION_TYPE.UPDATED_USER_ADMIN_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.UPDATED_USER_ADMIN_REJECTED,payload:{error:error}})
    }
}

function* workerDeleteUserAdmin(action){
    yield put({type:ACTION_TYPE.DELETE_USER_ADMIN_PENDING})

    try {
        const data = yield call(deleteUserAdminApi,action.payload.userId);
        yield put({type:ACTION_TYPE.DELETE_USER_ADMIN_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.DELETE_USER_ADMIN_REJECTED,payload:{error:error}})
    }
}

export function* watchCreateUserAdmin(){
    yield takeLatest(ACTION_TYPE.CREATE_USER_ADMIN_SAGA,workerCreateUserAdmin)
}

export function* watchGetAllUserAdmin(){
    yield takeLatest(ACTION_TYPE.GET_ALL_USER_ADMIN_SAGA,workerGetAllUserAdmin)
}

export function* watchUpdatedUserAdmin(){
    yield takeLatest(ACTION_TYPE.UPDATED_USER_ADMIN_SAGA,workerUpdatedUserAdmin)
}

export function* watchDeleteUserAdmin(){
    yield takeLatest(ACTION_TYPE.DELETE_USER_ADMIN_SAGA,workerDeleteUserAdmin)
}
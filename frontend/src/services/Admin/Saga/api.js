import {axio} from '../../../axios'


export const createUserAdminApi = (userData) => {
    return (
        axio.post('/admin/createuseradmin',{userData:userData})
    )
}

export const getAllUserAdminApi = () => {
    return (
        axio.get('/admin/getalluseradmin')
    )
}

export const updatedUserAdminApi = (updatedData) => {
    return (
        axio.put('/admin/updateuseradmin',{updatedData:updatedData})
    )
}

export const deleteUserAdminApi = (userId) => {
    return (
        axio.delete('/admin/deleteuseradmin',{params:{id:userId}})
    )
}
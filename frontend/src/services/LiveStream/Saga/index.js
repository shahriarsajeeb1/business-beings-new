import { put,call,takeLatest } from 'redux-saga/effects'
import { ACTION_TYPE } from '../../../Constant'
import {updatedLiveStreamAdminApi,createLiveStreamAdminApi,getAllLiveStreamAdminApi,deleteLiveStreamAdminApi} from './api'

function* workerCreateLiveStreamAdmin(action){
    yield put({type:ACTION_TYPE.CREATE_LIVESTREAM_ADMIN_PENDING})

    try {
        const data = yield call(createLiveStreamAdminApi,action.payload.liveStreamData);
        yield put({type:ACTION_TYPE.CREATE_LIVESTREAM_ADMIN_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.CREATE_LIVESTREAM_ADMIN_REJECTED,payload:{error:error}})
    }
}

function* workerGetAllLiveStreamAdmin(action){
    yield put({type:ACTION_TYPE.GET_ALL_LIVESTREAM_ADMIN_PENDING})
    
    try {
        const data = yield call(getAllLiveStreamAdminApi);
        yield put({type:ACTION_TYPE.GET_ALL_LIVESTREAM_ADMIN_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.GET_ALL_LIVESTREAM_ADMIN_REJECTED,payload:{error:error}})
    }
}

function* workerUpdatedLiveStreamAdmin(action){
    yield put({type:ACTION_TYPE.UPDATED_LIVESTREAM_ADMIN_PENDING})

    try {
        
        const data = yield call(updatedLiveStreamAdminApi,action.payload.updatedData);
        yield put({type:ACTION_TYPE.UPDATED_LIVESTREAM_ADMIN_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.UPDATED_LIVESTREAM_ADMIN_REJECTED,payload:{error:error}})
    }
}

function* workerDeleteLiveStreamAdmin(action){
    yield put({type:ACTION_TYPE.DELETE_LIVESTREAM_ADMIN_PENDING})

    try {
        const data = yield call(deleteLiveStreamAdminApi,action.payload.id);
        yield put({type:ACTION_TYPE.DELETE_LIVESTREAM_ADMIN_FULFILLED,payload:{data:data.data}})
    } catch (error) {
        yield put({type:ACTION_TYPE.DELETE_LIVESTREAM_ADMIN_REJECTED,payload:{error:error}})
    }
}

export function* watchCreateLiveStreamAdmin(){
    yield takeLatest(ACTION_TYPE.CREATE_LIVESTREAM_ADMIN_SAGA,workerCreateLiveStreamAdmin)
}

export function* watchGetAllLiveStreamAdmin(){
    yield takeLatest(ACTION_TYPE.GET_ALL_LIVESTREAM_ADMIN_SAGA,workerGetAllLiveStreamAdmin)
}


export function* watchUpdatedLiveStreamAdmin(){
    yield takeLatest(ACTION_TYPE.UPDATED_LIVESTREAM_ADMIN_SAGA,workerUpdatedLiveStreamAdmin)
}

export function* watchDeleteLiveStreamAdmin(){
    yield takeLatest(ACTION_TYPE.DELETE_LIVESTREAM_ADMIN_SAGA,workerDeleteLiveStreamAdmin)
}
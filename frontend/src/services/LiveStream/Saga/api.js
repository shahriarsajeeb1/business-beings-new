import {axio} from '../../../axios'


export const createLiveStreamAdminApi = (liveStreamData) => {
    return (
        axio.post('/livestream/createlivestreamadmin',{liveStreamData:liveStreamData})
    )
}

export const getAllLiveStreamAdminApi = () => {
    return (
        axio.get('/livestream/getalllivestreamadmin')
    )
}

export const updatedLiveStreamAdminApi = (updatedData) => {
    return (
        axio.put('/livestream/updatelivestreamadmin',{updatedData:updatedData})
    )
}

export const deleteLiveStreamAdminApi = (id) => {
    return (
        axio.delete('/livestream/deletelivestreamadmin',{data:{id:id}})
    )
}
import { ACTION_TYPE } from "../../../Constant";

export const getAllLiveStreamAdmin = ()=>({
    type:ACTION_TYPE.GET_ALL_LIVESTREAM_ADMIN_SAGA,   
})

export const createLiveStreamAdmin = (createData)=>({
    type:ACTION_TYPE.CREATE_LIVESTREAM_ADMIN_SAGA,
    payload:{
        liveStreamData:createData,
    }
})

export const updatedLiveStreamAdmin = (updatedData)=>({
    type:ACTION_TYPE.UPDATED_LIVESTREAM_ADMIN_SAGA,
    payload:{
        updatedData:updatedData,
    }
})

export const deleteLiveStreamAdmin = (id)=>({
    type:ACTION_TYPE.DELETE_LIVESTREAM_ADMIN_SAGA,
    payload:{
        id:id
    }    
})

export const getLiveStreamReturnInitialStateAdmin = ()=>({
    type:ACTION_TYPE.GET_LIVESTREAM_RETURN_INITIAL_STATE_ADMIN,     
})

export const createLiveStreamReturnInitialStateAdmin = ()=>({
    type:ACTION_TYPE.CREATE_LIVESTREAM_RETURN_INITIAL_STATE_ADMIN,     
})

export const updateLiveStreamReturnInitialStateAdmin = ()=>({
    type:ACTION_TYPE.UPDATED_LIVESTREAM_RETURN_INITIAL_STATE_ADMIN,     
})


export const deleteLiveStreamReturnInitialStateAdmin = ()=>({
    type:ACTION_TYPE.DELETE_LIVESTREAM_RETURN_INITIAL_STATE_ADMIN,     
})




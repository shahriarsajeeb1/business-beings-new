import { ACTION_TYPE } from "../../../Constant";

const initialState = {
  themeBlack: false,
  modalOpen: false,
  drawerOpen: false,
  rightDrawerOpen: false,
  invitedFriend:'',
  mobile:0,
  error: "",

  
};

const styleReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TYPE.UPDATE_BLACK_THEME:
      return {
        ...state,
        themeBlack: action.payload.themeBlack,
      };

    case ACTION_TYPE.UPDATE_MODAL_OPEN:
      return {
        ...state,
        modalOpen: action.payload.modalOpen,
      };

    case ACTION_TYPE.UPDATE_DRAWER_OPEN:
      return {
        ...state,
        drawerOpen: action.payload.drawerOpen,
      };

      case ACTION_TYPE.UPDATE_RIGHT_DRAWER_OPEN:
      return {
        ...state,
        rightDrawerOpen: action.payload.drawerOpen,
      };

      case ACTION_TYPE.UPDATE_INVITED_FRIEND:
      return {
        ...state,
        invitedFriend: action.payload.id,
      };

      case ACTION_TYPE.UPDATE_MOBILE_LOCALLY:
      return {
        ...state,
        mobile: action.payload.mobile,
      };




    // case ACTION_TYPE.UPDATE_INVITED_FRIEND_PENDING: {
    //   return {
    //     ...state,
    //     isLoadingUpdate: true,
    //     isSuccessUpdate: false,
    //     isErrorUpdate: false,
    //     successMessageUpdate: "",
    //     errorMessageUpdate: "",
    //   };
    // }

    // case ACTION_TYPE.UPDATE_INVITED_FRIEND_FULFILLED: {
    //   return {
    //     ...state,
    //     isLoadingUpdate: false,
    //     isSuccessUpdate: true,
    //     isErrorUpdate: false,
    //     successMessageUpdate: action.payload.data.message,
    //     errorMessageUpdate: "",
    //     invitedFriendId:action.payload.data.id,
    //   };
    // }

    // case ACTION_TYPE.UPDATE_INVITED_FRIEND_REJECTED: {
    //   return {
    //     ...state,
    //     isLoadingUpdate: false,
    //     isSuccessUpdate: false,
    //     isErrorUpdate: true,
    //     successMessageUpdate: "",
    //     errorMessageUpdate: action.payload.error,
    //   };
    // }
    
    default:
      return state;
  }
};

export default styleReducer;

import { ACTION_TYPE } from '../../../Constant'

export const updateBlackTheme = (theme)=>({
    type:ACTION_TYPE.UPDATE_BLACK_THEME,
    payload:{
        themeBlack:theme,
    }
})

export const updateModalOpen = (open)=>({
    type:ACTION_TYPE.UPDATE_MODAL_OPEN,
    payload:{
        modalOpen:open,
    }
})

export const updateDrawerOpen = (open)=>({
    type:ACTION_TYPE.UPDATE_DRAWER_OPEN,
    payload:{
        drawerOpen:open,
    }
})

export const updateRightDrawerOpen = (open)=>({
    type:ACTION_TYPE.UPDATE_RIGHT_DRAWER_OPEN,
    payload:{
        drawerOpen:open,
    }
})

export const updateInvitedFriend = (id)=>({
    type:ACTION_TYPE.UPDATE_INVITED_FRIEND, 
    payload:{
        id:id,
    }  
})

export const updateMobileLocally = (mobile)=>({
    type:ACTION_TYPE.UPDATE_MOBILE_LOCALLY, 
    payload:{
        mobile:mobile,
    }  
})



 
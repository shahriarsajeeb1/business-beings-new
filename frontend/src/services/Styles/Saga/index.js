// import { call, put, takeLatest } from "redux-saga/effects";
// import { ACTION_TYPE } from "../../../Constant";
// import { getInvitedFriendApi } from "./api";

// function* workerGetInvitedFriend() {
//   yield put({ type: ACTION_TYPE.GET_INVITED_FRIEND_PENDING });

//   try {
//     const data = yield call(
//       getInvitedFriendApi,
//     );
//     yield put({
//       type: ACTION_TYPE.GET_INVITED_FRIEND_FULFILLED,
//       payload: { data: data.data },
//     });
//   } catch (error) {
//     yield put({
//       type: ACTION_TYPE.GET_INVITED_FRIEND_REJECTED,
//       payload: { error: error },
//     });
//   }
// }

// export function* watchGetInvitedFriend() {
//   yield takeLatest(
//     ACTION_TYPE.GET_INVITED_FRIEND_SAGA,
//     workerGetInvitedFriend
//   );
// }

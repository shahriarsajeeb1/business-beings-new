import React,{useEffect} from "react";
import { CssBaseline, ThemeProvider,Hidden } from "@material-ui/core";
import { useSelector,useDispatch } from "react-redux";

import theme from "./Theme";
import { BrowserRouter as Router, Switch, Route,} from "react-router-dom";
import themeBlack from "./BlackTheme";

import Home from "./Pages/Home";

import SearchPage from "./Pages/Search";
import About from "./Pages/About";
import Refund from "./Pages/Refund";
import Privacy from "./Pages/Privacy";
import Terms from "./Pages/Terms";
import Login from './Pages/Login'
import Signup from "./Pages/Signup";
import UserHome from "./Pages/userPage/UserHome";
import UserProfile from "./Pages/userPage/UserProfile";
import Ideas from "./Pages/Ideas";
import CaseStudy from "./Pages/CaseStudy";
import Interviews from "./Pages/Interviews";
import AdminLayout from "./Pages/Admin";
import Detail from "./Pages/DetailPage";
import LiveStreamDetail from "./Pages/LiveStreamDetailPage";
import { getUser } from "./services/Userdata/action";
import ProtectedRoute from "./Components/ProtectedRoute";
// import UnprotectedRoute from "./Components/UnprotectedRoute";
// import DetailProtectedRoute from "./Components/DetailProtectedRoute";

import AdminRoute from "./Components/AdminRoute";
import { getAllPlanAdmin } from './services/PlanData/action'
import { authInitialState, updateAuthTokenFromCreateUser } from './services/Authentication/action'
import { getStreamData } from "./services/Streamingdata/action";
import Services from "./Pages/Services";
import Header from './Components/Header'
import Footer from './Components/Footer'
import Contact from "./Pages/Contact";
import { Helmet } from "react-helmet";


function App() {
  const dispatch = useDispatch()
  const auth = useSelector((state) => state.auth);
  const style = useSelector((state) => state.style);  
  const user = useSelector(state => state.user)

  useEffect(() => {
    dispatch(getStreamData());
    if (user.token){
        dispatch(updateAuthTokenFromCreateUser(user.token))
    }   
    if (auth.token){
      dispatch(getUser(auth.token));
    }
    if(!style.modalOpen){      
      dispatch(authInitialState());
    }
    if(auth.token || style.mobile){
      dispatch(getAllPlanAdmin());
    }
    
  }, [style.modalOpen,style.mobile,user.token,auth.token])
  

  return (
   
    <ThemeProvider theme={style.themeBlack ? themeBlack : theme}>
      <CssBaseline />
      <Router>
      <Header/>
      <Helmet>
        <title>Business Beings - Get Business Ideas Based on Your Budget and Interests</title>
        <meta name='description' content='Business Beings is a world’s first ever video streaming platform for business minded people who can explore business ideas in their regional languages'/>
        <meta name='keywords' content='business ideas, business ideas in tamil, small business ideas in tamil, online business ideas in tamil, How to start a business, business ideas list'/>
      </Helmet>
        <Switch>
          <Route path="/" component={Home} exact />
          <Route path="/invite/:id" component={Home}/>
          <Route path="/login" component={Login} exact />
          <ProtectedRoute path="/user" component={UserHome}/>
          <ProtectedRoute path="/profile" component={UserProfile}/>
          {/* <UnprotectedRoute path="/signup" component={Signup}/>  */}
           {/* <DetailrotectedRoute path="/detail/:id" component={Detail}/>  */}
          <Route path="/signup" component={Signup}/> 
          <Route path="/detail/:id" component={Detail}/>
          <Route path="/livestreamdetail/:id" component={LiveStreamDetail}/>
          <Route path="/search" component={SearchPage}/>
          <Route path="/services" component={Services}/>
          <Route path="/about" component={About}/>
          <Route path="/contact" component={Contact}/>
          <Route path="/refund" component={Refund}/>
          <Route path="/privacy" component={Privacy}/>
          <Route path="/terms" component={Terms}/> 
          <Route path="/ideas" component={Ideas}/> 
          <Route path="/casestudy" component={CaseStudy}/> 
          <Route path="/interviews" component={Interviews}/>   
          <AdminRoute path="/admin" component={AdminLayout}/>       
        </Switch>
        <Hidden smDown>
      <div>
        <Footer />
      </div>
      </Hidden>
      </Router>
    </ThemeProvider>
  );
}

export default App;

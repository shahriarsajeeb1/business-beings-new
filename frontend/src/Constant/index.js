export const ACTION_TYPE = {
    UPDATE_BLACK_THEME:'update_black_theme',
    UPDATE_MODAL_OPEN:'update_modal_open',    
    UPDATE_DRAWER_OPEN:'update_drawer_open',
    UPDATE_RIGHT_DRAWER_OPEN:'update_right_drawer_open',
    UPDATE_INVITED_FRIEND:'update_invited_friend',
    UPDATE_MOBILE_LOCALLY:'update_mobile_locally',
    UPDATE_AUTH_TOKEN_FROM_CREATE_USER:'update_auth_token_from_create_user',
    
    GET_STREAM_DATA_SAGA:'get_stream_data_saga',
    GET_STREAM_DATA_PENDING:'get_stream_data_pending',
    GET_STREAM_DATA_FULFILLED:'get_stream_data_fullfilled',
    GET_STREAM_DATA_REJECTED:'get_stream_data_rejected',
    GET_STREAM_RETURN_INITIAL_STATE:'get_stream_return_initial_state',    

    GET_MEDIA_DATA_SAGA:'get_media_data_saga',
    GET_MEDIA_DATA_PENDING:'get_media_data_pending',
    GET_MEDIA_DATA_FULFILLED:'get_media_data_fullfilled',
    GET_MEDIA_DATA_REJECTED:'get_media_data_rejected',
    GET_MEDIA_RETURN_INITIAL_STATE:'get_media_return_initial_state',

    UPDATE_LIKE_SAGA:'update_like_saga',
    UPDATE_LIKE_PENDING:'update_like_pending',
    UPDATE_LIKE_FULFILLED:'update_like_fullfilled',
    UPDATE_LIKE_REJECTED:'update_like_rejected',
    UPDATE_LIKE_RETURN_INITIAL_STATE:'update_like_return_initial_state',
    
    FIND_USER_SAGA:'find_user_saga',
    FIND_USER_PENDING:'find_user_pending',
    FIND_USER_FULFILLED:'find_user_fullfilled',
    FIND_USER_REJECTED:'find_user_rejected',
    FIND_AUTH_RETURN_INITIAL_STATE:'find_auth_return_initial_state',    

    LOGIN_USER_SAGA:'login_user_saga',
    LOGIN_USER_PENDING:'login_user_pending',
    LOGIN_USER_FULFILLED:'login_user_fullfilled',
    LOGIN_USER_REJECTED:'login_user_rejected',
    LOGIN_AUTH_RETURN_INITIAL_STATE:'login_auth_return_initial_state',

    AUTH_RETURN_INITIAL_STATE:'auth_return_initial_state',
    AUTH_RETURN_INITIAL_STATE_LOGOUT:'auth_return_initial_state_logout',

    CREATE_USER_SAGA:'create_user_data_saga',
    CREATE_USER_PENDING:'create_user_data_pending',
    CREATE_USER_FULFILLED:'create_user_data_fullfilled',
    CREATE_USER_REJECTED:'create_user_data_rejected',
    CREATE_RETURN_INITIAL_STATE_USER:'create_return_initial_state_user',

    GET_USER_SAGA:'get_user_saga',
    GET_USER_PENDING:'get_user_pending',
    GET_USER_FULFILLED:'get_user_fullfilled',
    GET_USER_REJECTED:'get_user_rejected',
    GET_RETURN_INITIAL_STATE_USER:'get_return_initial_state_user',
    

    UPDATED_USER_SAGA:'update_user_data_saga',
    UPDATED_USER_PENDING:'update_user_data_pending',
    UPDATED_USER_FULFILLED:'update_user_data_fullfilled',
    UPDATED_USER_REJECTED:'update_user_data_rejected',
    UPDATED_RETURN_INITIAL_STATE_USER:'update_return_initial_state_user',

    DELETE_USER_SAGA:'delete_user_data_saga',
    DELETE_USER_PENDING:'delete_user_data_pending',
    DELETE_USER_FULFILLED:'delete_user_data_fullfilled',
    DELETE_USER_REJECTED:'delete_user_data_rejected',
    DELETE_RETURN_INITIAL_STATE_USER:'delete_return_initial_state_user',

    SEND_SERVICE_FORM_USER_SAGA:'send_service_form_user_data_saga',
    SEND_SERVICE_FORM_USER_PENDING:'send_service_form_user_data_pending',
    SEND_SERVICE_FORM_USER_FULFILLED:'send_service_form_user_data_fullfilled',
    SEND_SERVICE_FORM_USER_REJECTED:'send_service_form_user_data_rejected',
    SEND_SERVICE_FORM_RETURN_INITIAL_STATE_USER:'send_service_form_return_initial_state_user',

    SEND_CONTACT_FORM_USER_SAGA:'send_contact_form_user_data_saga',
    SEND_CONTACT_FORM_USER_PENDING:'send_contact_form_user_data_pending',
    SEND_CONTACT_FORM_USER_FULFILLED:'send_contact_form_user_data_fullfilled',
    SEND_CONTACT_FORM_USER_REJECTED:'send_contact_form_user_data_rejected',
    SEND_CONTACT_FORM_RETURN_INITIAL_STATE_USER:'send_contact_form_return_initial_state_user',CONACTSER_RETURN_INITIAL_STATE_USER:'user_return_initial_state_user',

    USER_RETURN_INITIAL_STATE_USER:'return_initial_state_user',

    GET_ALL_USER_ADMIN_SAGA:'get_all_user_admin_saga',
    GET_ALL_USER_ADMIN_PENDING:'get_all_user_admin_pending',
    GET_ALL_USER_ADMIN_FULFILLED:'get_all_user_admin_fullfilled',
    GET_ALL_USER_ADMIN_REJECTED:'get_all_user_admin_rejected',
    GET_RETURN_INITIAL_STATE_ADMIN:'get_return_initial_state_admin',

    CREATE_USER_ADMIN_SAGA:'create_user_data_admin_saga',
    CREATE_USER_ADMIN_PENDING:'create_user_data_admin_pending',
    CREATE_USER_ADMIN_FULFILLED:'create_user_data_admin_fullfilled',
    CREATE_USER_ADMIN_REJECTED:'create_user_data_admin_rejected',   
    CREATE_RETURN_INITIAL_STATE_ADMIN:'create_return_initial_state_admin',

    UPDATED_USER_ADMIN_SAGA:'update_user_data_admin_saga',
    UPDATED_USER_ADMIN_PENDING:'update_user_data_admin_pending',
    UPDATED_USER_ADMIN_FULFILLED:'update_user_data_admin_fullfilled',
    UPDATED_USER_ADMIN_REJECTED:'update_user_data_admin_rejected',
    UPDATED_RETURN_INITIAL_STATE_ADMIN:'updated_return_initial_state_admin',

    DELETE_USER_ADMIN_SAGA:'delete_user_data_admin_saga',
    DELETE_USER_ADMIN_PENDING:'delete_user_data_admin_pending',
    DELETE_USER_ADMIN_FULFILLED:'delete_user_data_admin_fullfilled',
    DELETE_USER_ADMIN_REJECTED:'delete_user_data_admin_rejected',
    DELETE_RETURN_INITIAL_STATE_ADMIN:'delete_return_initial_state_admin',

    RETURN_INITIAL_STATE_ADMIN:'return_initial_state_admin',

    GET_ALL_PLAN_ADMIN_SAGA:'get_all_plan_admin_saga',
    GET_ALL_PLAN_ADMIN_PENDING:'get_all_plan_admin_pending',
    GET_ALL_PLAN_ADMIN_FULFILLED:'get_all_plan_admin_fullfilled',
    GET_ALL_PLAN_ADMIN_REJECTED:'get_all_plan_admin_rejected',
    GET_PLAN_RETURN_INITIAL_STATE_ADMIN:'get_plan_return_initial_state_admin',

    CREATE_PLAN_ADMIN_SAGA:'create_plan_admin_saga',
    CREATE_PLAN_ADMIN_PENDING:'create_plan_admin_pending',
    CREATE_PLAN_ADMIN_FULFILLED:'create_plan_ddmin_fullfilled',
    CREATE_PLAN_ADMIN_REJECTED:'create_plan_admin_rejected',   
    CREATE_PLAN_RETURN_INITIAL_STATE_ADMIN:'create_plan_return_initial_state_admin',

    UPDATED_PLAN_ADMIN_SAGA:'update_plan_admin_saga',
    UPDATED_PLAN_ADMIN_PENDING:'update_plan_admin_pending',
    UPDATED_PLAN_ADMIN_FULFILLED:'update_plan_ddmin_fullfilled',
    UPDATED_PLAN_ADMIN_REJECTED:'update_plan_admin_rejected',
    UPDATED_PLAN_RETURN_INITIAL_STATE_ADMIN:'updated_plan_return_initial_state_admin',

    DELETE_PLAN_ADMIN_SAGA:'delete_plan_admin_saga',
    DELETE_PLAN_ADMIN_PENDING:'delete_plan_admin_pending',
    DELETE_PLAN_ADMIN_FULFILLED:'delete_plan_admin_fullfilled',
    DELETE_PLAN_ADMIN_REJECTED:'delete_plan_admin_rejected',
    DELETE_PLAN_RETURN_INITIAL_STATE_ADMIN:'delete_plan_return_initial_state_admin',

    PLAN_RETURN_INITIAL_STATE_ADMIN:'plan_return_initial_state_admin',

    GET_ALL_LIVESTREAM_ADMIN_SAGA:'get_all_livestream_admin_saga',
    GET_ALL_LIVESTREAM_ADMIN_PENDING:'get_all_livestream_admin_pending',
    GET_ALL_LIVESTREAM_ADMIN_FULFILLED:'get_all_livestream_admin_fullfilled',
    GET_ALL_LIVESTREAM_ADMIN_REJECTED:'get_all_livestream_admin_rejected',
    GET_LIVESTREAM_RETURN_INITIAL_STATE_ADMIN:'get_livestream_return_initial_state_admin',

    CREATE_LIVESTREAM_ADMIN_SAGA:'create_livestream_admin_saga',
    CREATE_LIVESTREAM_ADMIN_PENDING:'create_livestream_admin_pending',
    CREATE_LIVESTREAM_ADMIN_FULFILLED:'create_livestream_admin_fullfilled',
    CREATE_LIVESTREAM_ADMIN_REJECTED:'create_livestream_admin_rejected',   
    CREATE_LIVESTREAM_RETURN_INITIAL_STATE_ADMIN:'create_livestream_return_initial_state_admin',

    UPDATED_LIVESTREAM_ADMIN_SAGA:'update_livestream_admin_saga',
    UPDATED_LIVESTREAM_ADMIN_PENDING:'update_livestream_admin_pending',
    UPDATED_LIVESTREAM_ADMIN_FULFILLED:'update_livestream_ddmin_fullfilled',
    UPDATED_LIVESTREAM_ADMIN_REJECTED:'update_livestream_admin_rejected',
    UPDATED_LIVESTREAM_RETURN_INITIAL_STATE_ADMIN:'updated_livestream_return_initial_state_admin',

    DELETE_LIVESTREAM_ADMIN_SAGA:'delete_livestream_admin_saga',
    DELETE_LIVESTREAM_ADMIN_PENDING:'delete_livestream_admin_pending',
    DELETE_LIVESTREAM_ADMIN_FULFILLED:'delete_livestream_admin_fullfilled',
    DELETE_LIVESTREAM_ADMIN_REJECTED:'delete_livestream_admin_rejected',
    DELETE_LIVESTREAM_RETURN_INITIAL_STATE_ADMIN:'delete_livestream_return_initial_state_admin',

    LIVESTREAM_RETURN_INITIAL_STATE_ADMIN:'livestream_return_initial_state_admin',

    CREATE_PAYMENT_ORDER_SAGA:'create_payment_order_saga',
    CREATE_PAYMENT_ORDER_PENDING:'create_payment_order_pending',
    CREATE_PAYMENT_ORDER_FULFILLED:'create_payment_order_fullfilled',
    CREATE_PAYMENT_ORDER_REJECTED:'create_payment_order_rejected',   
    CREATE_PAYMENT_ORDER_INITIAL_STATE:'create_payment_order_initial_state',

    VERIFY_PAYMENT_SAGA:'verify_payment_saga',
    VERIFY_PAYMENT_PENDING:'verify_payment_pending',
    VERIFY_PAYMENT_FULFILLED:'verify_payment_fullfilled',
    VERIFY_PAYMENT_REJECTED:'verify_payment_rejected',
    VERIFY_PAYMENT_INITIAL_STATE:'verify_payment_initial_state',

    PAYMENT_INITIAL_STATE:'payment_initial_state',


}

export const END_POINTS = {
    FIND_USER:'/user/finduser',
    LOGIN_USER:'/loginuser',

}

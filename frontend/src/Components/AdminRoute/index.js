import React from 'react'
import { useSelector } from 'react-redux'
import { Route, Redirect } from "react-router-dom";


const AdminRoute = ({component:Component,...rest}) => {

    const user = useSelector(state => state.user)     

    return (
        <Route
        {...rest}
        render={(props)=>{
            if(user.user.admin){
                return <Component {...props}/>
            }
            else{                
                return (
                    <Redirect to={{
                        pathname:'/',
                    }}/>
                )
            }
        }}/>       
    )
}

export default AdminRoute

import React from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Avatar,
  makeStyles,
  Paper,
  TextField,
  InputAdornment,
  Typography,
  Divider,
  Hidden,
  IconButton,
} from "@material-ui/core";
import { Link } from "react-router-dom";
import SearchIcon from "@material-ui/icons/Search";
import Home from "../icons/Home.js";
import Logo from "../icons/Logo.js";
import Collections from "../icons/Collections.js";
import Idea from "../icons/Idea.js";
import Engineering from "../icons/Engineering.js";
import MenuIcon from "@material-ui/icons/Menu";
import { updateDrawerOpen } from "../../services/Styles/action";
import Drawering from "../Drawer";
import UserHome from "../../Pages/userPage/UserHome/index.js";
import UserProfile from "../../Pages/userPage/UserProfile/index.js";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "row",
    width: "100%",
    height: "100%",
    minHeight: "100vh",
    alignItems: "stretch",
    justifyContent: "stretch",
  },
  sideBar: {
    display: "flex",
    flexDirection: "column",
    width: "calc(80px + (100 - 80) * ((100vw - 350px) / (2000 - 350)))",
    alignItems: "center",
    justifyContent: "flex-start",
    zIndex: 2,
    // border: '2px solid yellow'
  },
  logo: {
    width: "calc(50px + (80 - 50) * ((100vw - 350px) / (2000 - 350)))",
    height: "calc(50px + (80 - 50) * ((100vw - 350px) / (2000 - 350)))",
  },
  sideBarItems: {
    display: "flex",
    flexDirection: "column",
    width: "calc(80px + (100 - 80) * ((100vw - 350px) / (2000 - 350)))",
    height: "calc(80px + (100 - 80) * ((100vw - 350px) / (2000 - 350)))",
    margin: "calc(7px + (10 - 7) * ((100vw - 350px) / (2000 - 350)))",
    alignItems: "center",
    justifyContent: "center",
  },
  sideBarItemIcon: {
    width: "calc(30px + (45 - 30) * ((100vw - 350px) / (2000 - 350)))",
    height: "calc(30px + (45 - 30) * ((100vw - 350px) / (2000 - 350)))",
    margin: "calc(10px + (15 - 10) * ((100vw - 350px) / (2000 - 350)))",
    color: theme.palette.text.secondary,
  },
  contentContainer: {
    display: "flex",
    flexDirection: "column",
    flex: 1,
    alignItems: "stretch",
    justifyContent: "stretch",
    // border: '2px solid purple'
  },
  divider: {
    width: "calc(30px + (45 - 30) * ((100vw - 350px) / (2000 - 350)))",
  },
  link: {
    textDecoration: "none",
  },
  topBar: {
    display: "flex",
    flexDirection: "row",
    height: "calc(75px + (100 - 75) * ((100vw - 350px) / (2000 - 350)))",
    alignItems: "center",
    justifyContent: "space-between",
    // border: '2px solid green'
  },
  input: {
    width: "calc(100px + (700 - 100) * ((100vw - 350px) / (2000 - 350)))",
    margin: "calc(30px + (70 - 30) * ((100vw - 350px) / (2000 - 350)))",
  },
  avatar: {
    margin: "calc(30px + (70 - 30) * ((100vw - 350px) / (2000 - 350)))",
  },
  smallTopBar: {
    display: "flex",
    flexDirection: "row",
    height: "calc(75px + (100 - 75) * ((100vw - 350px) / (2000 - 350)))",
    alignItems: "center",
    justifyContent: "flex-start",
    textTransform: "uppercase",
  },
  smallIconBack: {
    margin: "calc(30px + (70 - 30) * ((100vw - 350px) / (2000 - 350)))",
  },
  mainContainer: {
    flex: 1,
    // border: '2px solid purple'
  },
}));

function UserLayout({ children }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const user = useSelector((state) => state.user);

  const toggleDrawer = (open) => (event) => {
    dispatch(updateDrawerOpen(open));
  };

  return (
    <>
      <div className={classes.root}>
        <Hidden xsDown>
          <Paper elevation={2} className={classes.sideBar}>
            <Link to="/" className={classes.link}>
              <div className={classes.sideBarItems}>
                <Logo className={classes.sideBarItemIcon} />
              </div>
            </Link>
            <Link to="/" className={classes.link}>
              <div className={classes.sideBarItems}>
                <Home className={classes.sideBarItemIcon} />
                <Typography varient="h6" color="textSecondary">
                  Home
                </Typography>
              </div>
            </Link>
            <Divider className={classes.divider} />
            <Link to="/ideas" className={classes.link}>
              <div className={classes.sideBarItems}>
                <Idea className={classes.sideBarItemIcon} />
                <Typography varient="h6" color="textSecondary">
                  Ideas
                </Typography>
              </div>
            </Link>
            <Divider className={classes.divider} />
            <Link to="/casestudy" className={classes.link}>
              <div className={classes.sideBarItems}>
                <Collections className={classes.sideBarItemIcon} />
                <Typography varient="h6" color="textSecondary">
                  Case Study
                </Typography>
              </div>
            </Link>
            <Divider className={classes.divider} />
            <Link to="/interviews" className={classes.link}>
              <div className={classes.sideBarItems}>
                <Idea className={classes.sideBarItemIcon} />
                <Typography varient="h6" color="textSecondary">
                  Interviews
                </Typography>
              </div>
            </Link>
            <Divider className={classes.divider} />
            <Link to="" className={classes.link}>
              <div className={classes.sideBarItems}>
                <Engineering className={classes.sideBarItemIcon} />
                <Typography varient="h6" color="textSecondary">
                  Services
                </Typography>
              </div>
            </Link>
            <Divider className={classes.divider} />
          </Paper>
        </Hidden>
        <div className={classes.contentContainer}>
          <Hidden xsDown>
            <Paper elevation={2} className={classes.topBar}>
              <TextField
                className={classes.input}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start">
                      <SearchIcon />
                    </InputAdornment>
                  ),
                }}
              />
              <Link to="/profile">
                <Avatar className={classes.avatar} src={user.image} />
              </Link>
            </Paper>
          </Hidden>
          <Hidden smUp>
            <Paper elevation={2} className={classes.smallTopBar}>
              <IconButton onClick={toggleDrawer(true)} aria-label="menu">
                <MenuIcon />
              </IconButton>
              <Drawering />
              <Typography varient="h6">Settings</Typography>
            </Paper>
          </Hidden>
          <div className={classes.mainContainer}>{children}</div>
        </div>
      </div>
    </>
  );
}

export default UserLayout;

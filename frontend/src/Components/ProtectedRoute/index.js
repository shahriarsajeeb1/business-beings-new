import React from 'react'
import { useSelector,useDispatch } from 'react-redux'
import { Route, Redirect } from "react-router-dom";
import { updateModalOpen } from '../../services/Styles/action';

const ProtectedRoute = ({component:Component,...rest}) => {

    const user = useSelector(state => state.user)
    const dispatch = useDispatch()
    console.log(user.user._id)

    return (
        <Route
        {...rest}
        render={(props)=>{
            if(user.user._id){
                return <Component {...props}/>
            }
            else{
                dispatch(updateModalOpen(true));
                return (
                    <Redirect to={{
                        pathname:'/',
                    }}/>
                )
            }
        }}/>       
    )
}

export default ProtectedRoute

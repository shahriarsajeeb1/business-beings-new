import React, { useState } from "react";
import Radio from "@material-ui/core/Radio";
import RadioGroup from "@material-ui/core/RadioGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormControl from "@material-ui/core/FormControl";
import FormLabel from "@material-ui/core/FormLabel";
import InputLabel from "@material-ui/core/InputLabel";
import NativeSelect from "@material-ui/core/NativeSelect";
import FormHelperText from "@material-ui/core/FormHelperText";
import { Divider, Hidden, makeStyles } from "@material-ui/core";
import { Link } from "react-router-dom";
import { useSelector } from "react-redux";
import Carding from "../Card";

const usestyle = makeStyles((theme) => ({
  container: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexWrap: "wrap",
  },
  type: {
    minWidth: "250px",
    flex: 1,
    [theme.breakpoints.up("lg")]: {
      display: "flex",
      justifyContent: "space-around",
      alignItems: "center",
    },
  },
  link: {
    textDecoration: "none",
    // textTransform: "uppercase",
    color: theme.palette.type == 'light'? theme.palette.common.black :theme.palette.common.white,
    fontSize: theme.typography.subtitle2.fontSize,
    " &:hover": {
      color: theme.palette.common.black,
    },
  },
  VideoContainer: {
    width: "100%",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    flexWrap: "wrap",
    marginTop: theme.spacing(3),
    [theme.breakpoints.down("sm")]: {
      marginTop: theme.spacing(3),
    },
  },
}));

const Filter = ({ type }) => {
  const [value, setValue] = useState({
    type: type || "All",
    budget: "None",
    category: "None",
    interest: "None",
  });
  const [budgetDisable, setDisable] = useState(false);
  const data = useSelector((state) => state.data.streams);
  const classes = usestyle();

  const handleChange = (event) => {
    setValue({ ...value, [event.target.name]: event.target.value });
    if (value.type == "Case" || value.type == "Interviews") {
      // setDisable(true);
    }
  };
  //   useEffect(() => {
  //     if (value.type === "All") {

  //     }
  //   }, [value]);

  return (
    <>
      <div className={classes.container}>
        <div className={classes.type}>
          <FormControl component="fieldset">
            <FormLabel shrink component="legend">
              Type
            </FormLabel>
            <RadioGroup
              aria-label="Type"
              name="type"
              value={value.type}
              onChange={handleChange}
            >
              <div>
                <FormControlLabel value="All" control={<Radio />} label="All" />

                <Link to="/ideas" className={classes.link}>
                  <FormControlLabel
                    value="Ideas"
                    control={<Radio />}
                    label="Ideas"
                  />
                </Link>
              </div>
              <div>
                <Link to="/casestudy" className={classes.link}>
                  <FormControlLabel
                    value="Case"
                    control={<Radio />}
                    label="Case Study"
                  />
                </Link>
                <Link to="/interviews" className={classes.link}>
                  <FormControlLabel
                    value="Interviews"
                    control={<Radio />}
                    label="Interviews"
                  />
                </Link>
              </div>
            </RadioGroup>
          </FormControl>
          <Hidden mdDown>
            <Divider orientation="vertical" flexItem />
          </Hidden>
        </div>
        <div className={classes.type}>
          <FormControl disabled={budgetDisable}>
            <InputLabel shrink>Budget</InputLabel>
            <NativeSelect
              value={value.budget}
              onChange={handleChange}
              inputProps={{
                name: "budget",
              }}
            >
              <option value="None">None</option>
              <option value="0-25k">0-25k</option>
              <option value="25k-50k">25k-50k</option>
              <option value="50k-100k">50k-100k</option>
              <option value="100k-500k">100k-500k</option>
              <option value="500k-1000k">500k-1000k</option>
              <option value="Above 1000k">Above 1000k</option>
            </NativeSelect>
            <FormHelperText>Enter your Budget</FormHelperText>
          </FormControl>
          <Hidden mdDown>
            <Divider orientation="vertical" flexItem />
          </Hidden>{" "}
        </div>
        <div className={classes.type}>
          <FormControl>
            <InputLabel shrink>Category</InputLabel>
            <NativeSelect
              value={value.category}
              onChange={handleChange}
              inputProps={{
                name: "category",
              }}
            >
              <option value="None">None</option>
              <option value="Manufacturing">Manufacturing</option>
              <option value="Wholesale">Wholesale</option>
              <option value="Retail">Retail</option>
              <option value="Franchise">Franchise</option>
              <option value="Online Business">Online Business</option>
              <option value="Homemade">Homemade</option>
              <option value="Service">Service</option>
              <option value="Distributor">Distributor</option>
              <option value="Trading">Trading</option>
            </NativeSelect>
            <FormHelperText>Enter the Category of Business</FormHelperText>
          </FormControl>
          <Hidden mdDown>
            <Divider orientation="vertical" flexItem />
          </Hidden>{" "}
        </div>
        <div className={classes.type}>
          <FormControl>
            <InputLabel>Interest</InputLabel>
            <NativeSelect
              value={value.interest}
              onChange={handleChange}
              inputProps={{
                name: "interest",
              }}
            >
              <option value="None">None</option>
              <option value="Food business">Food business</option>
              <option value="Pet business">Pet business</option>
              <option value="Farming">Farming</option>
              <option value="Import/Export">Import/Export</option>
              <option value="Textile">Textile</option>
              <option value="Logistics">Logistics</option>
              <option value="Education">Education</option>
              <option value="Energy">Energy</option>
              <option value="Livestock">Livestock</option>
              <option value="Pharmaceutical">Pharmaceutical</option>
              <option value="Electronics">Electronics</option>
              <option value="Electrical">Electrical</option>
              <option value="Apparel">Apparel</option>
              <option value="Packaging">Packaging</option>
              <option value="AI / Automation">AI / Automation</option>
            </NativeSelect>
            <FormHelperText>Enter your area of Interest</FormHelperText>
          </FormControl>
        </div>
      </div>

      <div className={classes.VideoContainer}>
        {data.map((dat) => {
          switch (value.type) {
            case "All": {
              if (
                (value.budget == dat.params.budget || value.budget == "None") &&
                (value.category == dat.params.category ||
                  value.category == "None") &&
                (value.interest == dat.params.interest ||
                  value.interest == "None")
              ) {
                return <Carding stream={dat} />;
              }
            }

            case "Ideas": {
              if (
                value.type == dat.params.type &&
                (value.budget == dat.params.budget || value.budget == "None") &&
                (value.category == dat.params.category ||
                  value.category == "None") &&
                (value.interest == dat.params.interest ||
                  value.interest == "None")
              ) {
                return <Carding stream={dat} />;
              }
            }

            case "Case": {
              if (
                value.type == dat.params.type &&
                (value.category == dat.params.category ||
                  value.category == "None") &&
                (value.interest == dat.params.interest ||
                  value.interest == "None")
              ) {
                return <Carding stream={dat} />;
              }
            }

            case "Interviews": {
              if (
                value.type == dat.params.type &&
                (value.category == dat.params.category ||
                  value.category == "None") &&
                (value.interest == dat.params.interest ||
                  value.interest == "None")
              ) {
                return <Carding stream={dat} />;
              }
            }
          }
        })}
      </div>
    </>
  );
};

export default Filter;

import React, { useState } from "react";
import { Link,useHistory } from "react-router-dom";
import {
  Card,
  ListItemText,
  List,
  Typography,
  FormControlLabel,
  Switch,
  makeStyles,
  Drawer,
  Avatar,
  Divider,
  MenuItem,
  Hidden,
  } from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { updateRightDrawerOpen } from "../../services/Styles/action";
import { logOutAuth } from "../../services/Authentication/action";
import { userReturnInitialState } from "../../services/Userdata/action";
import SettingsIcon from "@material-ui/icons/Settings";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import PersonIcon from "@material-ui/icons/Person";
import SupervisorAccountIcon from "@material-ui/icons/SupervisorAccount";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";
import PinterestIcon from "@material-ui/icons/Pinterest";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import WhatsAppIcon from "@material-ui/icons/WhatsApp";
import FileCopyIcon from '@material-ui/icons/FileCopy';

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",   
    alignItems: "center",
    padding: "30px 20px 0 20px",
    width: "calc(200px + (400 - 200) * ((100vw - 350px) / (2000 - 350)))",
  },
  avatar: {
    width: theme.spacing(7),
    height: theme.spacing(7),
    margin: "10px 0 10px 0",
  },
  item: {
    marginTop: "5px",
    marginLeft: "10px",
    " &:hover": {
      color: theme.palette.common.black,
    },
  },
  divider: {
    width: "160px",
    margin: "10px 0 10px 0",
  },
  card: {
    width: "calc(180px + (350 - 180) * ((100vw - 350px) / (2000 - 350)))",
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
    justifyContent: "flex-start",
  },
  link: {
    textDecoration: "none",
    textTransform: "uppercase",
    color:
      theme.palette.type == "light"
        ? theme.palette.text.secondary
        : theme.palette.common.white,
    fontSize: theme.typography.subtitle2.fontSize,
    " &:hover": {
      color: theme.palette.common.black,
    },
  },
  menu: {
    marginTop: "0px",
    paddingBottom: "2px",
    fontSize: theme.typography.subtitle2.fontSize,
    [theme.breakpoints.down("sm")]: {
      paddingTop: 0,
      paddingBottom: 0,
      minHeight: "25px",
    },
  },
  socialIconsWrapper: {
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
    width: "calc(150px + (300 - 150) * ((100vw - 350px) / (2000 - 350)))",
    margin: "calc(12px + (25 - 12) * ((100vw - 350px) / (2000 - 350)))",
  },
  iconsAnimation: {
    transition: "500ms",
    cursor: "pointer",
    "& :hover": {
      transform: "scale(1.1)",
    },
  },
  inviteLike:{
    textAlign:'left',
    overflow:'hidden',
    width:'90%',
    height:'auto',
    textOverflow: 'ellipsis',
    border:`2px solid ${theme.palette.text.hint}`,
    borderRadius:'5px',
    position:'relative',
  },
  copyIcon:{
    position:'absolute',    
    right:0,top:0,bottom:0,
    paddingLeft:'5px',
    paddingRight:'5px',
    backgroundColor:theme.palette.common.white,
    color:theme.palette.text.hint,
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
  }
}));

const RightDrawer = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory()
  const style = useSelector((state) => state.style);
  const user = useSelector((state) => state.user.user);
  const [open, setOpen] = React.useState(false);
  const postUrl = `https://businessbeings.com/register/${user._id}`;
  const postTitle = "Join Business Beings to explore business ideas based on your budget and interest, using my referral link ";
  const postImg = "http://placehold.it/400x400&text=image1";

  const toggleDrawer = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    dispatch(updateRightDrawerOpen(open));
  };

  const handleLogout = () => {
    localStorage.removeItem("persist:root");
    localStorage.removeItem("persist:auth");
    dispatch(logOutAuth());
    dispatch(userReturnInitialState());
    history.push("/home")
  };

  const copyToClipboard = () => {   
    const ta = document.createElement("textarea");
    ta.innerText = `www.businessbeings.com/invite/${user._id}`    
    document.body.appendChild(ta);
    ta.select();
    document.execCommand("copy");    
    ta.remove();
  };

  return (
    <Drawer
      anchor="right"
      onClose={toggleDrawer(false)}
      open={style.rightDrawerOpen}
    >
      <div
        className={classes.root}
        role="presentation"
        onClick={toggleDrawer(false)}
        onKeyDown={toggleDrawer(false)}
      >
        <div>
          <Avatar src={user.image} className={classes.avatar} />
          <Typography
            style={{ textTransform: "uppercase" }}
            variant="subtitle1"
          >
            {user.name}
          </Typography>
        </div>
        <Divider className={classes.divider} />
        <Card className={classes.card} style={{ alignItems: "center" }}>
          <Hidden smDown>
            <Typography variant="h6" color="textSecondary">
              Invite Friends and start earning!{" "}
            </Typography>
          </Hidden>
          <Hidden mdUp>
            <Typography
              variant="subtitle1"
              color="textSecondary"
              style={{ marginLeft: "10px", alignItems: "flex-start" }}
            >
              Invite Friends and start earning!{" "}
            </Typography>
          </Hidden>
          <Hidden smDown>
            <Typography variant="h6" color="textSecondary">
              Link
            </Typography>
          </Hidden>
          <Hidden mdUp>
            <Typography
              variant="subtitle1"
              color="textSecondary"
              style={{ marginLeft: "10px", alignItems: "flex-start" }}
            >
              Link
            </Typography>
          </Hidden>

          <div className={classes.inviteLike}
          >
            www.businessbeings.com/invite/{user._id}
            <div onClick={copyToClipboard} className={classes.copyIcon}><FileCopyIcon style={{width:'15px',height:'15px'}}/></div>
          </div>

          <div className={classes.socialIconsWrapper}>
            <a
              className={classes.iconsAnimation}
              href={`https://www.facebook.com/sharer.php?u=${postUrl}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <FacebookIcon style={{ color: "#3b5998" }} />
            </a>
            <a
              className={classes.iconsAnimation}
              href={`https://twitter.com/share?url=${postUrl}&text=${postTitle}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <TwitterIcon style={{ color: "#1da1f2" }} />
            </a>
            <a
              className={classes.iconsAnimation}
              href={`https://pinterest.com/pin/create/bookmarklet/?media=${postImg}&url=${postUrl}&description=${postTitle}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <PinterestIcon style={{ color: "#0077b5" }} />
            </a>
            <a
              className={classes.iconsAnimation}
              href={`https://www.linkedin.com/shareArticle?url=${postUrl}&title=${postTitle}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <LinkedInIcon style={{ color: "#bd081c" }} />
            </a>
            <a
              className={classes.iconsAnimation}
              href={`https://wa.me/?text=${postTitle} ${postUrl}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <WhatsAppIcon style={{ color: "#25d366" }} />
            </a>
          </div>
        </Card>
        <Divider className={classes.divider} />
        <Card className={classes.card}>
          {user.admin && (
            <div className={classes.item} style={{ marginTop: '10px' }}>
              <Link to="/admin" className={classes.link}>
                <MenuItem className={classes.menu}>
                  <SupervisorAccountIcon style={{ marginRight: "10px" }} />
                  Admin
                </MenuItem>
              </Link>
            </div>
          )}
          <div className={classes.item}>
            <Link to="/user" className={classes.link}>
              <MenuItem className={classes.menu}>
                <PersonIcon style={{ marginRight: "10px" }} />
                Profile
              </MenuItem>
            </Link>
          </div>
          <div className={classes.item}>
            <Link to="/profile" className={classes.link}>
              <MenuItem className={classes.menu}>
                <SettingsIcon style={{ marginRight: "10px" }} />
                My account
              </MenuItem>
            </Link>
          </div>
          <div className={classes.item} style={{ marginBottom: '10px' }}>
            <Link to="/profile" className={classes.link}>
              <MenuItem className={classes.menu} onClick={handleLogout}>
                <ExitToAppIcon style={{ marginRight: "10px" }} /> Logout
              </MenuItem>
            </Link>
          </div>
        </Card>
        <Divider className={classes.divider} />
      </div>
    </Drawer>
  );
};

export default RightDrawer;

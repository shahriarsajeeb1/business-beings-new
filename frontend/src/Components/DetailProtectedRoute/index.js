import React from 'react'
import { useSelector,useDispatch } from 'react-redux'
import { Route, Redirect } from "react-router-dom";
import { updateModalOpen } from '../../services/Styles/action';

const DetailProtectedRoute = ({component:Component,...rest}) => {

    const user = useSelector(state => state.user) 
    const data = useSelector(state => state.data)
     
    const dispatch = useDispatch()    

    return (
        <Route
        {...rest}
        render={(props)=>{
            if(data.streams && user.user._id){
                return <Component {...props}/>
            }
            else{
                dispatch(updateModalOpen(true));
                return (
                    <Redirect to={{
                        pathname:'/',
                    }}/>
                )
            }
        }}/>       
    )
}

export default DetailProtectedRoute

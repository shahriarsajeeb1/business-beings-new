import { IconButton, makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import YouTubeIcon from '@material-ui/icons/YouTube';
import InstagramIcon from '@material-ui/icons/Instagram';
import { Link } from 'react-router-dom';

const useStyle = makeStyles((theme)=>({
    container:{
        width: '100%',
        paddingLeft:'2.7vw',
        paddingBottom:'0.7vw',
        paddingTop:'1vw',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: 'grey',
        color: 'white'
        
    },
    desc:{
        
        display: 'flex',
        flexDirection: 'column',
        
    },
    desc1:{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        
    },
    item:{
        marginRight:'calc(30px + (45 - 30) * ((100vw - 350px) / (2000 - 350)))',
    },
    desc2:{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding:'12px'
    },
    social:{
        flex:0.3,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'center',
        
    },
    social1:{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    social2:{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    link: {
        textDecoration: "none",        
        color:theme.palette.common.white,
        fontSize: theme.typography.subtitle1.fontSize,
        
      },

}))

const Footer = () => {
    const classes = useStyle();

    return (
        <div className={classes.container}>
            <div className={classes.desc}>
                <div className={classes.desc1}>
                <Link to='/about' className={classes.link}><Typography className={classes.item}  variant='subtitle1'>About us </Typography></Link>
                <Link to='/contact' className={classes.link}><Typography className={classes.item} variant='subtitle1'>Contact </Typography></Link>
                <Link to='/privacy' className={classes.link}><Typography className={classes.item} variant='subtitle1'>Privacy Policy </Typography></Link>
                <Link to='/refund' className={classes.link}><Typography className={classes.item} variant='subtitle1'>Refund policy </Typography></Link>
                <Link to='/terms' className={classes.link}><Typography className={classes.item} variant='subtitle1'>Terms Of Use </Typography></Link>
                </div>
                <div className={classes.desc2}>
                © 2021 BusinessBeings. All Rights Reserved.
                </div>
            </div>
            <div className={classes.social}>
                <div className={classes.social1}>
                <Typography variant='subtitle1'>Contact  Us</Typography>
                </div>
                <div className={classes.social2}>
                    <a href='https://www.youtube.com/c/BusinessBeings' target="_blank">
                    <IconButton color='primary'>
                        <YouTubeIcon/>
                    </IconButton>
                    </a>
                    <a href='https://www.instagram.com/businessbeingsofficial/' target="_blank">
                    <IconButton color='primary'>
                        <InstagramIcon/>
                    </IconButton>
                    </a>
                   
                </div>
            </div>
           
        </div>
    )
}

export default Footer

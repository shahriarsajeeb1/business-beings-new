import * as React from "react";

function SvgManageSearch(props) {
  return (
    <svg
      width={60}
      height={60}
      viewBox='0 0 60 60'
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M17.5 22.5H5v-5h12.5v5zm0 7.5H5v5h12.5v-5zm33.975 17.5L41.9 37.925C39.9 39.225 37.55 40 35 40c-6.9 0-12.5-5.6-12.5-12.5S28.1 15 35 15s12.5 5.6 12.5 12.5c0 2.55-.775 4.9-2.075 6.875l9.575 9.6-3.525 3.525zm-8.975-20c0-4.125-3.375-7.5-7.5-7.5s-7.5 3.375-7.5 7.5S30.875 35 35 35s7.5-3.375 7.5-7.5zM5 47.5h25v-5H5v5z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgManageSearch;

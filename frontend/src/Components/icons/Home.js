import * as React from "react";

function SvgHome(props) {
  return (
    <svg
      width={48}
      height={48}
      viewBox='0 0 48 48'
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path d="M20 40V28h8v12h10V24h6L24 6 4 24h6v16h10z" fill="currentColor" />
    </svg>
  );
}

export default SvgHome;

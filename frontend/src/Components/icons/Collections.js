import * as React from "react";

function SvgCollections(props) {
  return (
    <svg
      width={72}
      height={80}
      viewBox='0 0 72 80'
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M24.3 32h-4.5v34.125c0 2.681 2.025 4.875 4.5 4.875h31.5v-4.875H24.3V32z"
        fill="currentColor"
      />
      <path
        d="M61.088 21H33.413c-2.537 0-4.613 2.194-4.613 4.875v29.25c0 2.681 2.076 4.875 4.613 4.875h27.675c2.536 0 4.612-2.194 4.612-4.875v-29.25c0-2.681-2.076-4.875-4.612-4.875zm0 24.375l-5.766-3.656-5.766 3.656v-19.5h11.532v19.5z"
        fill="currentColor"
      />
    </svg>
  );
}

export default SvgCollections;

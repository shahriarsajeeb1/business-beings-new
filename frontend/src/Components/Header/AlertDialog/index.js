import React,{useState} from 'react';
import {useSelector,useDispatch } from 'react-redux'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { getUser } from '../../../services/Userdata/action';
import { createPaymentOrderInitialState, verifyPaymentIntialState } from '../../../services/Payment/action';
import { updateModalOpen } from '../../../services/Styles/action';




const AlertDialogInfo = React.forwardRef((props,ref) => {
  const [open, setOpen] = useState(props.view); 
  const dispatch = useDispatch();
  const auth = useSelector(state => state.auth)  

  const handleClose = () => {
     
    if(props.alertClose){
      props.alertClose(false)
      setOpen(false);
      dispatch(updateModalOpen(true))
      return;
    }
    dispatch(getUser(auth.token));
    setOpen(false);
    props.modalClose(false) 
    dispatch(createPaymentOrderInitialState())
    dispatch(verifyPaymentIntialState())
  };

  return (
     
      <Dialog
        open={open}
        onClose={handleClose}        
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        ref={ref}      >
        
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {props.dialogContent}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    
  );
})
export default AlertDialogInfo
import React, { useState,useEffect,useRef } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Avatar,
  Toolbar,
  Button,
  Drawer,
  Hidden,
  TextField,
  InputAdornment,
  IconButton,
  ListItem,
  ListItemText,
  List,
  Typography,
  FormControlLabel,
  Switch,
  makeStyles,
} from "@material-ui/core";
import Logo from "../icons/Logo";
import { Link, useHistory } from "react-router-dom";
import MenuIcon from "@material-ui/icons/Menu";
import SearchIcon from "@material-ui/icons/Search";
import { updateBlackTheme, updateDrawerOpen,updateModalOpen, updateRightDrawerOpen } from "../../services/Styles/action";
import Drawering from "../Drawer";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import AlertDialogInfo from "./AlertDialog";
import UpgradeModal from "./SubscribeModal";
import { logOutAuth } from '../../services/Authentication/action'
import { userReturnInitialState } from '../../services/Userdata/action'
import RightDrawer from "../RightDrawer";

const usestyle = makeStyles((theme) => ({
  root: {
    height: "calc(70px + (100 - 70) * ((100vw - 350px) / (2000 - 350)))",
    alignItems: "center",
    justifyContent: "space-around",
  },
  logo: {
    width: "calc(50px + (80 - 50) * ((100vw - 350px) / (2000 - 350)))",
    height: "calc(50px + (80 - 50) * ((100vw - 350px) / (2000 - 350)))",
    marginLeft: "calc(80px + (96 - 80) * ((100vw - 350px) / (2000 - 350)))",
    marginRight: "calc(80px + (96 - 80) * ((100vw - 350px) / (2000 - 350)))",
  },
  link: {
    textDecoration: "none",
    textTransform: "uppercase",
    color: theme.palette.type == 'light'? theme.palette.text.secondary :theme.palette.common.white,
    fontSize: theme.typography.subtitle2.fontSize,
    " &:hover": {
      color: theme.palette.common.black,
    },
  },
  linkPopper: {
    textDecoration: "none",
    // textTransform: "uppercase",
    color: theme.palette.type == 'light'? theme.palette.text.secondary :theme.palette.common.white,
    fontSize: theme.typography.subtitle2.fontSize,
    " &:hover": {
      color: theme.palette.common.black,
    },
  },
  input: {
    width: "calc(150px + (150 - 100) * ((100vw - 350px) / (2000 - 350)))",
    marginLeft: "calc(50px + (80 - 50) * ((100vw - 350px) / (2000 - 350)))",
    color: theme.palette.type == 'light'? theme.palette.text.secondary :theme.palette.common.white,
    fontSize: theme.typography.subtitle2.fontSize,
    " &:hover": {
      color: theme.palette.common.black,
    },
   
  },
  subscribe: {
    color: theme.palette.common.white,
  },
  switch: {
    color: theme.palette.type == 'light'? theme.palette.text.secondary :theme.palette.common.white,
    textTransform: "uppercase",
  },
  popper:{
    zIndex:55,
  }
}));


const Header = () => {
  const classes = usestyle();
  const dispatch = useDispatch();  
  const history = useHistory();
  const style = useSelector((state) => state.style);
  const user = useSelector(state => state.user.user)
  const payment = useSelector(state => state.payment)
  const [modalOpen, setModalOpen] = useState(false)
  const [loginCheck, setLoginCheck] = useState(false)

  
  const [theme, setTheme] = useState(false);
  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleToggle = () => {
    // setOpen((prevOpen) => !prevOpen);
    dispatch(updateRightDrawerOpen(true));
  };

  const handleModal = (view) => {
    if(user._id){
      setModalOpen(view);
    }else{
      setLoginCheck(true)
    }
  }

  const loginCheckAlertClose = () => {
    setLoginCheck(false)
  }
  

  const onClickClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  const handleLogout = ()=>{
    localStorage.removeItem('persist:root');
    localStorage.removeItem('persist:auth');
    dispatch(logOutAuth());
    dispatch(userReturnInitialState())        
    setOpen(false);
  }

  function handleListKeyDown(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = useRef(open);
  useEffect(() => {
    prevOpen.current = open;
  }, [open]);


  const handleTheme = () => {
    setTheme((theme) => !theme);
    dispatch(updateBlackTheme(theme));
  };

  const handleModalOpen = () => {
    history.push("/");
    dispatch(updateModalOpen(true))
}

  const toggleDrawer = (open) => (event) => {    
    dispatch(updateDrawerOpen(open));
  };


  
  return (
    <div>
    <Toolbar className={classes.root}>
      <Hidden mdUp>
        <IconButton onClick={toggleDrawer(true)} aria-label="menu">
          <MenuIcon />
        </IconButton>
        
        <Drawering/>
      </Hidden>
      <Hidden smDown>
        <Logo className={classes.logo} />
        <Link to="/" className={classes.link}>
          Home
        </Link>
        <Link to="/ideas" className={classes.link}>
          Ideas
        </Link>
        <Link to="/casestudy" className={classes.link}>
          Case Studies
        </Link>
        <Link to="/interviews" className={classes.link}>
          Interviews
        </Link>
        <Link to="/services" className={classes.link}>
          Services
        </Link>
          <Hidden mdDown>
        <Link to="/search">

      <TextField
        className={classes.input}
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          ),
        }}
        />
        </Link>
        </Hidden>
      </Hidden>
      <Button
        className={classes.subscribe}
        variant="contained"
        color="primary"
        disableElevation
        onClick={()=>handleModal(true)}
      >
        Subscribe
      </Button>
      {
        user._id? 
      
       <Avatar className={classes.avatar} src={user.image} ref={anchorRef} onClick={handleToggle}/>
     
       :<Button onClick={handleModalOpen} className={classes.link}>
        Login
      </Button>
      }
      <FormControlLabel
        className={classes.switch}
        control={
          <Switch
            checked={style.themeBlack}
            onChange={handleTheme}
            color="primary"
          />
        }
        label="Dark"
      />
    </Toolbar>
    <RightDrawer/>
    <Backdrop  className={classes.backdrop} open={payment.isLoadingCreate || payment.isLoadingVerify} >
        <CircularProgress color="inherit" onBackdropClick="false"/>
      </Backdrop>
      {modalOpen && <div> <UpgradeModal view={true} handleModal={handleModal}/></div>}
      {payment.isSuccessUpdate && <AlertDialogInfo modalClose={handleModal} view={true} dialogContent={'Transaction Successful.Account is Upgraded'}/>}
      {payment.isErrorUpdate && <AlertDialogInfo modalClose={handleModal} view={true} dialogContent={'Error Occured.Contact Admin.'} />}
      {loginCheck && <AlertDialogInfo view={true} alertClose={loginCheckAlertClose} dialogContent={'Please Login,or SignUp if you are a first time user. '} />}
     {/* <Popper className={classes.popper} open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
     {({ TransitionProps, placement }) => (
       <Grow
         {...TransitionProps}
         style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
       >
         <Paper>
           <ClickAwayListener onClickAway={onClickClose}>
             <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
              {user.admin && <Link to='/admin'className={classes.linkPopper}><MenuItem onClick={onClickClose}>Admin</MenuItem></Link>}
               <Link to='/user'className={classes.linkPopper}><MenuItem onClick={onClickClose}>Profile</MenuItem></Link>
               <Link to='/profile'className={classes.linkPopper}><MenuItem onClick={onClickClose}>My account</MenuItem></Link>
               <MenuItem className={classes.linkPopper} onClick={handleLogout}>Logout</MenuItem>
             </MenuList>
           </ClickAwayListener>
         </Paper>
       </Grow>
     )}
   </Popper> */}
   </div>
  );
};

export default Header;

import React from 'react'
import { useSelector,useDispatch } from 'react-redux'
import { Route, Redirect } from "react-router-dom";
import { updateModalOpen } from '../../services/Styles/action';

const UnprotectedRoute = ({component:Component,...rest}) => {

    const auth = useSelector(state => state.auth)   
    const style = useSelector(state => state.style)   
    const dispatch = useDispatch()    

    return (
        <Route
        {...rest}
        render={(props)=>{
            if(auth.newUser && style.mobile){
                return <Component {...props}/>
            }
            else{
                dispatch(updateModalOpen(true));
                return (
                    <Redirect to={{
                        pathname:'/',
                    }}/>
                )
            }
        }}/>       
    )
}

export default UnprotectedRoute

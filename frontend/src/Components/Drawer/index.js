import React from "react";
import { Link } from "react-router-dom";
import {  
  makeStyles,
  Drawer,  
  Divider,
} from "@material-ui/core";
import { useDispatch, useSelector } from "react-redux";
import { updateDrawerOpen } from "../../services/Styles/action";
import Logo from "../icons/Logo";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "200px",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    padding: "30px 20px 0 20px",
  },
  logo: {
    width: theme.spacing(7),
      height: theme.spacing(7),
      margin: '10px 0 10px 40px',
  },
  
  item:{
    marginTop:'20px',
    marginLeft:'10px',
    " &:hover": {
      color: theme.palette.common.black,
    },
  },
  divider:{
    width: "160px",
    margin: '10px 0 10px 0'
  },
  link: {
    textDecoration: "none",
    textTransform: "uppercase",
    color:
      theme.palette.type === "light"
        ? theme.palette.text.secondary
        : theme.palette.common.white,
    fontSize: theme.typography.subtitle2.fontSize,
    
  },
}));

const Drawering = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const style = useSelector((state) => state.style);  

  const toggleDrawer = (open) => (event) => {
    if (
      event.type === "keydown" &&
      (event.key === "Tab" || event.key === "Shift")
    ) {
      return;
    }
    dispatch(updateDrawerOpen(open));
  };

  

  return (
    <Drawer anchor="left" onClose={toggleDrawer(false)} open={style.drawerOpen}>
      <div className={classes.root}
       role="presentation"
      onClick={toggleDrawer(false)}
      onKeyDown={toggleDrawer(false)}>  
       <Logo className={classes.logo} />      
        <Divider className={classes.divider} />
        <div className={classes.item} style={{marginTop:0,}}>
          <Link to="/" className={classes.link}>
            Home
          </Link>
        </div>
        <div className={classes.item}>
          <Link to="/ideas" className={classes.link}>
            Ideas
          </Link>
        </div>
        <div className={classes.item}>
          <Link to="/casestudy" className={classes.link}>
            Case Studies
          </Link>
        </div>
        <div className={classes.item}>
          <Link to="/interviews" className={classes.link}>
            Interviews
          </Link>
        </div>
        <div className={classes.item}>
          <Link to="/search" className={classes.link}>
            Services
          </Link>
        </div>
        <Divider className={classes.divider} />
        <div className={classes.item} style={{marginTop:0,}}>
          <Link to="/about" className={classes.link}>
            About Us
          </Link>
        </div>
        <div className={classes.item}>
          <Link to="/contact" className={classes.link}>
          Contact
          </Link>
        </div>
        <div className={classes.item}>
          <Link to="/privacy" className={classes.link}>
          Privacy Policy
          </Link>
        </div>
        <div className={classes.item}>
          <Link to="/refund" className={classes.link}>
          Refund policy

          </Link>
        </div>
        <div className={classes.item}>
          <Link to="/terms" className={classes.link}>
          Terms Of Use
           </Link>
        </div>
        <Divider className={classes.divider} />
        <div className={classes.item} style={{marginTop:0,}}>
          <Link to="/terms" className={classes.link}>
          Connect with Us
           </Link>
        </div>
      </div>
    </Drawer>
  );
};

export default Drawering;

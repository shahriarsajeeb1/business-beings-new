import React, { useState } from 'react'
import { Button, Card, CardContent, CardMedia, makeStyles, Typography } from '@material-ui/core'
import { useHistory } from "react-router-dom";


const usestyles = makeStyles((theme)=>({
    card: {
        width: 'calc(100px + (500 - 100) * ((100vw - 350px) / (2000 - 350)))',        
        height: 'calc(75px + (300 - 75) * ((100vw - 350px) / (2000 - 350)))',
        borderRadius: '17px',
        position: 'relative',
        cursor: 'pointer', 
        margin:'0.7vw',       
        '&:hover': {
            border: '1px solid green',
            transform: 'scale(1.08)'
        }
    },
    image:{
        width: 'calc(100px + (500 - 100) * ((100vw - 350px) / (2000 - 350)))',
        height: 'calc(75px + (300 - 75) * ((100vw - 350px) / (2000 - 350)))',             
    },   

    cont: {
        width: '100%',
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',            
        alignItems: 'center',
        position: 'absolute',
        top: 0, bottom: 0, left: 0, right: 0,
        background: 'rgba(10, 9, 9, 0.65)',
        color: 'white',
        [theme.breakpoints.down('sm')]:{
            display:'none',
        }
        
    },
    desc: {
        textAlign: 'center',       
        maxHeight: '80px',        
        textOverflow:'ellipsis',
        overflow: 'hidden',
    }

}))


const Carding = (props) => {
    const {id,title, description,image } = props.stream
    const [visible,setVisible] = useState(false)
    const classes = usestyles();
    const history = useHistory();

    const visibilities = ()=>{
        setVisible(true)
    }
    const notVisibilities = ()=>{
        setVisible(false)
    }

    const onClickNavigation = ()=>{
        history.push(`/detail/${id}`);
    }

    return (
        <div>
            <Card onMouseEnter={visibilities} onMouseLeave={notVisibilities} onClick={onClickNavigation} className={classes.card} >
                <CardMedia component="img" className={classes.image} image={image} title={title}/>               
                {visible? <CardContent className={classes.cont}>
                    <Typography variant='h6'>
                        {title}
                    </Typography>
                    <Typography variant='subtitle1' className={classes.desc}>
                        {description}
                    </Typography>
                    <Button color='primary' variant='contained'>
                        Play Now
                    </Button>
                </CardContent>:null }              
            </Card>
        </div>
    )
}

export default Carding

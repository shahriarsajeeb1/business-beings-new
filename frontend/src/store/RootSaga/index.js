import { all } from "@redux-saga/core/effects";
import { watchFindUser,watchLoginUser } from "../../services/Authentication/Saga";
import { watchCreateUser,watchDeleteUser,watchGetUser,watchUpdatedUser,watchContactUser,watchServiceUser } from "../../services/Userdata/Saga";
import { watchCreateUserAdmin, watchDeleteUserAdmin, watchGetAllUserAdmin, watchUpdatedUserAdmin } from "../../services/Admin/Saga";
import { watchCreatePlanAdmin, watchDeletePlanAdmin, watchGetAllPlanAdmin, watchUpdatedPlanAdmin } from "../../services/PlanData/Saga";
import { watchCreateLiveStreamAdmin, watchDeleteLiveStreamAdmin, watchGetAllLiveStreamAdmin, watchUpdatedLiveStreamAdmin } from "../../services/LiveStream/Saga";
import { watchCreatePaymentOrder,watchVerifyPayment } from '../../services/Payment/Saga'
import { watchGetStreamData,watchGetMediaData,watchUpdateLike } from '../../services/Streamingdata/Saga'


export default function* rootSaga(){
    yield all([
        watchFindUser(),
        watchLoginUser(),
        watchCreateUser(),
        watchGetUser(),
        watchUpdatedUser(),
        watchDeleteUser(),
        watchCreateUserAdmin(),
        watchGetAllUserAdmin(),
        watchUpdatedUserAdmin(),
        watchDeleteUserAdmin(),
        watchCreatePlanAdmin(),
        watchGetAllPlanAdmin(),
        watchUpdatedPlanAdmin(),
        watchDeletePlanAdmin(),
        watchCreateLiveStreamAdmin(),
        watchGetAllLiveStreamAdmin(),
        watchUpdatedLiveStreamAdmin(),
        watchDeleteLiveStreamAdmin(),
        watchCreatePaymentOrder(),
        watchVerifyPayment(),
        watchGetStreamData(),
        watchGetMediaData(),
        watchUpdateLike(),
        watchContactUser(),
        watchServiceUser(),
    ])
};
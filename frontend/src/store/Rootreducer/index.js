import { combineReducers } from "redux";
import { ACTION_TYPE } from "../../Constant";
import adminReducer from "../../services/Admin/Reducer";
import authenticateReducer from "../../services/Authentication/Reducer";
import planReducer from "../../services/PlanData/Reducer";
import dataReducer from "../../services/Streamingdata/Reducer";
import styleReducer from "../../services/Styles/Reducer";
import userDataReducer from "../../services/Userdata/Reducer";
import liveStreamReducer from "../../services/LiveStream/Reducer"
import paymentReducer from "../../services/Payment/Reducer"
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const config = {
    key:'auth',
    storage,
    whitelist:['token'],
}

const appReducer = combineReducers(
    {
        data : dataReducer,
        user:userDataReducer,
        plan:planReducer,
        style:styleReducer,
        auth:persistReducer(config,authenticateReducer),
        admin:adminReducer,
        liveStream:liveStreamReducer,
        payment:paymentReducer,
    }
)



const rootReducer = (state,action)=>{ 
      
    return appReducer(state,action);
}


export default rootReducer;
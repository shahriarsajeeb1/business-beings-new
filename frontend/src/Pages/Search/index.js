import { Typography } from '@material-ui/core'
import React from 'react'
import Filter from '../../Components/Filter'


const SearchPage = () => {
    return (
        <div>
            
            <Typography varient='h6'>Search your ideas</Typography>
            <Filter/> 
                      
        </div>
    )
}

export default SearchPage

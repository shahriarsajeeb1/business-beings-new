import React from "react";
import { makeStyles, Typography } from "@material-ui/core";
import Header from '../../Components/Header'

const useStyles = makeStyles((theme)=>({
  root:{
    width:'100%',
    minHeight:'100vh',
    padding:'calc(30px + (70 - 30) * ((100vw - 350px) / (2000 - 350))) calc(30px + (70 - 30) * ((100vw - 350px) / (2000 - 350))) 0 calc(30px + (70 - 30) * ((100vw - 350px) / (2000 - 350)))',

  },
  heading:{
    textTransform: 'uppercase',
    fontWeight:'bold',
    marginTop: 'calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))',
    
  },
  para:{
    // textIndent:'calc(30px + (70 - 30) * ((100vw - 350px) / (2000 - 350)))',
    textAlign:'justify',
    textJustify:"inter-word",
    marginTop: 'calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))',
  }
}))

const About = () => {
  const classes = useStyles()

  return (
   <>
      
      <div className={classes.root}>
      <Typography className={classes.heading} variant='h6'>About Us</Typography>
      <Typography className={classes.para} variant='subtitle1'>
        Business Beings is a world’s first ever OTT Typographylatform for business minded
        people who are interested in starting a business and love to become a
        businessman or an entrepreneur. Business Beings currently offers high
        quality content, business ideas in tamil language and an academy of
        courses which assist to launch a business successfully.
      </Typography>
      <Typography className={classes.para} variant='body1'>
      You can explore business ideas based on your interests and budget, Watch casestudies and meet successful entrepreneurs through our platform
      </Typography>
      <Typography className={classes.heading} variant='h6'>Support</Typography>
      <Typography className={classes.para} variant='body1'>If you have any queries, suggestion, or operational/ technical issues, please reach out to us at hello@businessbeings.com</Typography>
    </div>
    </>
  );
};

export default About;

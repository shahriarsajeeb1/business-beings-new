import { Button, Card, makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from "react-router-dom";


const useStyles = makeStyles((theme) => ({
    root:{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',        
    },
    cardWrapper:{
        width:'calc(300px + (1700 - 300) * ((100vw - 350px) / (2000 - 350)))',
        padding: 'calc(30px + (45 - 30) * ((100vw - 350px) / (2000 - 350)))',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',  
        flexDirection:'column',
        textAlign:'center',
        marginBottom:'calc(10px + (40 - 10) * ((100vw - 350px) / (2000 - 350)))',
    },
    featuresWrapper:{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',  
        flexWrap:'wrap',
        marginTop:'calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))',
        marginBottom:'calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))',
    },
    feature:{
        width:'50%',
        marginTop:'calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))',
        
    },
    marginGap:{
        marginTop:'calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))',
    }
}))


const FinishTab = () => {
const classes = useStyles();
const user = useSelector(state => state.user.user)
const history = useHistory();

const onClickNavigation = () => {
    history.push("/user");
}


    return (
        <div className={classes.root}>
            <div className={classes.cardWrapper}>
            <Typography variant='subtitle1' color='textSecondary'>
            Signup completed!
            </Typography>
            <Typography className={classes.marginGap} variant='h6' color='textPrimary' style={{fontWeight:'bold'}}>
            Welcome to Business Beings
            </Typography>
            <Typography className={classes.marginGap} variant='subtitle1' color='textSecondary'>
            You can now start exploring business ideas, interviews and case studies.
Your next billing information will be sent on your mail on {user.planExpiryDate}

            </Typography>
            <Typography className={classes.marginGap} variant='h6' color='textPrimary' style={{fontWeight:'bold'}}>
            Your plan:{user.planName}
            </Typography>
            <div className={classes.featuresWrapper}>
            <Typography variant='subtitle1' color='textSecondary' className={classes.feature}>
            Explore Case studies
            </Typography>
            <Typography variant='subtitle1' color='textSecondary' className={classes.feature}>
            Watch Expert Interviews
            </Typography>
            <Typography variant='subtitle1' color='textSecondary' className={classes.feature}>
            Adfree Streams
            </Typography>
            <Typography variant='subtitle1' color='textSecondary' className={classes.feature}>
            HD Video Quality
            </Typography>
            </div>
            <Button className={classes.marginGap} onClick={onClickNavigation} color='primary' variant='contained'>Start watching</Button>
            </div>
        </div>
    )
}

export default FinishTab

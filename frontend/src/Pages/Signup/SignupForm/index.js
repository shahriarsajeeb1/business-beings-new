import React, { useEffect, useState } from "react";
import * as yup from 'yup';
import { useFormik } from 'formik';
import { useSelector, useDispatch } from "react-redux";
import {
  Button,
  InputBase,
  makeStyles,
  Typography,
  FormControl,
  FormHelperText,
} from "@material-ui/core";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import { createUser } from "../../../services/Userdata/action/index.js";
import { updatedUser } from "../../../services/Userdata/action/index.js";
import storage from "../../../firebase";
import { ref, uploadBytesResumable, getDownloadURL } from "firebase/storage";
import AlertDialogUser from "./AlertDialog";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "stretch",
    alignItems: "stretch",
  },
  userProfile: {
    width: "calc(300px + (450 - 300) * ((100vw - 350px) / (2000 - 350)))",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
  },
  dp: {
    width: "calc(250px + (300 - 250) * ((100vw - 350px) / (2000 - 350)))",
    height: "calc(300px + (400 - 300) * ((100vw - 350px) / (2000 - 350)))",
    marginBottom: "calc(20px + (70 - 20) * ((100vw - 350px) / (2000 - 350)))",
  },
  buttonWrapper: {
    width: "calc(250px + (300 - 250) * ((100vw - 350px) / (2000 - 350)))",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  inputWrapper: {
    width: "calc(200px + (700 - 200) * ((100vw - 350px) / (2000 - 350)))",
    marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  text: {
    marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  subText: {
    marginBottom: "calc(3px + (10 - 3) * ((100vw - 350px) / (2000 - 350)))",
  },
  input: {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.common.white,
    border: "1px solid #ced4da",
    fontSize: 16,
    width: "100%",
    height: "calc(30px + (50 - 30) * ((100vw - 350px) / (2000 - 350)))",
    padding: "10px 12px",
    color: theme.palette.text.secondary,
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    "&:focus": {
      // boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      borderColor: theme.palette.primary.main,
      color: "black",
    },
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
}));

const validate = yup.object({
  name: yup
    .string("Enter your Name")
    .min(2, "Enter valid Name")
    .max(50, "You have exeeded 50 characters")
    .required("Name is required"),
  email: yup
    .string("Enter valid Email")
    .email("Enter valid Email")
    .required("Email is required"),
  pincode: yup
    .string("Enter your Pincode")
    .length(6, "Enter valid Pincode,of 6 digits")
    .required("Pincode is required"),
  password: yup
    .string("Enter your password")
    .min(8, "Password should be of minimum 8 characters length")
    .max(20, "You have exeeded 20 characters")
    .required("Password is required"),
  passwordConfirm: yup
    .string()
    .required("Re-Enter Password")
    .oneOf([yup.ref("password"), null], "Password is not matching"),
});

const SignupForm = ({ handleTab }) => {
  const classes = useStyles();
  const user = useSelector((state) => state.user.user);
  const users = useSelector((state) => state.user);
  const style = useSelector((state) => state.style);
  const dispatch = useDispatch();
//   const [createData, setCreateData] = useState({
//     name: "",
//     mobile: style.mobile,
//     email: "",
//     pincode: "",
//     password: "",
//     invitedFriend: style.invitedFriend,
//   });
  const [image, setImage] = useState(null);
  const [imageurl, setImageurl] = useState("");

  const formik = useFormik({
    initialValues: {
      name: "",
      mobile: style.mobile,
      email: "",
      pincode: "",
      password: "",
      passwordConfirm:'',
      invitedFriend: style.invitedFriend,
    },
    validationSchema: validate,
    validateOnMount: true,
    onSubmit: () => {},
  });

  useEffect(() => {
    if (users.isSuccessCreate && imageurl) {
      dispatch(
        updatedUser({
          image: imageurl,
          password: "",
        })
      );
    }
  }, [users.isSuccessCreate, imageurl]);

  const onChangeImage = (event) => {
    setImage(event.target.files[0]);
  };

//   const onChangeFields = (event) => {
//     setCreateData((prevData) => ({
//       ...prevData,
//       [event.target.name]: event.target.value,
//     }));
//   };

  const handleSubmitButton = (e) => {
    dispatch(createUser(formik.values));
    if (image) {
      e.preventDefault();
      const imageRef = ref(storage, `/images/${image.name}`);
      const uploadTask = uploadBytesResumable(imageRef, image);
      uploadTask.on(
        "state_changed",
        (snapshot) => {
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          const progress =
            (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log("Upload is " + progress + "% done");
          switch (snapshot.state) {
            case "paused":
              console.log("Upload is paused");
              break;
            case "running":
              console.log("Upload is running");
              break;
          }
        },
        (error) => {
          switch (error.code) {
            case "storage/unauthorized":
              // User doesn't have permission to access the object
              break;
            case "storage/canceled":
              // User canceled the upload
              break;

            // ...

            case "storage/unknown":
              // Unknown error occurred, inspect error.serverResponse
              break;
          }
        },
        () => {
          getDownloadURL(uploadTask.snapshot.ref).then((url) => {
            setImageurl(url);
          });
        }
      );
    }
  };

  return (
    <div className={classes.root}>
      {/* <div className={classes.userProfile}>
                <img src={user.image} className={classes.dp} />
                <div className={classes.buttonWrapper}>

                <Button variant='contained' color='primary'>Change Picture</Button>
                </div>
            </div> */}
      <div>
        <Typography className={classes.text} varient="h6">
          Account Information
        </Typography>
        <Typography className={classes.text} varient="h6"></Typography>
        <div className={classes.inputWrapper}>
          <FormControl>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Name
            </Typography>
            <InputBase
              id="name"
              name="name"
              value={formik.values.name}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              placeholder="Name..."
              className={classes.input}
            />
            {Boolean(formik.errors.name) && formik.touched.name && (
                <FormHelperText error id="standard-weight-helper-text">
                  {formik.errors.name}
                </FormHelperText>
              )}
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Mobile
            </Typography>
            <InputBase
              style={{ backgroundColor: "#f0f0f0" }}
              disabled
              name="mobile"
              placeholder={style.mobile}
              className={classes.input}
            />

          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Email
            </Typography>
            <InputBase
             id="email"
             name="email"
             value={formik.values.email}
             onChange={formik.handleChange}
             onBlur={formik.handleBlur}
              placeholder="abc@mail.com"
              className={classes.input}
            />
            {Boolean(formik.errors.email) && formik.touched.email && (
                <FormHelperText error id="standard-weight-helper-text">
                  {formik.errors.email}
                </FormHelperText>
              )}
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Pincode
            </Typography>
            <InputBase
              id="pincode"
              name="pincode"
              value={formik.values.pincode}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              placeholder="123456"
              className={classes.input}
            />
            {Boolean(formik.errors.pincode) && formik.touched.pincode && (
                <FormHelperText error id="standard-weight-helper-text">
                  {formik.errors.pincode}
                </FormHelperText>
              )}
          </FormControl>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Invited Friend
            </Typography>
            <InputBase
              style={{ backgroundColor: "#f0f0f0" }}
              disabled
              name="invitedFriend"
              placeholder={
                style.invitedFriend == "" ? "No One" : style.invitedFriend
              }
              className={classes.input}
            />
          </FormControl>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Profile Image
            </Typography>
            <InputBase
              onChange={onChangeImage}
              name={style.mobile}
              type="file"
              className={classes.input}
            />
          </FormControl>
        </div>
        <Typography className={classes.text} varient="h6">
          Password Change
        </Typography>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Password
            </Typography>
            <InputBase
              id="password"
              name="password"
              value={formik.values.password}
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              type="password"
              className={classes.input}
            />
            {Boolean(formik.errors.password) && formik.touched.password && (
                <FormHelperText error id="standard-weight-helper-text">
                  {formik.errors.password}
                </FormHelperText>
              )}
          </FormControl>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Confirm Password
            </Typography>
            <InputBase
             id="passwordConfirm"
             name="passwordConfirm"
             value={formik.values.passwordConfirm}
             onChange={formik.handleChange}
             onBlur={formik.handleBlur}
              type="password"
              className={classes.input}
            />
            {Boolean(formik.errors.passwordConfirm) && formik.touched.passwordConfirm && (
                <FormHelperText error id="standard-weight-helper-text">
                  {formik.errors.passwordConfirm}
                </FormHelperText>
              )}
          </FormControl>
        </div>
        <Button
          style={{
            margin: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
          }}
          disabled={!formik.isValid}
          variant="contained"
          color="primary"
          onClick={handleSubmitButton}
        >          
          Submit
        </Button>
      </div>
      {users.isSuccessCreate && users.isSuccessUpdate && (
        <AlertDialogUser
          view={true}
          dialogContent={"Account Created"}
          handleTab={handleTab}
        />
      )}
      {users.isErrorCreate && (
        <AlertDialogUser
          view={true}
          dialogContent={"Error Occured.Try Again"}
        />
      )}
      <Backdrop
        className={classes.backdrop}
        open={users.isLoadingCreate || users.isLoadingUpdate}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
};

export default SignupForm;

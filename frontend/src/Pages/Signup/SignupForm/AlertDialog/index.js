import React,{useState} from 'react';
import {useSelector,useDispatch } from 'react-redux'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';



const AlertDialogUser = React.forwardRef((props,ref) => {
  const [open, setOpen] = useState(props.view); 
  const dispatch = useDispatch();
  const user = useSelector(state => state.user)
  

  const handleClose = () => {
    setOpen(false);
    if(user.isSuccessCreate){
      props.handleTab(1) ;   
    }
  };

  return (
     
      <Dialog
        open={open}
        onClose={handleClose}        
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        ref={ref}      >
        
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {props.dialogContent}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    
  );
})
export default AlertDialogUser
import React,{useState,useEffect} from 'react'
import { useSelector,useDispatch } from 'react-redux'
import { Card, CardContent, makeStyles, Typography } from '@material-ui/core'
import { verifyPayment,createPaymentOrder } from '../../../../services/Payment/action'

const useStyles = makeStyles((theme)=>({
    card:{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: 'calc(120px + (250 - 120) * ((100vw - 350px) / (2000 - 350)))',
        height: 'calc(100px + (150 - 100) * ((100vw - 350px) / (2000 - 350)))',
        borderRadius: '17px',
        position: 'relative',
        cursor: 'pointer', 
        margin:'0.7vw',  
        textAlign:'center',     
        '&:hover': {
            border: `1px solid ${theme.palette.primary.dark}`,
            transform: 'scale(1.08)'
        }
        
    }
}))

const PlanCard = ({plan}) => {

    const classes = useStyles();
    const dispatch = useDispatch()
    const payment = useSelector(state => state.payment)

    const options = {
        "key":'rzp_test_4zqfJYaZbadTlg' ,
        "amount": 0,
        "name": "Merchant Name",
        'order_id':"",
        "handler": function(response) {
            console.log(response);
            var orderDetails ={
                razorpay_signature : response.razorpay_signature,
                razorpay_order_id : response.razorpay_order_id,
                transactionid : response.razorpay_payment_id,
                plan_id:plan._id,
              }
             dispatch(verifyPayment(orderDetails))
            
        },
        "theme": {
          "color": "#528ff0"
        }
      };
  

    useEffect(() => {
        const script = document.createElement("script");
        script.src = "https://checkout.razorpay.com/v1/checkout.js";
        script.async = true;
        document.body.appendChild(script);
        if(payment.order.id){
            options.order_id = payment.order.id;
            options.amount = payment.order.amount;
            console.log(options)
            const rzp1 = new window.Razorpay(options);
            rzp1.open();
        }
        
    }, [payment.order])


    const handlePaymentOrder=(plan)=>{       
        dispatch(createPaymentOrder(plan))
    }
    


    return (
        
            <Card className={classes.card} onClick={()=>handlePaymentOrder(plan)}>
                <CardContent>
                    <Typography variant="subtitle1" color='textSecondary' gutterBottom>
                            For {plan.duration} months,
                    </Typography>
                    <Typography variant="h6"  gutterBottom>
                            Rs. {Math.round(plan.price/plan.duration)}/month,
                    </Typography>
                </CardContent>
            </Card>
        
    )
}

export default PlanCard

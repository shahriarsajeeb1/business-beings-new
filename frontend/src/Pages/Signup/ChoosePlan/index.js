import React from 'react'
import { useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import { Button, Typography } from '@material-ui/core';
import PlanCard from './PlanCard';
import AlertDialogPlan from './AlertDialogPlan';


const useStyles = makeStyles((theme)=>({
    root:{
        width:'calc(300px + (1700 - 300) * ((100vw - 350px) / (2000 - 350)))',
    },
    skipButtonWrapper:{
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
      width:'100%'
      
    }
}))

const Plan = ({handleTab}) => {
    const classes = useStyles();
    const plans = useSelector(state => state.plan.plans)
    const payment = useSelector(state => state.payment)

  return (
    <div style={{marginBottom:'calc(45px + (70 - 45) * ((100vw - 350px) / (2000 - 350)))'}}>
      <TableContainer component={Paper}>
        <Table className={classes.root} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell align="center">Free</TableCell>
              <TableCell align="center">Premium</TableCell>
              <TableCell align="center">VIP</TableCell>            
            </TableRow>
          </TableHead>
          <TableBody>
              <TableRow key='1'>
                <TableCell component="th" scope="row">
                Explore Business Streams
                </TableCell>
                <TableCell align="center"><CloseIcon/></TableCell>
                <TableCell align="center"><DoneIcon/></TableCell>
                <TableCell align="center"><DoneIcon/></TableCell>             
              </TableRow>
              <TableRow key='2'>
                <TableCell component="th" scope="row">
                Premium Business Streams
                </TableCell>
                <TableCell align="center"><CloseIcon/></TableCell>
                <TableCell align="center"><CloseIcon/></TableCell>
                <TableCell align="center"><DoneIcon/></TableCell>             
              </TableRow>
              <TableRow key='3'>
                <TableCell component="th" scope="row">
                Explore Case studies
                </TableCell>
                <TableCell align="center"><CloseIcon/></TableCell>
                <TableCell align="center"><CloseIcon/></TableCell>
                <TableCell align="center"><DoneIcon/></TableCell>             
              </TableRow>
              <TableRow key='4'>
                <TableCell component="th" scope="row">
                Watch Expert Interviews
                </TableCell>
                <TableCell align="center"><CloseIcon/></TableCell>
                <TableCell align="center"><CloseIcon/></TableCell>
                <TableCell align="center"><DoneIcon/></TableCell>             
              </TableRow>
              <TableRow key='5'>
                <TableCell component="th" scope="row">
                All Live sessions (Trainings, Consultations, Events)
                </TableCell>
                <TableCell align="center"><CloseIcon/></TableCell>
                <TableCell align="center"><CloseIcon/></TableCell>
                <TableCell align="center"><DoneIcon/></TableCell>             
              </TableRow>
              <TableRow key='6'>
                <TableCell component="th" scope="row">
                Adfree Streams
                </TableCell>
                <TableCell align="center"><CloseIcon/></TableCell>
                <TableCell align="center"><CloseIcon/></TableCell>
                <TableCell align="center"><DoneIcon/></TableCell>             
              </TableRow>
              <TableRow key='7'>
                <TableCell component="th" scope="row">
                HD Video Quality
                </TableCell>
                <TableCell align="center"><CloseIcon/></TableCell>
                <TableCell align="center"><DoneIcon/></TableCell>
                <TableCell align="center"><DoneIcon/></TableCell>             
              </TableRow>        
          </TableBody>
        </Table>
      </TableContainer>
      <div>
        <div style={{margin:'calc(15px + (30 - 15) * ((100vw - 350px) / (2000 - 350)))'}}>
          <Typography variant='h4' gutterBottom>
          Premium Plans
          </Typography>
          <div style={{display: 'flex'}}>
            {plans.filter(plan=>plan.type === 'PRE').map((planItem)=>
              <PlanCard plan={planItem}/>
            )}

          </div>
        </div>
        <div style={{margin:'calc(15px + (30 - 15) * ((100vw - 350px) / (2000 - 350)))'}}>
          <Typography variant='h4' gutterBottom>
          VIP Plans
          </Typography>
          <div style={{display: 'flex'}}>
            {plans.filter(plan=>plan.type === 'VIP').map((planItem)=>
              <PlanCard plan={planItem}/>
            )}

          </div>
        </div>
        <div className={classes.skipButtonWrapper}>
          <Button  onClick={()=>handleTab(2)} variant='contained' color='primary'>Skip</Button>
        </div>
      </div>
      {payment.isSuccessUpdate && <AlertDialogPlan view={true} dialogContent={'Transaction Successful.Account is Upgraded'} handleTab={handleTab}/>}
      {payment.isErrorUpdate && <AlertDialogPlan view={true} dialogContent={'Error Occured.Try Again'} />}
    </div>
    );
}

export default Plan

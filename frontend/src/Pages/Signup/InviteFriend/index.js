import React from "react";
import { useSelector } from "react-redux";
import { Button, Typography } from "@material-ui/core";
import FacebookIcon from "@material-ui/icons/Facebook";
import TwitterIcon from "@material-ui/icons/Twitter";
import PinterestIcon from "@material-ui/icons/Pinterest";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import WhatsAppIcon from "@material-ui/icons/WhatsApp";
import { makeStyles } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection:'column',
    width: "calc(300px + (1000 - 300) * ((100vw - 350px) / (2000 - 350)))",
    borderRadius: "17px",
    marginBottom: "calc(12px + (25 - 12) * ((100vw - 350px) / (2000 - 350)))",
  },
  contWrapper: {
    margin: "calc(12px + (25 - 12) * ((100vw - 350px) / (2000 - 350)))",
  },
  socialIconsWrapper: {
    display: "flex",
    justifyContent: "space-around",
    alignItems: "center",
    width: "calc(150px + (500 - 150) * ((100vw - 350px) / (2000 - 350)))",
    margin: "calc(12px + (25 - 12) * ((100vw - 350px) / (2000 - 350)))",
  },
  iconsAnimation: {
    transition: "500ms",
    cursor: "pointer",
    "& :hover": {
      transform: "scale(1.1)",
    },
  },
}));

const Invitation = (props) => {
  const classes = useStyles();
  const user = useSelector((state) => state.user.user);
  const postUrl = `https://businessbeings.com/register/${user._id}`;
  const postTitle = "Join Business Beings to explore business ideas based on your budget and interest, using my referral link ";
  const postImg = "http://placehold.it/400x400&text=image1";

  return (
    <div className={classes.root}>
      <div className={classes.contWrapper}>
        {props.type ? null :<Typography variant="h6" style={{fontWeight:'bold'}}>Invite Your Friend</Typography>}
        <Typography variant="subtitle1">
          Invite your friend by sharing on social medias, and earn upto 10 days
          of free VIP plan as reward.
        </Typography>
        <div className={classes.socialIconsWrapper}>
          <a
            className={classes.iconsAnimation}
            href={`https://www.facebook.com/sharer.php?u=${postUrl}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <FacebookIcon style={{color:"#3b5998"}} />
          </a>
          <a
            className={classes.iconsAnimation}
            href={`https://twitter.com/share?url=${postUrl}&text=${postTitle}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <TwitterIcon style={{color:"#1da1f2"}} />
          </a>
          <a
            className={classes.iconsAnimation}
            href={`https://pinterest.com/pin/create/bookmarklet/?media=${postImg}&url=${postUrl}&description=${postTitle}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <PinterestIcon style={{color:"#0077b5"}} />
          </a>
          <a
            className={classes.iconsAnimation}
            href={`https://www.linkedin.com/shareArticle?url=${postUrl}&title=${postTitle}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <LinkedInIcon style={{color:"#bd081c"}} />
          </a>
          <a
            className={classes.iconsAnimation}
            href={`https://wa.me/?text=${postTitle} ${postUrl}`}
            target="_blank"
            rel="noopener noreferrer"
          >
            <WhatsAppIcon style={{color:"#25d366"}} />
          </a>
        </div>
        <Typography variant="subtitle1">
          Once your friend completed registration,you will be credited with your
          reward,based on the type of subscription yor friend regitered.
        </Typography>
      </div>
      <div>
        <Button onClick={()=>props.handleTab(3)} variant='contained' color='primary'>Next</Button>
      </div>
    </div>
  );
};

export default Invitation;

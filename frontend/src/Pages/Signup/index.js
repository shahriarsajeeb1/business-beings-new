import React, { useState } from 'react'
import { makeStyles, Tab, Tabs, Typography,Card } from '@material-ui/core'
import PhoneIcon from '@material-ui/icons/Phone';
import FavoriteIcon from '@material-ui/icons/Favorite';
import PersonPinIcon from '@material-ui/icons/PersonPin';
import SignupForm from './SignupForm';
import Invitation from './InviteFriend';
import Plan from './ChoosePlan';
import FinishTab from './FinishTab';

const useStyles = makeStyles((theme)=>({
    root:{
        width:'calc(300px + (1700 - 300) * ((100vw - 350px) / (2000 - 350)))',
        height:'auto',
        marginTop:'calc(10px + (60 - 10) * ((100vw - 350px) / (2000 - 350)))',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        margin: 'auto',
    },
    subtitle:{
        textAlign:'center',
        textTransform:'uppercase',
        marginTop:'calc(10px + (40 - 10) * ((100vw - 350px) / (2000 - 350)))',
    },
    title:{       
        textAlign:'center',
        textTransform:'uppercase',
        // margin:'calc(20px + (30 - 20) * ((100vw - 350px) / (2000 - 350)))',
    },
    tabWrapper:{
        width:'calc(300px + (1700 - 250) * ((100vw - 350px) / (2000 - 350)))',
        // height:'calc(300px + (700 - 300) * ((100vw - 350px) / (2000 - 350)))',
        marginTop:'calc(20px + (40 - 20) * ((100vw - 350px) / (2000 - 350)))',
    },
    tabs:{

    },
    tabIndicator:{
       
      maxWidth: 40,
      width: '100%',
      backgroundColor: theme.palette.primary,
    
}
}))

const Signup = () => {

    const classes = useStyles();
    const [tab,setTab] = useState(0);

    const handleTab = (value)=>{
        setTab(value);
        
    }
    const tempHandleTab = (event,value)=>{
        setTab(value);
        console.log(value)
    }


    return (
        <div >
            <Card className={classes.root}>
            <div className={classes.title}>
                <Typography variant='subtitle1'>Sign Up here</Typography>
                <Typography variant='h4'>Rocket your business</Typography>
            </div>
            <div className={classes.tabWrapper}>
                <Tabs
                value={tab}
                onChange={tempHandleTab}
                textColor='textSecondary'
                variant="fullWidth"
                TabIndicatorProps={
                    classes.tabIndicator
                }>
                    <Tab icon={<PhoneIcon />} label="Create Your Account" />
                    <Tab icon={<FavoriteIcon />} label="Choose Your Plan" />
                    <Tab icon={<PersonPinIcon />} label="Invite Your Friend" />
                    <Tab icon={<PersonPinIcon />} label="Finish" />
                </Tabs>
            </div>
            <div>
            {tab == 0 && <SignupForm handleTab={handleTab}/>}
          {tab == 1 && <Plan handleTab={handleTab}/>}
          {tab == 2 && <Invitation handleTab={handleTab}/>}
          {tab == 3 && <FinishTab/>}
            </div>
            </Card>
        </div>
    )
}

export default Signup

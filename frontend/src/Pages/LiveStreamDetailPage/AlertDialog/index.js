import React,{useState,useEffect} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';




const AlertDialogLiveStream = ({view,message,alertClose}) => {
  const [alertOpen, setAlertOpen] = useState(view); 
  
  

  const handleClose = () => {
    setAlertOpen(false);
     alertClose()
  };

  return (
     
      <Dialog
        open={alertOpen}
        onClose={handleClose}        
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
        
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
        {message}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    
  );
}
export default AlertDialogLiveStream
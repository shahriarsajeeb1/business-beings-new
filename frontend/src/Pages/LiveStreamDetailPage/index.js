import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import dayjs from "dayjs";
import { useParams } from "react-router-dom";
import Header from "../../Components/Header";
import CardSlider from "../Home/CardSlider";
import Carding from "../../Components/Card";
import {
  makeStyles,
  Typography,
  Backdrop,
  CircularProgress,
  Button,
  Hidden,
  Card,
} from "@material-ui/core";
import AlertDialogLiveStream from "./AlertDialog";
import { getAllLiveStreamAdmin } from "../../services/LiveStream/action";
import LiveStreamCard from "../Home/LiveStreamCard";

const useStyles = makeStyles((theme) => ({
  contWrapper: {
    width: "95%",
    flexDirection: "column",
    display: "flex",
    margin: "auto",
  },
  cont: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: "calc(5px + (10 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  textWrapper: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    flexDirection: "column",
    width: "40%",
    margin: "calc(5px + (10 - 5) * ((100vw - 350px) / (2000 - 350)))",
    [theme.breakpoints.down("sm")]: {
      width: "90%",
    },
  },
  textCont: {
    marginTop: "calc(5px + (10 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  text: {
    color:
      theme.palette.type == "light"
        ? theme.palette.primary.dark
        : theme.palette.common.white,
  },
  buttons: {
    width: "70%",
    display: "flex",
    alignItems: "center",
    marginTop: "calc(15px + (30 - 15) * ((100vw - 350px) / (2000 - 350)))",
    marginBottom: "calc(15px + (30 - 15) * ((100vw - 350px) / (2000 - 350)))",
    [theme.breakpoints.down("sm")]: {
      width: "90%",
    },
  },
  imgWrapper: {
    width: "40%",
    height: "100%",
  },
  imgCont: {
    width: "100%",
    height: "100%",
  },
  descWrapper: {
    width: "90%",
  },
  epidsodes: {
    display:'flex',
    justifyContent:'center',
    alignItems:'center',
    flexDirection:'column',
    marginTop: "calc(15px + (30 - 15) * ((100vw - 350px) / (2000 - 350)))",
  },
  recent: {
    marginTop: "calc(15px + (30 - 15) * ((100vw - 350px) / (2000 - 350)))",
    marginBottom: "calc(30px + (45 - 35) * ((100vw - 350px) / (2000 - 350)))",
  },
  videoWrapper: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    flexWrap: "wrap",
  },
  jwVideoContainer: {
    position: "relative",
    width: "90vw",
    height: "auto",
    transitionProperty: "height",
    transitionduration: "10s",
    marginLeft: "auto",
    marginRight: "auto",
  },
  jwVideoClose: {
    position: "absolute",
    top: "10px",
    right: "10px",
    width: "calc(30px + (40 - 30) * ((100vw - 350px) / (2000 - 350)))",
    height: "calc(30px + (40 - 30) * ((100vw - 350px) / (2000 - 350)))",
    cursor: "pointer",
    zIndex: 10,
  },
  cardClose: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
}));

const LiveStreamDetail = () => {
  const classes = useStyles();
  const params = useParams();
  const dispatch = useDispatch();
  const liveStream = useSelector((state) => state.liveStream);
  const liveStreams = useSelector((state) => state.liveStream.liveStreams);
  const stream = liveStreams.find((dat) => dat._id == params.id);
  const user = useSelector((state) => state.user.user);  
  const [loginCheck, setLoginCheck] = useState(false);
  const [timeCheck, setTimeCheck] = useState(false);

  useEffect(() => {
    if (liveStreams == []) {
      dispatch(getAllLiveStreamAdmin());
    }
  }, [liveStreams]) 

  const handleJoin = () => {
    var isBetween = require('dayjs/plugin/isBetween')
    dayjs.extend(isBetween)
     
      if(!user._id){
        setLoginCheck(true)
        return;
      }else{
        if (!dayjs().isBetween(dayjs(stream.startingDate), dayjs(stream.endingDate))){
          console.log('jhhh')
          setTimeCheck(true)
          return;
        }
      }
      window.open(`${stream.link}`,'_newtab');
  }

  const timeAlertClose = () => {
    setTimeCheck(false)
  }

  const loginAlertClose = () => {
    setLoginCheck(false)
  }
  
  
  return (
    <div>
      {liveStreams && stream && (
        <div className={classes.contWrapper}>
          <div>
            <Header />
          </div>
          <div className={classes.cont}>
            <div className={classes.textWrapper}>
              <Typography variant="h4">{stream.name}</Typography>
              {/* <div className={classes.buttons}>
                <Button
                  disabled={liked}
                  onClick={handleLike}
                  variant="contained"
                  color="primary"
                >
                  <ThumbUpAltIcon />
                  {liked ? "Liked" : "Like"}
                </Button>
                <Button
                  disabled={liked}
                  variant="outlined"
                  onClick={handleWatchList}
                  style={{ marginLeft: "20px" }}
                  className={classes.text}
                >
                  <PlaylistAddCheckRoundedIcon />

                  {bookMarked ? "Added to watchList" : "Add to watchList"}
                </Button>
              </div> */}
              <Typography variant="subtitle1" className={classes.textCont}>
                Language:
                <Typography
                  display="inline"
                  className={classes.text}
                  variant="subtitle1"
                >
                  Tamil
                </Typography>
              </Typography>
              <Typography variant="subtitle1" className={classes.textCont}>
                Category:
                <Typography
                  display="inline"
                  className={classes.text}
                  variant="subtitle1"
                >
                  {stream.category}
                </Typography>
              </Typography>
              <Typography variant="subtitle1" className={classes.textCont}>
                Interest:
                <Typography
                  display="inline"
                  className={classes.text}
                  variant="subtitle1"
                >
                  {stream.interest}
                </Typography>
              </Typography>
              <Typography variant="subtitle1" className={classes.textCont}>
                Starting Time:
                <Typography
                  display="inline"
                  className={classes.text}
                  variant="subtitle1"
                >
                  {stream.startingDate}
                </Typography>
              </Typography>
              <Typography variant="subtitle1" className={classes.textCont}>
                Ending Time:
                <Typography
                  display="inline"
                  className={classes.text}
                  variant="subtitle1"
                >
                  {stream.endingDate}
                </Typography>
              </Typography>
              <Typography variant="subtitle1" className={classes.textCont}>
                Duration:
                <Typography
                  display="inline"
                  className={classes.text}
                  variant="subtitle1"
                >
                  {dayjs(stream.startingDate).diff(dayjs(stream.endingDate), "m")} Minutes.
                </Typography>
              </Typography>
             
              <Typography className={classes.textCont} variant="subtitle1">{stream.description}</Typography>
            
            </div>
            <Hidden smDown>
              <div className={classes.imgWrapper}>
                <img src={stream.image} className={classes.imgCont} />
              </div>
            </Hidden>
            
          </div>
          <div className={classes.epidsodes}>
            <Typography variant="h6" style={{marginTop:'calc(15px + (30 - 15) * ((100vw - 350px) / (2000 - 350)))'}}>
              Live stream starts by              
            </Typography>
            <Typography variant="h3" style={{marginTop:'calc(15px + (30 - 15) * ((100vw - 350px) / (2000 - 350)))'}}>
              {dayjs(stream.startingDate).diff(dayjs(), "minute")} Minutes
            </Typography>
            <Button onClick={handleJoin} style={{width:'90%',marginTop:'calc(15px + (30 - 15) * ((100vw - 350px) / (2000 - 350)))'}} color="primary" variant="contained">
              Join Live Stream
            </Button>
          </div>
          <div className={classes.recent}>
            <Typography variant="h5">Recent Live Streams</Typography>
            <div className={classes.videoWrapper}>
              {liveStreams
                .sort((a, b) => new dayjs(b.created) - new dayjs(a.created))
                .map((dat) => {
                  return (
                    <div>
                      <LiveStreamCard stream={dat} />
                    </div>
                  );
                })}
            </div>
          </div>
          {loginCheck && <AlertDialogLiveStream alertClose={loginAlertClose} view={loginCheck} message='Please Login Or SignUp if you are a first time User.'/>}
          {timeCheck && <AlertDialogLiveStream alertClose={timeAlertClose} view={timeCheck} message='Please wait till the live Session start'/>}
        </div>
      )}
      <Backdrop
        className={classes.backdrop}
        open={liveStream.isLoadingGet}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
};

export default LiveStreamDetail;

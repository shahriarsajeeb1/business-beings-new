import { makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import Filter from '../../Components/Filter'
import { Helmet } from "react-helmet";

const useStyles = makeStyles((themes)=>({
    root:{
        width:'90%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
       
        flexDirection: 'column',
        margin:"calc(10px + (60 - 10) * ((100vw - 350px) / (2000 - 350)))",
        
    },
    title1:{
        textAlign:'center',
    },
    title:{       
        marginBottom:"calc(10px + (60 - 10) * ((100vw - 350px) / (2000 - 350)))",
        // marginTop:"calc(10px + (60 - 10) * ((100vw - 350px) / (2000 - 350)))",
    },
   
}))

const Inerviews = () => {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <Helmet>
        <title>Success stories by Entrepreneurs</title>
        <meta name='description' content='If you love to learn business from business leaders and mentors, youre on the right page'/>
        <meta name='keywords' content='business interviews, success stories, business leader interviews'/>
      </Helmet>
            <Typography variant='h4' className={classes.title1}>If entrepreneurship is your life and you love to learn business then Business Beings is streaming business interviews of successful people</Typography>
            <Typography variant='h6' className={classes.title}>We are streaming the best success stories that is most wanted by entrepreneurs</Typography>
            <Filter type='Interviews'/>
        </div>
    )
}

export default Inerviews

import React, { useState,useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Button,
  InputBase,
  makeStyles,
  Typography,
  FormControl, 
} from "@material-ui/core";
import { createUserAdmin,getAllUserAdmin } from "../../../../services/Admin/action";
import AlertDialog from "../AlertDialog";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "calc(300px + (450 - 300) * ((100vw - 350px) / (2000 - 350)))",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },

  inputWrapper: {
    width: "calc(200px + (400 - 200) * ((100vw - 350px) / (2000 - 350)))",
    marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  text: {
    marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  subText: {
    marginBottom: "calc(3px + (10 - 3) * ((100vw - 350px) / (2000 - 350)))",
  },
  input: {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.common.white,
    border: "1px solid #ced4da",
    fontSize: 16,
    width: "100%",
    height: "calc(30px + (50 - 30) * ((100vw - 350px) / (2000 - 350)))",
    padding: "10px 12px",
    color: theme.palette.text.secondary,
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    "&:focus": {
      // boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      borderColor: theme.palette.primary.main,
      color: "black",
    },
  },
}));

const CreateModal = ( {onClickCreateModalClose}) => {
  const classes = useStyles();
  const dispatch = useDispatch(); 
  const admin = useSelector((state) => state.admin);
  const [createData, setCreateData] = useState({
    name: "",
    mobile: 0,
    email: "",
    pincode: "",
    password: "",
  });

  useEffect(() => {
    if(admin.isSuccessCreate){        
        dispatch(getAllUserAdmin());     
    }    
  }, [admin.isSuccessCreate])

  const onChangeFields = (event) => {
    setCreateData((prevData) => ({
      ...prevData,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmitButton = () => {
    dispatch(createUserAdmin(createData));   
    
  };

  

  return (
    <div className={classes.root}>
        {/* {alertSuccess&&<div>User Successfully Created</div>} */}
      <div>
        <Typography className={classes.text} varient="h6">
          Create User
        </Typography>
        <Typography className={classes.text} varient="h6"></Typography>
        <div className={classes.inputWrapper}>
          <FormControl>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Name
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="name"
              className={classes.input}
            />
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Mobile
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="mobile"
              className={classes.input}
            />
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Email
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="email"
              className={classes.input}
            />
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Pincode
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="pincode"
              className={classes.input}
            />
          </FormControl>
        </div>
        <Typography className={classes.text} varient="h6">
          Password Change
        </Typography>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Password
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="password"
              type="password"
              className={classes.input}
            />
          </FormControl>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Conform Password
            </Typography>
            <InputBase
              name="password"
              type="password"
              className={classes.input}
            />
          </FormControl>
        </div>
        {/* <div className={classes.inputWrapper}>
                <FormControl className={classes.margin}>
                    <Typography varient='subtitle1' color='textSecondary' className={classes.subText}>
                        Conform Password
                    </Typography>
                    <InputBase type='password' className={classes.input} />
                </FormControl>
                </div> */}
        <Button
          style={{
            margin: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
          }}
          variant="contained"
          color="primary"
          onClick={handleSubmitButton}
        >         
          Submit
        </Button>
      </div>
      {admin.isSuccessCreate && <AlertDialog view={true} dialogContent={'Account Created'} onClickModalClose={onClickCreateModalClose} />}
      {admin.isErrorCreate && <AlertDialog view={true} dialogContent={'Error Occured.Try Again'} onClickModalClose={onClickCreateModalClose} />}
      
    </div>
  );
};

export default CreateModal;

import React,{useState} from 'react'
import { makeStyles,Tabs,Tab } from '@material-ui/core'
import UserLayout from '../../Components/UserLayout'
import UsersTab from './UsersTab'
import PlansTab from './PlansTab'
import LiveStreamTab from './StreamsTab'

const useStyles = makeStyles((theme)=>({
    contWrapper:{
        margin: "calc(20px + (70 - 20) * ((100vw - 350px) / (2000 - 350)))",
        marginTop: "calc(10px + (50 - 10) * ((100vw - 350px) / (2000 - 350)))",
    },
    tab:{
        marginBottom: "calc(10px + (50 - 10) * ((100vw - 350px) / (2000 - 350)))",
    }
     
}))

const AdminLayout = () => {

    const classes = useStyles();    
    const [tab, setTab] = useState(0);
    const hangleTab = (event, value) => {
        setTab(value)
    }

    return (
        <UserLayout>
          <div className={classes.contWrapper}>
                <Tabs
                    value={tab}
                    indicatorColor="primary"
                    textColor="secondary"
                    onChange={hangleTab}
                    className={classes.tab}
                >
                    <Tab label="Users" />
                    <Tab label="Plans" /> 
                    <Tab label="Live Streams" />                    
                </Tabs>
                {tab == 0 && (
                    <UsersTab/>
                )}
                {tab == 1 && (
                    <PlansTab/>
                )}   
                {tab == 2 && (
                    <LiveStreamTab/>
                )}            
            </div>
        </UserLayout>
    )
}

export default AdminLayout

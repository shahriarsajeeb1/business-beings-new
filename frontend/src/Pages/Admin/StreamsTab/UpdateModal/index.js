import React, { useState,useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import utils from "@date-io/dayjs";
import {
  Button,
  InputBase,
  makeStyles,
  Typography,
  FormControl, 
} from "@material-ui/core";
import { updatedLiveStreamAdmin,getAllLiveStreamAdmin,deleteLiveStreamAdmin } from "../../../../services/LiveStream/action";
import AlertLiveStreamDialog from "../AlertDialog";
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import dayjs from "dayjs";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "calc(300px + (450 - 300) * ((100vw - 350px) / (2000 - 350)))",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },

  inputWrapper: {
    width: "calc(200px + (400 - 200) * ((100vw - 350px) / (2000 - 350)))",
    marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  text: {
    marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  subText: {
    marginBottom: "calc(3px + (10 - 3) * ((100vw - 350px) / (2000 - 350)))",
  },
  input: {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.common.white,
    border: "1px solid #ced4da",
    fontSize: 16,
    width: "100%",
    height: "calc(30px + (50 - 30) * ((100vw - 350px) / (2000 - 350)))",
    padding: "10px 12px",
    color: theme.palette.text.secondary,
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    "&:focus": {
      // boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      borderColor: theme.palette.primary.main,
      color: "black",
    },
  },
  buttonWrap:{
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    
  }
}));

const UpdateLiveStreamModal = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch(); 
  const liveStream = useSelector((state) => state.liveStream);
  const [updateData, setUpdateData] = useState(props.data); 

  useEffect(() => {
    if(liveStream.isSuccessUpdate){ 
      dispatch(getAllLiveStreamAdmin());     
    }   
    if(liveStream.isSuccessDelete){        
      dispatch(getAllLiveStreamAdmin());     
  }    
  }, [liveStream.isSuccessUpdate,liveStream.isSuccessDelete])

  const onChangeFields = (event) => {
    setUpdateData((prevData) => ({
      ...prevData,
      [event.target.name]: event.target.value,
    }));
  };
  
  const onChangeStartingDate = (event) => {
    setUpdateData((prevData) => ({
      ...prevData,
      ['startingDate']: event.toString(),
    }));
  }

  const onChangeEndingDate = (event) => {
    setUpdateData((prevData) => ({
      ...prevData,
      ['endingDate']: event.toString(),
    }));
  }

  const onClickUpdateButton = () => {
    dispatch(updatedLiveStreamAdmin(updateData));   
    
  };
  const onClickDeleteButton = () => {
    dispatch(deleteLiveStreamAdmin(updateData._id));   
    
  };

  

  return (
    <div className={classes.root}>
      <div>
        <Typography className={classes.text} varient="h6">
          Update Live Stream
        </Typography>
        <Typography className={classes.text} varient="h6"></Typography>
        <div className={classes.inputWrapper}>
          <FormControl>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Name
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="name"
              className={classes.input}
              value={updateData.name}
            />
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Starting Time
            </Typography>
            <MuiPickersUtilsProvider utils={utils}>
              <DateTimePicker
                className={classes.dateWrapper}                
                value={updateData.startingDate}
                onChange={onChangeStartingDate}
              />
            </MuiPickersUtilsProvider>
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Ending Time
            </Typography>
            <MuiPickersUtilsProvider utils={utils}>
              <DateTimePicker
                className={classes.dateWrapper}                
                value={updateData.endingDate}
                onChange={onChangeEndingDate}
              />
            </MuiPickersUtilsProvider>
          </FormControl>
        </div>       
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Description
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="description"
              className={classes.input}
              value={updateData.description}
            />
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Link
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="link"
              className={classes.input}
              value={updateData.link}
            />
          </FormControl>
        </div>
        
        
       

              <div className={classes.buttonWrap}>
              <Button
          style={{
            margin: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
          }}
          variant="contained"
          color="primary"
          onClick={onClickUpdateButton}
        >         
          Update Live Stream
        </Button>
        <Button
          style={{
            margin: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
          }}
          variant="contained"
          color="primary"
          onClick={onClickDeleteButton}
        >         
          Delete Live Stream
        </Button>
              </div>
        
      </div>
      {liveStream.isSuccessUpdate && <div> <AlertLiveStreamDialog view={true} dialogContent={'Live Stream Updated'} onClickModalClose={props.onClickUpdateModalClose} /></div>}
      {liveStream.isErrorUpdate && <div> <AlertLiveStreamDialog view={true} dialogContent={'Error Occured.Try Again'} onClickModalClose={props.onClickUpdateModalClose} /></div>}
      {liveStream.isSuccessDelete && <div> <AlertLiveStreamDialog view={true} dialogContent={'Live Stream Deleted'} onClickModalClose={props.onClickUpdateModalClose} /></div>}
      {liveStream.isErrorDelete && <div> <AlertLiveStreamDialog view={true} dialogContent={'Error Occured.Try Again'} onClickModalClose={props.onClickUpdateModalClose} /></div>}
      
    </div>
  );
};

export default UpdateLiveStreamModal;

import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableFooter from "@material-ui/core/TableFooter";
import TablePagination from "@material-ui/core/TablePagination";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import { Button } from "@material-ui/core";
import Modal from "@material-ui/core/Modal";
import { getAllLiveStreamAdmin } from "../../../services/LiveStream/action";
import CreateLiveStreamModal from "./CreateModal";
import UpdateLiveStreamModal from "./UpdateModal";


const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
    "& .MuiTablePagination-toolbar": {
      overflow: "hidden",
    },
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

const useStyles2 = makeStyles({
  table: {
    minWidth: 300,
  },
  buttonDiv: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    minHeight: "70px",
    "& button": {
      marginLeft: "20px",
    },
  },
  modal: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
});

const LiveStreamTab = () => {
  const classes = useStyles2();
  const dispatch = useDispatch();
  const [page, setPage] = useState(0);
  const [tableItemData, setTableItemData] = useState({});
  const [createModalOpen, setCreateModalOpen] = useState(false);
  const [updateModalOpen, setUpdateModalOpen] = useState(false);
  const liveStreams = useSelector((state) => state.liveStream.liveStreams);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const emptyRows = rowsPerPage - Math.min(rowsPerPage, liveStreams.length - page * rowsPerPage);

  useEffect(() => {
    dispatch(getAllLiveStreamAdmin());
   
  }, []);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const onClickCreateButton = () => {
    setCreateModalOpen(true);
  };
  const onClickCreateModalClose = () => {
    setCreateModalOpen(false);
  };
  const onClickUpdateButton = (row) => {
    setTableItemData(row);    
    setUpdateModalOpen(true);
  };
  const onClickUpdateModalClose = () => {
    setUpdateModalOpen(false);
  };

  return (
    <div>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="custom pagination table">
          <TableHead>
            <TableRow>
              <TableCell align="right">Live Stream Id</TableCell>
              <TableCell align="right">Name</TableCell>
              <TableCell align="right">startingDate</TableCell>
              <TableCell align="right">endingDate</TableCell>
              <TableCell align="right">Link</TableCell>                            
            </TableRow>
          </TableHead>
          <TableBody>
            {(rowsPerPage > 0
              ? liveStreams.slice(
                  page * rowsPerPage,
                  page * rowsPerPage + rowsPerPage
                )
              : liveStreams
            ).map((row) => (
              <TableRow
                key={row._id}
                onClick={()=>onClickUpdateButton(row)}
              >
                <TableCell >{row._id}</TableCell>
                <TableCell  align="right">
                  {row.name}
                </TableCell>
                <TableCell  align="right">
                  {row.startingDate}
                </TableCell>
                <TableCell  align="right">
                  {row.endingDate}
                </TableCell>
                {/* <TableCell  align="right" style={{textOverflow:'ellipsis'}}>
                  {row.description}
                </TableCell>   */}
                <TableCell  align="right">
                  {row.link}
                </TableCell>               
                
              </TableRow>
            ))}

            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
                colSpan={6}
                count={liveStreams.length}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  inputProps: { "aria-label": "rows per page" },
                  native: true,
                }}
                onPageChange={handleChangePage}
                onRowsPerPageChange={handleChangeRowsPerPage}
                ActionsComponent={TablePaginationActions}
              />
            </TableRow>
          </TableFooter>
        </Table>
        <div className={classes.buttonDiv}>
          <Button
            color="primary"
            variant="contained"
            onClick={onClickCreateButton}
          >
            Create Live Stream
          </Button>
        </div>
      </TableContainer>
      <Modal
        open={createModalOpen}
        onClose={onClickCreateModalClose}
        className={classes.modal}
       
      >
        <div>

        <CreateLiveStreamModal
          onClickCreateModalClose={onClickCreateModalClose}          
          />
          </div>
      </Modal>
      <Modal
        open={updateModalOpen}
        onClose={onClickUpdateModalClose}
        className={classes.modal}
        
      >
        <div>
        <UpdateLiveStreamModal
          onClickUpdateModalClose={onClickUpdateModalClose}
          data={tableItemData}          
          />
          </div>
      </Modal>
    </div>
  );
}

export default LiveStreamTab;


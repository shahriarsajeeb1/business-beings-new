import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import utils from "@date-io/dayjs";
import {
  Button,
  InputBase,
  makeStyles,
  Typography,
  FormControl,
} from "@material-ui/core";
import {
  createLiveStreamAdmin,
  getAllLiveStreamAdmin,
  updatedLiveStreamAdmin,
} from "../../../../services/LiveStream/action";
import AlertLiveStreamDialog from "../AlertDialog";
import { DateTimePicker, MuiPickersUtilsProvider } from "@material-ui/pickers";
import storage from "../../../../firebase"
import { ref,uploadBytesResumable,getDownloadURL } from "firebase/storage";
import dayjs from "dayjs";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "calc(300px + (450 - 300) * ((100vw - 350px) / (2000 - 350)))",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },

  inputWrapper: {
    width: "calc(200px + (400 - 200) * ((100vw - 350px) / (2000 - 350)))",
    marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  text: {
    marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  subText: {
    marginBottom: "calc(3px + (10 - 3) * ((100vw - 350px) / (2000 - 350)))",
  },
  input: {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.common.white,
    border: "1px solid #ced4da",
    fontSize: 16,
    width: "100%",
    height: "calc(30px + (50 - 30) * ((100vw - 350px) / (2000 - 350)))",
    padding: "10px 12px",
    color: theme.palette.text.secondary,
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    "&:focus": {
      // boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      borderColor: theme.palette.primary.main,
      color: "black",
    },
  },
  dateWrapper: {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.common.white,
    border: "1px solid #ced4da",
    fontSize: 16,
    width: "100%",
    height: "calc(30px + (50 - 30) * ((100vw - 350px) / (2000 - 350)))",
    padding: "10px 12px",
    color: theme.palette.text.secondary,
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    justifyContent: "center",
    "&:focus": {
      // boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      borderColor: theme.palette.primary.main,
      color: "black",
    },
    "& .MuiFormControl-root": {},
    "& .MuiInput-underline:before": {
      border: "0px solid white",
    },
    "& .MuiInput-underline:after": {
      border: "0px solid white",
    },
    "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
      border: "0px solid white",
    },
    "& p": {
      display: "none",
    },
  },
}));

const CreateLiveStreamModal = ({ onClickCreateModalClose }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const liveStream = useSelector((state) => state.liveStream);
  const [createData, setCreateData] = useState({
    name: "",
    startingDate: "",
    endingDate:'',
    category:'',
    interest:'',
    description: "",
    link: "",
  });
  const [image, setImage] = useState(null);
  const [imageurl, setImageurl] = useState('');

  useEffect(() => {
    if (liveStream.isSuccessCreate) {
      dispatch(getAllLiveStreamAdmin());
    }
    if(liveStream.isSuccessCreate && imageurl){
      console.log(liveStream.successMessageCreate)      
      dispatch(updatedLiveStreamAdmin({
          _id:liveStream.successMessageCreate,
          image:imageurl,         
      }))
  }
  }, [liveStream.isSuccessCreate,imageurl]);

  const onChangeFields = (event) => {
    setCreateData((prevData) => ({
      ...prevData,
      [event.target.name]: event.target.value,
    }));
    console.log(createData);
  };

  const onChangeStartingDate = (event) => {
    setCreateData((prevData) => ({
      ...prevData,
      ['startingDate']: event.toString(),
    }));
  }

  const onChangeEndingDate = (event) => {
    setCreateData((prevData) => ({
      ...prevData,
      ['endingDate']: event.toString(),
    }));
  }

  const onChangeImage = (event) => {        
    setImage(event.target.files[0]);     
}


  const handleSubmitButton = (e) => {
    dispatch(createLiveStreamAdmin(createData));
    if(image){
      e.preventDefault();
  const imageRef = ref(storage,`/liveStreams/${image.name}`);
  const uploadTask = uploadBytesResumable(imageRef,image);   
  uploadTask.on("state_changed", 
  (snapshot) => {
      // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
      const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
      console.log('Upload is ' + progress + '% done');
      switch (snapshot.state) {
        case 'paused':
          console.log('Upload is paused');
          break;
        case 'running':
          console.log('Upload is running');
          break;
      }
    }, 
    (error) => {        
      switch (error.code) {
        case 'storage/unauthorized':
          // User doesn't have permission to access the object
          break;
        case 'storage/canceled':
          // User canceled the upload
          break;
  
        // ...
  
        case 'storage/unknown':
          // Unknown error occurred, inspect error.serverResponse
          break;
      }
    }, 
    () => {
      getDownloadURL(uploadTask.snapshot.ref)
      .then((url) => {
        setImageurl(url)
      });
  });
     }
  };

  return (
    <div className={classes.root}>
      <div>
        <Typography className={classes.text} varient="h6">
          Create Live Stream
        </Typography>
        <Typography className={classes.text} varient="h6"></Typography>
        <div className={classes.inputWrapper}>
          <FormControl>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Name
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="name"
              className={classes.input}
            />
          </FormControl>
        </div>        
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Starting Time
            </Typography>
            <MuiPickersUtilsProvider utils={utils}>
              <DateTimePicker
                className={classes.dateWrapper}                
                value={createData.startingDate}
                onChange={onChangeStartingDate}
              />
            </MuiPickersUtilsProvider>
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Ending Time
            </Typography>
            <MuiPickersUtilsProvider utils={utils}>
              <DateTimePicker
                className={classes.dateWrapper}                
                value={createData.endingDate}
                onChange={onChangeEndingDate}
              />
            </MuiPickersUtilsProvider>
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Category
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="category"
              className={classes.input}
            />
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Interest
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="interest"
              className={classes.input}
            />
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Description
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="description"
              className={classes.input}
            />
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Image
            </Typography>
            <InputBase onChange={onChangeImage}  type='file'  className={classes.input} />
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Link
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="link"
              className={classes.input}
            />
          </FormControl>
        </div>
        <Button
          style={{
            margin: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
          }}
          variant="contained"
          color="primary"
          onClick={handleSubmitButton}
        >
          Submit
        </Button>
      </div>
      {liveStream.isSuccessCreate && liveStream.isSuccessUpdate && (
        <AlertLiveStreamDialog
          view={true}
          dialogContent={"Live Stream Created"}
          onClickModalClose={onClickCreateModalClose}
        />
      )}
      {liveStream.isErrorCreate && (
        <AlertLiveStreamDialog
          view={true}
          dialogContent={"Error Occured.Try Again"}
          onClickModalClose={onClickCreateModalClose}
        />
      )}
    </div>
  );
};

export default CreateLiveStreamModal;

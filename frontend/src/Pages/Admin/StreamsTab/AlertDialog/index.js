import React,{useState} from 'react';
import { useDispatch } from 'react-redux'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { createLiveStreamReturnInitialStateAdmin, updateLiveStreamReturnInitialStateAdmin,deleteLiveStreamReturnInitialStateAdmin } from '../../../../services/LiveStream/action';
// import { useDispatch } from 'react-redux';


const AlertLiveStreamDialog = (props) => {
  const [open, setOpen] = useState(props.view); 
  const dispatch = useDispatch();
  

  const handleClose = () => {
    setOpen(false);
    props.onClickModalClose();
    dispatch(createLiveStreamReturnInitialStateAdmin());
    dispatch(updateLiveStreamReturnInitialStateAdmin());
    dispatch(deleteLiveStreamReturnInitialStateAdmin());
  };

  return (
     
      <Dialog
        open={open}
        onClose={handleClose}        
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
         >
        
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {props.dialogContent}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    
  );
}
export default AlertLiveStreamDialog
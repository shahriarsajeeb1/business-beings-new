import React, { useState,useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Button,
  InputBase,
  makeStyles,
  Typography,
  FormControl, 
} from "@material-ui/core";
import { createPlanAdmin,getAllPlanAdmin } from "../../../../services/PlanData/action";
import AlertPlanDialog from "../AlertDialog";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "calc(300px + (450 - 300) * ((100vw - 350px) / (2000 - 350)))",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },

  inputWrapper: {
    width: "calc(200px + (400 - 200) * ((100vw - 350px) / (2000 - 350)))",
    marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  text: {
    marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  subText: {
    marginBottom: "calc(3px + (10 - 3) * ((100vw - 350px) / (2000 - 350)))",
  },
  input: {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.common.white,
    border: "1px solid #ced4da",
    fontSize: 16,
    width: "100%",
    height: "calc(30px + (50 - 30) * ((100vw - 350px) / (2000 - 350)))",
    padding: "10px 12px",
    color: theme.palette.text.secondary,
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    "&:focus": {
      // boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      borderColor: theme.palette.primary.main,
      color: "black",
    },
  },
}));

const CreatePlanModal = ( {onClickCreateModalClose}) => {
  const classes = useStyles();
  const dispatch = useDispatch(); 
  const plan = useSelector((state) => state.plan);
  const [createData, setCreateData] = useState({
    name:'',
    type: '',
    duration: 0,
    price: 0,    
  });

  useEffect(() => {
    if(plan.isSuccessCreate){        
        dispatch(getAllPlanAdmin());     
    }    
  }, [plan.isSuccessCreate])

  const onChangeFields = (event) => {
    setCreateData((prevData) => ({
      ...prevData,
      [event.target.name]: event.target.value,
    }));
  };

  const handleSubmitButton = () => {
    dispatch(createPlanAdmin(createData));   
    
  };

  

  return (
    <div className={classes.root}>        
      <div>
        <Typography className={classes.text} varient="h6">
          Create Plan
        </Typography>
        <Typography className={classes.text} varient="h6"></Typography>
        <div className={classes.inputWrapper}>
          <FormControl>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Name
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="name"
              className={classes.input}
            />
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Type
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="type"
              className={classes.input}
            />
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Duration
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="duration"
              className={classes.input}
            />
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Price
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="price"
              className={classes.input}
            />
          </FormControl>
        </div>       
        
        <Button
          style={{
            margin: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
          }}
          variant="contained"
          color="primary"
          onClick={handleSubmitButton}
        >         
          Submit
        </Button>
      </div>
      {plan.isSuccessCreate && <AlertPlanDialog view={true} dialogContent={'Plan Created'} onClickModalClose={onClickCreateModalClose} />}
      {plan.isErrorCreate && <AlertPlanDialog view={true} dialogContent={'Error Occured.Try Again'} onClickModalClose={onClickCreateModalClose} />}
      
    </div>
  );
};

export default CreatePlanModal;

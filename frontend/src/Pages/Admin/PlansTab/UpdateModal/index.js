import React, { useState,useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import {
  Button,
  InputBase,
  makeStyles,
  Typography,
  FormControl, 
} from "@material-ui/core";
import { updatedPlanAdmin,getAllPlanAdmin,deletePlanAdmin } from "../../../../services/PlanData/action";
import AlertPlanDialog from "../AlertDialog";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "calc(300px + (450 - 300) * ((100vw - 350px) / (2000 - 350)))",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    backgroundColor: theme.palette.background.paper,
    border: "2px solid #000",
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },

  inputWrapper: {
    width: "calc(200px + (400 - 200) * ((100vw - 350px) / (2000 - 350)))",
    marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  text: {
    marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  subText: {
    marginBottom: "calc(3px + (10 - 3) * ((100vw - 350px) / (2000 - 350)))",
  },
  input: {
    borderRadius: 4,
    position: "relative",
    backgroundColor: theme.palette.common.white,
    border: "1px solid #ced4da",
    fontSize: 16,
    width: "100%",
    height: "calc(30px + (50 - 30) * ((100vw - 350px) / (2000 - 350)))",
    padding: "10px 12px",
    color: theme.palette.text.secondary,
    transition: theme.transitions.create(["border-color", "box-shadow"]),
    "&:focus": {
      // boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
      borderColor: theme.palette.primary.main,
      color: "black",
    },
  },
  buttonWrap:{
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    
  }
}));

const UpdatePlanModal = (props) => {
  const classes = useStyles();
  const dispatch = useDispatch(); 
  const plan = useSelector((state) => state.plan);
  const [updateData, setUpdateData] = useState(props.data); 

  useEffect(() => {
    if(plan.isSuccessUpdate){        
        dispatch(getAllPlanAdmin());     
    }   
    if(plan.isSuccessDelete){        
      dispatch(getAllPlanAdmin());     
  }    
  }, [plan.isSuccessUpdate,plan.isSuccessDelete])

  const onChangeFields = (event) => {
    setUpdateData((prevData) => ({
      ...prevData,
      [event.target.name]: event.target.value,
    }));
  };

  const onClickUpdateButton = () => {
    dispatch(updatedPlanAdmin(updateData));   
    
  };
  const onClickDeleteButton = () => {
    dispatch(deletePlanAdmin(updateData._id));   
    
  };

  

  return (
    <div className={classes.root}>
      <div>
        <Typography className={classes.text} varient="h6">
          Update User
        </Typography>
        <Typography className={classes.text} varient="h6"></Typography>
        <div className={classes.inputWrapper}>
          <FormControl>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Name
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="name"
              className={classes.input}
              value={updateData.name}
            />
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}              
            >
              Type
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="type"
              className={classes.input}
              value={updateData.type}
            />
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Duration
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="duration"
              className={classes.input}
              value={updateData.duration}
            />
          </FormControl>
        </div>
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <Typography
              varient="subtitle1"
              color="textSecondary"
              className={classes.subText}
            >
              Price
            </Typography>
            <InputBase
              onChange={onChangeFields}
              name="price"
              className={classes.input}
              value={updateData.price}
            />
          </FormControl>
        </div>
        
        
       

              <div className={classes.buttonWrap}>
              <Button
          style={{
            margin: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
          }}
          variant="contained"
          color="primary"
          onClick={onClickUpdateButton}
        >         
          Update User
        </Button>
        <Button
          style={{
            margin: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
          }}
          variant="contained"
          color="primary"
          onClick={onClickDeleteButton}
        >         
          Delete User
        </Button>
              </div>
        
      </div>
      {plan.isSuccessUpdate && <div> <AlertPlanDialog view={true} dialogContent={'Account Updated'} onClickModalClose={props.onClickUpdateModalClose} /></div>}
      {plan.isErrorUpdate && <div> <AlertPlanDialog view={true} dialogContent={'Error Occured.Try Again'} onClickModalClose={props.onClickUpdateModalClose} /></div>}
      {plan.isSuccessDelete && <div> <AlertPlanDialog view={true} dialogContent={'Account Deleted'} onClickModalClose={props.onClickUpdateModalClose} /></div>}
      {plan.isErrorDelete && <div> <AlertPlanDialog view={true} dialogContent={'Error Occured.Try Again'} onClickModalClose={props.onClickUpdateModalClose} /></div>}
      
    </div>
  );
};

export default UpdatePlanModal;

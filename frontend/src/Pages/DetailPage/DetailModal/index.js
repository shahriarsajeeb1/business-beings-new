import React, { useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import {  
  Modal,makeStyles, Typography  
} from "@material-ui/core";
import ReactJWPlayer from "react-jw-player";
import { getMediaReturnInitialState } from "../../../services/Streamingdata/action";

const useStyles = makeStyles((themes)=>({
    modal: {
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      },
}))

const DetailModal = ({view}) => {
    const classes = useStyles();
    const [playerModalOpen, setPlayerModalOpen] = useState(view);
    const stream = useSelector((state) => state.data);
    const dispatch = useDispatch();


    const onClickPlayerModalClose = () => {
        setPlayerModalOpen(false);
        dispatch(getMediaReturnInitialState());
      };

    return (
        <div>
             <Modal
        open={
          playerModalOpen &&
          stream.isSuccessMedia &&
          stream.mediaUrl &&
          stream.playerUrl
        }
        onClose={onClickPlayerModalClose}
        className={classes.modal}
      >
        <div className="jw-video-container"
         style={{width:'80%',height:'80%'}}
        >
          <ReactJWPlayer
            playerId="abc123"
            playerScript="https://cdn.jwplayer.com/libraries/cKGhgIRA.js"
            playlist="https://cdn.jwplayer.com/v2/media/xzis89W7"
          />
          {/* https://cdn.jwplayer.com/libraries/ekbdoSbe.js */}
        </div>
        
      </Modal>
        </div>
    )
}

export default DetailModal

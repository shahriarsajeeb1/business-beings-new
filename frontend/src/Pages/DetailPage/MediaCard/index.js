import React, { useState,useRef,useEffect } from "react";
import { useDispatch } from "react-redux";
import {
  Button,
  Card,
  CardContent,
  CardMedia, 
  makeStyles,
  Typography, 
} from "@material-ui/core";
import { getMediaData,getMediaReturnInitialState } from "../../../services/Streamingdata/action";


const usestyles = makeStyles((theme)=>({
  card: {
    width: "calc(100px + (500 - 100) * ((100vw - 350px) / (2000 - 350)))",
    height: "calc(75px + (300 - 75) * ((100vw - 350px) / (2000 - 350)))",
    borderRadius: "17px",
    position: "relative",
    cursor: "pointer",
    margin: "0.7vw",
    "&:hover": {
      border: "1px solid green",
      transform: "scale(1.08)",
    },
  },
  image: {
    width: "calc(100px + (500 - 100) * ((100vw - 350px) / (2000 - 350)))",
    height: "calc(75px + (300 - 75) * ((100vw - 350px) / (2000 - 350)))",
  },

  cont: {
    width: "100%",
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center",
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    background: "rgba(10, 9, 9, 0.65)",
    color: "white",
    [theme.breakpoints.down('sm')]:{
      display:'none',
  }
  },
  desc: {
    textAlign: "center",
    maxHeight: "80px",
    textOverflow: "ellipsis",
    overflow: "hidden",
  },
  modal: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
}));

const MediaCard = (props) => {
  const [visible, setVisible] = useState(false); 
  const classes = usestyles();
  const dispatch = useDispatch();
      

  const visibilities = () => {
    setVisible(true);
  };
  
  const notVisibilities = () => {
    setVisible(false);
  };

  const handlePlayButton = () => {
    const id = props.stream.id;
    dispatch(getMediaReturnInitialState())
    dispatch(getMediaData(id));
    window.scrollTo(0, 0);
    
  };

  
  

  return (
    <div>
      <Card
        onMouseEnter={visibilities}
        onMouseLeave={notVisibilities}
        className={classes.card}
      >
        <CardMedia
          component="img"
          className={classes.image}
          image={props.stream.image}
          title={props.stream.title}
        />
        {visible ? (
          <CardContent className={classes.cont} onClick={handlePlayButton}>
            <Typography variant="h6">{props.stream.title}</Typography>
            <Typography variant="subtitle1" className={classes.desc}>
              {props.stream.description}
            </Typography>
            <Button
              color="primary"
              variant="contained"
              
            >
              Play Now
            </Button>
          </CardContent>
        ) : null}
      </Card>  
    </div>
  );
};

export default MediaCard;

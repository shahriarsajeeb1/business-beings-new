import React,{useState,useEffect} from 'react';
import { useSelector,useDispatch } from 'react-redux'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import { getMediaReturnInitialState } from "../../../services/Streamingdata/action";



const AlertDialogMedia = ({view}) => {
  const [alertOpen, setAlertOpen] = useState(view);
  const dispatch = useDispatch();
  
  

  const handleClose = () => {
    setAlertOpen(false);
    dispatch(getMediaReturnInitialState());    
  };

  return (
     
      <Dialog
        open={alertOpen}
        onClose={handleClose}        
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
        >
        
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
          Please Upgrade Your Plan.Upgrade option available in profile section
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Close
          </Button>
        </DialogActions>
      </Dialog>
    
  );
}
export default AlertDialogMedia
import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import dayjs from "dayjs";
import { useParams } from "react-router-dom";
import Header from "../../Components/Header";
import CardSlider from "../Home/CardSlider";
import Carding from "../../Components/Card";
import {
  makeStyles,
  Typography,
  Backdrop,
  CircularProgress,
  Button,
  Hidden,
  Card
} from "@material-ui/core";
import AlertDialogMedia from "./AlertDialog";
import { getMediaData, getMediaReturnInitialState, getStreamData, updateLike } from "../../services/Streamingdata/action";
import ReactJWPlayer from "react-jw-player";
import ThumbUpAltIcon from "@material-ui/icons/ThumbUpAlt";
import PlaylistAddCheckRoundedIcon from "@material-ui/icons/PlaylistAddCheckRounded";
import { updatedUser } from "../../services/Userdata/action";
import CloseOutlinedIcon from '@material-ui/icons/CloseOutlined';
import { Helmet } from "react-helmet";

const useStyles = makeStyles((theme) => ({
  contWrapper: {
    width: "95%",
    flexDirection: "column",
    display: "flex",
    margin: "auto",
  },
  cont: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    flexDirection: "row",
    flexWrap: "wrap",
    marginTop: "calc(5px + (10 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  textWrapper: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    flexDirection: "column",
    width: "40%",
    margin: "calc(5px + (10 - 5) * ((100vw - 350px) / (2000 - 350)))",
    [theme.breakpoints.down("sm")]: {
      width: "90%",
    },
  },
  textCont: {
    marginTop: "calc(5px + (10 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  text: {
    color:
      theme.palette.type == "light"
        ? theme.palette.primary.dark
        : theme.palette.common.white,
  },
  buttons: {
    width: "70%",
    display: "flex",
    alignItems: "center",
    marginTop: "calc(15px + (30 - 15) * ((100vw - 350px) / (2000 - 350)))",
    marginBottom: "calc(15px + (30 - 15) * ((100vw - 350px) / (2000 - 350)))",
    [theme.breakpoints.down("sm")]: {
      width: "90%",
    },
  },
  imgWrapper: {
    width: "40%",
    height: "100%",
  },
  descWrapper: {
    width: "90%",
  },
  epidsodes: {
    marginTop: "calc(15px + (30 - 15) * ((100vw - 350px) / (2000 - 350)))",
  },
  recent: {
    marginTop: "calc(15px + (30 - 15) * ((100vw - 350px) / (2000 - 350)))",
    marginBottom: "calc(30px + (45 - 35) * ((100vw - 350px) / (2000 - 350)))",
  },
  videoWrapper: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    flexWrap: "wrap",
  },
  jwVideoContainer: {
    position:'relative',
    width: "90vw",
    height: "auto",
    transitionProperty:'height',
  transitionduration:'10s',
  marginLeft: 'auto',
  marginRight: 'auto',
  },
  jwVideoClose:{
    position:'absolute',
    top:'10px',right:'10px',
    width:"calc(30px + (40 - 30) * ((100vw - 350px) / (2000 - 350)))",
    height:"calc(30px + (40 - 30) * ((100vw - 350px) / (2000 - 350)))",
    cursor:'pointer',
    zIndex:10,    
  },
  cardClose:{
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  }
}));

const Detail = () => {
  const classes = useStyles();
  const params = useParams();
  const dispatch = useDispatch();
  const data = useSelector((state) => state.data);
  const streams = useSelector((state) => state.data.streams);
  const stream = streams.find((dat) => dat.id == params.id);
  const user = useSelector((state) => state.user.user);
  const recent = [...streams].sort(
    (a, b) => new dayjs(b.created) - new dayjs(a.created)
  );
  const [alertOpen, setAlertOpen] = useState(data.isErrorMedia);
  const [liked, setLiked] = useState(false);
  const [bookMarked, setBookMarked] = useState(false);
  const [playing, setPlaying] = useState(false);
  const [mediaUrl, setMediaUrl] = useState('')
  const [playerUrl, setPlayerUrl] = useState('')

  useEffect(() => {    
    if (stream) {
      setMediaUrl(`https://cdn.jwplayer.com/v2/media/${stream.playlistItems[0].id}`);
      setPlayerUrl('https://cdn.jwplayer.com/libraries/ekbdoSbe.js')     
    } 
  }, [stream])

  useEffect(() => {    
    if (streams == []) {
      dispatch(getStreamData());
    }
    if (user.liked && user.bookMarked) {
      setLiked(user.liked.includes(params.id));
      setBookMarked(user.bookMarked.includes(params.id));
    } else {
      setBookMarked(true);
      setLiked(true);
    }

    if (data.isErrorMedia) {
      setAlertOpen(true);
    } else {
      setAlertOpen(false);
    }
    if (data.isSuccessMedia) {
      setPlaying(true);
    } else {
      setPlaying(false);
    }
  }, [data.isErrorMedia, data.isSuccessMedia, streams,user]);

  const handleLike = () => {
    dispatch(updateLike());
    dispatch(
      updatedUser({
        liked: params.id,
        password: "",
      })
    );
    setLiked(true);
  };

  const handleWatchList = () => {
    dispatch(
      updatedUser({
        bookMarked: params.id,
        password: "",
      })
    );
  };

  const onClickClose = ()=>{
    setPlaying(false);
    dispatch(getMediaReturnInitialState())
  }
  return (
    <div>      
      {streams && stream ?(<div>
        <Helmet>
        <title>{stream.title}</title>
        <meta name='description' content={stream.description}/>
        <meta name='keywords' content={stream.title}/>
      </Helmet>
        <div className={classes.contWrapper}>         
          {playing && (
            <div className={classes.jwVideoContainer}>
              <ReactJWPlayer
                playerId="abc123"
                playerScript={data.playerUrl}
                playlist={data.mediaUrl}
              />
              <div className={classes.jwVideoClose} onClick={onClickClose}>
                <Card className={classes.cardClose}>
                <CloseOutlinedIcon/>
                </Card>
              </div>
              {/* https://cdn.jwplayer.com/libraries/ekbdoSbe.js */}
            </div>
          )}
          <div className={classes.cont}>
            <div></div>
            <div className={classes.textWrapper}>
              <Typography variant="h4">{stream.title}</Typography>
              <div className={classes.buttons}>
                <Button
                  disabled={liked}
                  onClick={handleLike}
                  variant="contained"
                  color="primary"
                >
                  <ThumbUpAltIcon />
                  {liked ? "Liked" : "Like"}
                </Button>
                <Button
                  disabled={liked}
                  variant="outlined"
                  onClick={handleWatchList}
                  style={{ marginLeft: "20px" }}
                  className={classes.text}
                >
                  <PlaylistAddCheckRoundedIcon />

                  {bookMarked ? "Added to watchList" : "Add to watchList"}
                </Button>
              </div>
              <Typography variant="subtitle1" className={classes.textCont}>
                Language:
                <Typography
                  display="inline"
                  className={classes.text}
                  variant="subtitle1"
                >
                  Tamil
                </Typography>
              </Typography>

              <Typography variant="subtitle1" className={classes.textCont}>
                Type:
                <Typography
                  display="inline"
                  className={classes.text}
                  variant="subtitle1"
                >
                  {stream.params.type}
                </Typography>
              </Typography>
              <Typography variant="subtitle1" className={classes.textCont}>
                Budget:
                <Typography
                  display="inline"
                  className={classes.text}
                  variant="subtitle1"
                >
                  {stream.params.budget}
                </Typography>
              </Typography>
              <Typography variant="subtitle1" className={classes.textCont}>
                Category:
                <Typography
                  display="inline"
                  className={classes.text}
                  variant="subtitle1"
                >
                  {stream.params.category}
                </Typography>
              </Typography>
              <Typography variant="subtitle1" className={classes.textCont}>
                Interest:
                <Typography
                  display="inline"
                  className={classes.text}
                  variant="subtitle1"
                >
                  {stream.params.interest}
                </Typography>
              </Typography>
            </div>
            <Hidden smDown>
              {!playing && (
                <div className={classes.imgWrapper}>
                  <ReactJWPlayer
                playerId="abc123"
                playerScript={playerUrl}
                playlist={mediaUrl}
              />
                </div>
              )}
            </Hidden>
            <div className={classes.descWrapper}>
              <Typography variant="subtitle1">{stream.description}</Typography>
            </div>
          </div>
          <div className={classes.epidsodes}>
            <Typography variant="h5">Season 1</Typography>
            <CardSlider type={stream.playlistItems} path="detailPage" />
          </div>
          <div className={classes.recent}>
            <Typography variant="h5">Recent Videos</Typography>
            <div className={classes.videoWrapper}>
              {streams
                .sort((a, b) => new dayjs(b.created) - new dayjs(a.created))
                .map((dat) => {
                  return (
                    <div>
                      <Carding stream={dat} />
                    </div>
                  );
                })}
            </div>
          </div>
          {alertOpen && <AlertDialogMedia view={alertOpen} />}
        </div>
        </div>
      ):
      <Backdrop
        className={classes.backdrop}
        // open={data.isLoadingMedia || data.isLoadingStream}
        open = {streams == []}
      >
        <CircularProgress color="inherit" />
      </Backdrop>}
    </div>
  );
};

export default Detail;

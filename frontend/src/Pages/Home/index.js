import React,{ useEffect, useState } from "react";
import { useSelector,useDispatch } from "react-redux";
import CardSlider from "./CardSlider";
import Filter from "../../Components/Filter";
import Footer from "../../Components/Footer";
import { makeStyles, Typography,Tabs,Tab,Modal, Hidden,Backdrop,CircularProgress } from "@material-ui/core";
import dayjs from 'dayjs';
import { getAllLiveStreamAdmin } from '../../services/LiveStream/action'


import Header from "../../Components/Header";
import CarouselWrap from "./Carousal";
import { updateInvitedFriend, updateModalOpen } from "../../services/Styles/action";
import Login from "../Login";
import { useParams } from "react-router-dom";

const usestyles = makeStyles((theme)=>({
  slider: {
    width: "100%",
    padding: "0.7vw",
    
  },
  sliderHeading: {
    width: "100%",
    padding: "0.7vw",
    '& .MuiTabs-root':{     
      marginTop: theme.spacing(3),
      marginBottom:theme.spacing(2),

    },
    '& .MuiTab-root':{
      fontSize:theme.typography.h5.fontSize,
      fontWeight: 'light',
      textTransform: 'none',
    }
    
  },
  filterHeading: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    textAlign: "center",
    margin: "3%",
  },
  filterWrapper: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    flexDirection: "column",
    margin: "3%",
    [theme.breakpoints.down('sm')]:{
      width: "80%", 
      padding: "20px",
  }
  },
  font: {
    fontSize: "calc(80px+(96-80)*((100vw-350px)/(2000-350)))",
  },
  modal:{
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    
  }
}));
const Home = () => {
  const dispatch = useDispatch();
  const params = useParams();
  const classes = usestyles();
  const [tab, setTab] = useState(0)
  const [tab1, setTab1] = useState(0)
  const liveStream = useSelector(state => state.liveStream.liveStreams)
  const stream = useSelector(state => state.data)
  const user = useSelector(state => state.user.user)
  const data = useSelector(state => state.data.streams)    
  const modalOpen = useSelector(state => state.style.modalOpen)
  // const popular = [...data].sort((a,b) => Number(b.likes) -Number(a.likes))
  const trend = [...data].sort((a,b) => (b.likes / (new dayjs(b.created).diff(dayjs(),'days'))) - (a.likes / (new dayjs(a.created).diff(new dayjs(),'days'))));
  const watchLater = user.bookMarked? data.filter(dat => user.bookMarked.includes(dat.id)): []
  const recent = [...data].sort((a,b) => new dayjs(b.created) - new dayjs(a.created))
  
  const hangleTab = (event, value) => {
    setTab(value);
  };
  const hangleTab1 = (event, value) => {
    setTab1(value);
  }; 

  const handleModalClose = () => {
    dispatch(updateModalOpen(false))
}

useEffect(() => {
  dispatch(getAllLiveStreamAdmin());
  if(params.id){
    dispatch(updateInvitedFriend(params.id));
    dispatch(updateModalOpen(true));
  }
  
}, [])

  return (
    <div>
      {/* <div>
        <Header />
      </div> */}
      <div className={classes.wrapper}>
        <CarouselWrap />
      </div>
      <div className={classes.slider}>
        <div className={classes.sliderHeading}>
          <Tabs
            value={tab}           
            textColor="white"
            onChange={hangleTab}
          >
            <Tab label="Popular" />
            <Tab label="Recent Videos" />
            <Tab label="Trending" />
          </Tabs>
          {tab == 0 && <CardSlider type={recent} />}
          {tab == 1 && <CardSlider type={recent}/>}
          {tab == 2 && <CardSlider type={trend}/>}
        </div>
      </div>
      {user._id && user.bookMarked.length && <div className={classes.slider}>
        <div className={classes.sliderHeading}>
        <Tabs
            value={tab1}           
            textColor="white"
            onChange={hangleTab1}
          >
            <Tab label="Continue Watching" />
            
          </Tabs>
          {tab1 == 0 && <CardSlider type={watchLater}/>}
          
        </div>
        
      </div>}
     {liveStream.length && <div className={classes.slider}>
        <div className={classes.sliderHeading}>
        <Tabs
            value={tab1}           
            textColor="white"
            onChange={hangleTab1}
          >
            <Tab label="Live Streams" />
            
          </Tabs>
          {tab1 == 0 && <CardSlider path='liveStream' type={liveStream}/>}
          
        </div>
        
      </div>}
      
      <div className={classes.filterHeading}>
        <Hidden mdDown>
        <Typography variant="h3">
          Get Business Ideas Based on Your Budget & Interests
        </Typography>
        </Hidden>
        <Hidden lgUp>
        <Typography variant="h4">
          Get Business Ideas Based on Your Budget & Interests
        </Typography>
        </Hidden>
        <Typography variant="subtitle1">
          Explore Business ideas, case studies & Interviews
        </Typography>
      </div>
      <div className={classes.filterWrapper}>
        <Filter />
      </div>
      
      

      <Modal
      className={classes.modal}
      open={modalOpen}
      onClose={handleModalClose}>
        <div>

<Login/>
        </div>
      </Modal>
      <Backdrop className={classes.backdrop} open={stream.isLoadingStream}>
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
};

export default Home;

import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";
import IconButton from "@material-ui/core/IconButton";
import { makeStyles } from "@material-ui/core/styles";

import Carding from "../../../Components/Card";
import MediaCard from "../../DetailPage/MediaCard";
import LiveStreamCard from "../LiveStreamCard";

const useStyles = makeStyles({
  wrapper: {
    width: "100%",
    overflow: "hidden",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
  },
  contWrapper: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "stretch",
    flexShrink: 0,
    overflow: "hidden",
    position: "relative",
  },
  rightArrow: {
    position: "absolute",
    top: "50%",
    width: "48px",
    height: "48px",
    borderRadius: "50%",
    backgroundColor: "black",
    color: "white",
    right: 0,
    "&:hover": {
      backgroundColor: "white",
      color: "black",
    },
  },

  leftArrow: {
    position: "absolute",
    top: "50%",
    width: "48px",
    height: "48px",
    borderRadius: "50%",
    left: 0,
    backgroundColor: "black",
    color: "white",
    "&:hover": {
      backgroundColor: "white",
      color: "black",
    },
  },
});

const CardSlider = ({ type, path }) => {
  const styling = useStyles();
  const [currentSlide, setCurrent] = useState(0);
  const [length, setLength] = useState(0);
  const data = useSelector((state) => state.data);

  useEffect(() => {
    if (type.length) {
      setLength(type.length - 2);
    }
    setCurrent(0);
  }, [type]);

  const next = () => {
    if (currentSlide < length - 1) {
      setCurrent((state) => state + 1);
    }
  };
  const prev = () => {
    if (currentSlide > 0) {
      setCurrent((state) => state - 1);
    }
  };

  const compSelect = (path, dat) => {
    switch (path) {
      case "detailPage":
        <MediaCard stream={dat} />;
        break;

      case "livestream":
        <Carding path="liveStream" stream={dat} />;
        break;

      default:
        <Carding stream={dat} />;
        break;
    }
  };

  return (
    <div className={styling.Wrapper}>
      <div className={styling.contWrapper}>
        {type.map((dat, index) => {
          return (
            <div
              key={index}
              style={{ transform: `translateX(-${currentSlide * 100}%)` }}
            >
              {path == "detailPage" ? (
                <MediaCard stream={dat} />
              ) : path == "liveStream" ? (
                <LiveStreamCard stream={dat} />
              ) : (
                <Carding stream={dat} />
              )}
            </div>
          );
        })}

        {currentSlide > 0 && (
          <IconButton onClick={prev} className={styling.leftArrow}>
            <ArrowLeftIcon />
          </IconButton>
        )}
        {currentSlide < length - 1 && (
          <IconButton onClick={next} className={styling.rightArrow}>
            <ArrowRightIcon />
          </IconButton>
        )}
      </div>
    </div>
  );
};

export default CardSlider;

import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";
import IconButton from "@material-ui/core/IconButton";
import {
  Button,
  Card,
  CardContent,
  CardMedia,
  Hidden,
  makeStyles,
  Typography,
} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  contWrapper: {
    width: "100%",
    height: "calc(200px + (500 - 200) * ((100vw - 350px) / (2000 - 350)))",
    display: "flex",
    justifyContent: "center",
    alignItems: "stretch",
    position: "relative",
    '& .MuiPaper-root':{
      backgroundColor: theme.palette.common.white,
    }
    // border:'2px solid green'
  },
  rightArrow: {
    position: "absolute",
    top: "50%",
    width: "48px",
    height: "48px",
    borderRadius: "50%",
    right: 0,
    backgroundColor: theme.palette.primary.main,
    color: "white",
    "&:hover": {
      backgroundColor: "white",
      color: theme.palette.primary.main,
    },
  },

  leftArrow: {
    position: "absolute",
    top: "50%",
    width: "48px",
    height: "48px",
    borderRadius: "50%",
    left: 0,
    backgroundColor: theme.palette.primary.main,
    color: "white",
    "&:hover": {
      backgroundColor: "white",
      color: theme.palette.primary.main,
    },
  },
  cardWrapper: {
    width: "95%",
    height: "calc(200px + (500 - 200) * ((100vw - 350px) / (2000 - 350)))",
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "stretch",
    flexShrink: 0,
    overflow: "hidden",
    // border:'2px solid red',
    position: "relative",
  },
  card: {
    minWidth: "100%",
    borderRadius: "17px",
    position: "relative",
    cursor: "pointer",
    "&:hover": {
      transform: "scale(0.98)",
    },
  },
  overlay: {
    position: "absolute",
    zIndex: 3,
    top: 0,
    left: 0,
    right: "30%",
    bottom: 0,
    backgroundImage:
      "linear-gradient(90deg, rgba(249,249,249,0.9906337535014006) 0%, rgba(254,254,254,0.788953081232493) 0%, rgba(255,255,255,0) 0%)",
  },
  image: {
    position: "absolute",
    zIndex: 2,
    top: 0,
    left: "50%",
    right: 0,
    bottom: 0,
  },
  cardmedia: {
    width: "100%",
    height: "100%",
    backgroundSize: "100% 100%",
  },
  cont: {
    width: "70%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    alignItems: "flex-start",
    textAlign: "left",
    padding: "calc(35px + (50 - 35) * ((100vw - 350px) / (2000 - 350)))",
    color: theme.palette.text.secondary,
    [theme.breakpoints.down("sm")]: {
      width: "80%",
      padding: "20px",
    },
  },
  title: {
    color: theme.palette.common.black,
  },
  desc: {
    marginTop: "calc(10px + (30 - 10) * ((100vw - 350px) / (2000 - 350)))",
    maxHeight: "80px",
    textOverflow: "ellipsis",
    overflow: "hidden",
  },
  play: {
    marginTop: "calc(10px + (30 - 10) * ((100vw - 350px) / (2000 - 350)))",
  },
}));

const CarousalWrap = () => {
  const classes = useStyles();
  const history = useHistory();
  const data = useSelector((state) => state.data.streams);
  const [currentSlide, setCurrent] = useState(0);
  const [length, setLength] = useState(data.length);
  const [arrow, setArrow] = useState(false);

  useEffect(() => {
    if (data) {
      setLength(data.length);
    }
  }, [data]);

  const onClickNavigation = (id) => {
    history.push(`/detail/${id}`);
  };

  const next = () => {
    if (currentSlide < length - 1) {
      setCurrent((state) => state + 1);
    }
  };
  const prev = () => {
    if (currentSlide > 0) {
      setCurrent((state) => state - 1);
    }
  };

  return (
    <div className={classes.contWrapper}>
      <div
        className={classes.cardWrapper}
        onMouseEnter={() => setArrow(true)}
        onMouseLeave={() => setArrow(false)}
      >
        {data.map((dat) => {
          return (
            <Card
              className={classes.card}
              style={{ transform: `translateX(-${currentSlide * 100}%)` }}
              onClick={() => onClickNavigation(dat.id)}
            >
              <div className={classes.overlay}>
                <CardContent className={classes.cont}>
                  <Hidden mdUp>
                    <Typography variant="h6" className={classes.title}>
                      {dat.title}
                    </Typography>
                  </Hidden>
                  <Hidden smDown>
                    <Typography variant="h4" className={classes.title}>
                      {dat.title}
                    </Typography>
                    <Typography variant="subtitle1" className={classes.desc}>
                      {dat.description}
                    </Typography>
                  </Hidden>
                  <Button
                    onClick={() => onClickNavigation(dat.id)}
                    color="primary"
                    variant="contained"
                    className={classes.play}
                  >
                    Play Now
                  </Button>
                </CardContent>
              </div>
              <div className={classes.image}>
                <CardMedia
                  className={classes.cardmedia}
                  image={dat.image}
                  title={dat.title}
                />
              </div>
            </Card>
          );
        })}
        {currentSlide > 0 && arrow && (
          <IconButton onClick={prev} className={classes.leftArrow}>
            <ArrowLeftIcon />
          </IconButton>
        )}
        {currentSlide < length - 1 && arrow && (
          <IconButton onClick={next} className={classes.rightArrow}>
            <ArrowRightIcon />
          </IconButton>
        )}
      </div>
    </div>
  );
};

export default CarousalWrap;

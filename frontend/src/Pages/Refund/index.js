import React from "react";
import { makeStyles, Typography } from "@material-ui/core";


const useStyles = makeStyles((theme)=>({
  root:{
    width:'100%',
    minHeight:'100vh',
    padding:'calc(30px + (70 - 30) * ((100vw - 350px) / (2000 - 350)))',
    paddingTop:'calc(10px + (30 - 10) * ((100vw - 350px) / (2000 - 350)))',

  },
  heading:{
    textTransform: 'uppercase',
    fontWeight:'bold',
    marginTop: 'calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))',
    
  },
  para:{
    // textIndent:'calc(30px + (70 - 30) * ((100vw - 350px) / (2000 - 350)))',
    textAlign:'justify',
    textJustify:"inter-word",
    marginTop: 'calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))',
  }
}))


const Refund = () => {
  const classes = useStyles()

  return (
   <div className={classes.root}>
       <Typography className={classes.para} variant='subtitle1'>
        We&#8217;re so convinced you&#8217;ll absolutely love our services, If
        you are not satisfied with the service for any reason you can get a
        refund within 7 days of making a purchase. Please keep in mind that even
        though we offer a full money back guarantee, we will issue a refund only
        for the unused portion of the service.
       </Typography>
       <Typography className={classes.para} variant='subtitle1'>
        <Typography className={classes.heading} variant='h6'>Contacting us </Typography>
        <br />
        If you would like to contact us concerning any matter relating to this
        Refund Policy, you may send an email to{" "}
        <a href="mailto:hello@businessbeings.com">hello@businessbeings.com</a>
       </Typography>
       <Typography className={classes.para} variant='subtitle1'>This document was last updated on January 6, 2021 </Typography>
    </div>
  );
};

export default Refund;

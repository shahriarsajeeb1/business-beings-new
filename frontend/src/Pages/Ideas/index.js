import { makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import Filter from '../../Components/Filter'
import { Helmet } from "react-helmet";


const useStyles = makeStyles((themes)=>({
    root:{
        width:'90%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
       
        flexDirection: 'column',
        margin:"calc(10px + (60 - 10) * ((100vw - 350px) / (2000 - 350)))",
        
    },
    title1:{
        textAlign:'center',
    },
    title:{       
        marginBottom:"calc(10px + (60 - 10) * ((100vw - 350px) / (2000 - 350)))",
        // marginTop:"calc(10px + (60 - 10) * ((100vw - 350px) / (2000 - 350)))",
    },
   
}))

const Ideas = () => {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <Helmet>
        <title>Explore Business Ideas Based on Your Budget and Interest</title>
        <meta name='description' content='Explore business ideas based on your budget and interests in your native language'/>
        <meta name='keywords' content='business ideas, business ideas in tamil, small business ideas in tamil, online business ideas in tamil, How to start a business, business ideas list'/>
      </Helmet>
            <Typography variant='h4'className={classes.title1}>Explore and watch business ideas based on your field of interest and budget </Typography>
            <Typography variant='h6' className={classes.title}>Get the high quality streaming of business ideas in your native language</Typography>
            <Filter type='Ideas'/>
        </div>
    )
}

export default Ideas

import { makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import ContactForm from '../ContactForm'

const useStyles = makeStyles((theme)=>({
    root:{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexWrap: 'wrap',
        width: '100%',
        height: '100%',
        padding: 'calc(45px + (70 - 45) * ((100vw - 350px) / (2000 - 350)))',
        [theme.breakpoints.down('sm')]:{
            flexDirection:'column',
            padding: 'calc(45px + (70 - 45) * ((100vw - 350px) / (2000 - 350))) 0 calc(45px + (70 - 45) * ((100vw - 350px) / (2000 - 350))) 0 ',
        }
    },
    cont:{
        width: '40%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection:'column',
        [theme.breakpoints.down('sm')]:{
            width: '100%'
        }
    },
    contactForm:{
        width: '60%',
        height:'70%',
        [theme.breakpoints.down('sm')]:{
            width: '100%'
        }
    }
}))

const Services = () => {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <div className={classes.cont}>
                <Typography variant='h4' color='textPrimary'>Business Consultation</Typography>
                <Typography variant='h4' color='textPrimary'>@ Rs. 300*</Typography>
                <Typography variant='subtitle1' color='textPrimary'>Gpay: 9916791744</Typography>
                <Typography variant='subtitle1' color='textPrimary'>Duration: 30 to 45 mins</Typography>          
            </div>
            <div className={classes.contactForm}>
            <ContactForm/>
            </div>
        </div>
    )
}

export default Services

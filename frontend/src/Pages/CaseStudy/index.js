import { makeStyles, Typography } from '@material-ui/core'
import React from 'react'
import Filter from '../../Components/Filter'
import { Helmet } from "react-helmet";

const useStyles = makeStyles((themes)=>({
    root:{
        width:'100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',       
        flexDirection: 'column',
        margin:"calc(10px + (60 - 10) * ((100vw - 350px) / (2000 - 350)))",
        
    },
    title1:{
        textAlign:'center',
    },
    title:{       
        marginBottom:"calc(5px + (40 - 5) * ((100vw - 350px) / (2000 - 350)))",
        // marginTop:"calc(5px + (40 - 5) * ((100vw - 350px) / (2000 - 350)))",
    },
   
}))

const CaseStudy = () => {
    const classes = useStyles()

    return (
        <div className={classes.root}>
            <Helmet>
        <title>Most Popular Business Case Studies</title>
        <meta name='description' content='Find how other companies or businesses tackle the tough situations with right skills and grown'/>
        <meta name='keywords' content='business case studies, business case study examples, business case study examples with solutions'/>
      </Helmet>
            <Typography variant='h4' className={classes.title1}>Get an idea about real-life situations faced by companies and how they tackle it with right skills</Typography>
            <Typography variant='h6' className={classes.title}>Watch and apply the methods in your new endeavours</Typography>
            <Filter type='Case'/>
        </div>
    )
}

export default CaseStudy

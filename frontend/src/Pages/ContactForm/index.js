import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import {
  Card,
  Button,
  InputBase,
  makeStyles,
  Typography,
  FormControl,
  TextareaAutosize,
} from "@material-ui/core";
import NativeSelect from "@material-ui/core/NativeSelect";
import {
  sendContactForm,
  sendServiceForm,
  sendServiceReturnInitialState,
  sendContactReturnInitialState,
} from "../../services/Userdata/action";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "stretch",
    padding: "calc(30px + (70 - 50) * ((100vw - 350px) / (2000 - 350)))",
    margin: "calc(30px + (70 - 50) * ((100vw - 350px) / (2000 - 350)))",
    backgroundColor: theme.palette.background.default,
  },
  heading: {
    width: "100%",
    textAlign: "center",
    marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
    fontWeight: "bold",
  },
  inputWrapper: {
    width: "100%",
    marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
    "& .MuiFormControl-root": {
      width: "100%",
      "& .MuiInput-underline:before": {
        border: "0px solid white",
      },
      "& .MuiInput-underline:after": {
        border: "0px solid white",
      },
      "& .MuiInput-underline:hover:not(.Mui-disabled):before": {
        border: "0px solid white",
      },
      "& .MuiNativeSelect-select:focus": {
        backgroundColor: theme.palette.common.white,
      },
    },
  },
  input: {
    width: "100%",
    backgroundColor: theme.palette.common.white,
    height: "calc(30px + (70 - 30) * ((100vw - 350px) / (2000 - 350)))",
    marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
    padding: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  textArea: {
    maxWidth: "100%",
    maxHeight: "500px",
    minHeight: "100px",
    minWidth: "100%",
    outline: 0,
    border: 0,
    padding: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  defaultOption: {
    color: theme.palette.text.secondary,
  },
}));

const ContactForm = () => {
  const classes = useStyles();
  const location = useLocation();
  const dispatch = useDispatch();
  const errorContact = useSelector((state) => state.user.isErrorContact);
  const successContact = useSelector((state) => state.user.isSuccessContact);
  const errorService = useSelector((state) => state.user.isErrorService);
  const successService = useSelector((state) => state.user.isSuccessService);
  const [contactData, setContactData] = useState({
    name: "",
    mobile: "",
    email: "",
    city: "",
    service: "",
    description: "",
  });

  const onChangeFields = (event) => {
    setContactData((prevData) => ({
      ...prevData,
      [event.target.name]: event.target.value,
    }));
  };

  useEffect(() => {
    if (errorContact || successContact || errorService || successService) {
      setContactData((prevData) => ({
        ...prevData,
        name: "",
        mobile: "",
        email: "",
        city: "",
        service: "",
        description: "",
      }));
      // dispatch(sendContactReturnInitialState());
      // dispatch(sendServiceReturnInitialState());
    }
  }, [errorContact, successContact, errorService, successService]);

  const handleSubmitButton = () => {
    if (location.pathname == "/contact") {
      dispatch(sendContactForm(contactData));
    }
    if (location.pathname == "/services") {
      dispatch(sendServiceForm(contactData));
    }
  };

  return (
    <Card className={classes.root}>
      <Typography className={classes.heading} variant="h4">
        {location.pathname == "/contact"
          ? "Contact Form"
          : "Business Registration Services"}
      </Typography>
      {errorContact && (
        <Typography variant="subtitle1" color="primary">
          Something went wrong.Please Try after sometime.
        </Typography>
      )}
      {successContact && (
        <Typography variant="subtitle1" color="primary">
          Quary is Submitted.We will get back to you soon.
        </Typography>
      )}
      {errorService && (
        <Typography variant="subtitle1" color="primary">
          Something went wrong.Please Try after sometime.
        </Typography>
      )}
      {successService && (
        <Typography variant="subtitle1" color="primary">
          Quary is Submitted.We will get back to you soon.
        </Typography>
      )}
      <div className={classes.inputWrapper}>
        <FormControl>
          <InputBase
            onChange={onChangeFields}
            name="name"
            placeholder="Full Name..."
            className={classes.input}
            value={contactData.name}
          />
        </FormControl>
      </div>
      <div className={classes.inputWrapper}>
        <FormControl className={classes.margin}>
          <InputBase
            onChange={onChangeFields}
            name="mobile"
            placeholder="Phone number"
            className={classes.input}
            value={contactData.mobile}
          />
        </FormControl>
      </div>
      <div className={classes.inputWrapper}>
        <FormControl className={classes.margin}>
          <InputBase
            onChange={onChangeFields}
            name="email"
            placeholder="Email"
            className={classes.input}
            value={contactData.email}
          />
        </FormControl>
      </div>
      <div className={classes.inputWrapper}>
        <FormControl className={classes.margin}>
          <InputBase
            onChange={onChangeFields}
            name="city"
            placeholder="City"
            className={classes.input}
            value={contactData.city}
          />
        </FormControl>
      </div>
      {location.pathname == "/services" && (
        <div className={classes.inputWrapper}>
          <FormControl className={classes.margin}>
            <NativeSelect
              id="demo-customized-select-native"
              value={contactData.service}
              onChange={onChangeFields}
              name="service"
              placeholder="Service in need"
              className={classes.input}
            >
              <option className={classes.defaultOption} value="">
                Services
              </option>
              <option value="Company registration">Company registration</option>
              <option value="GST registration">GST registration</option>
              <option value="GST filing">GST filing</option>
              <option value="Trademark registrations">
                Trademark registrations
              </option>
              <option value="FSSAI registration">FSSAI registration</option>
              <option value="FSSAI license">FSSAI license</option>
            </NativeSelect>
          </FormControl>
        </div>
      )}
      {location.pathname == "/contact" && (
        <div className={classes.inputWrapper}>
          <TextareaAutosize
            maxRows={4}
            aria-label="maximum height"
            placeholder="Write your quaries"
            value={contactData.description}
            onChange={onChangeFields}
            className={classes.textArea}
            name="description"
          />
        </div>
      )}

      <Button
        style={{
          margin: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
        }}
        variant="contained"
        color="primary"
        onClick={handleSubmitButton}
      >
        Get started now
      </Button>
    </Card>
  );
};

export default ContactForm;

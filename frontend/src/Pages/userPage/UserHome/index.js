import React, { useEffect, useState } from "react";
import dayjs from "dayjs";
import { useSelector } from "react-redux";
import {
  makeStyles,
  Tabs,
  Tab,
  Typography,
  Hidden,
  Button,
} from "@material-ui/core";
import UserLayout from "../../../Components/UserLayout";
import Carding from "../../../Components/Card";
import CheckIcon from "@material-ui/icons/Check";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    alignItems: "stretch",
    justifyContent: "stretch",
  },
  userProfile: {
    width: "calc(300px + (450 - 300) * ((100vw - 350px) / (2000 - 350)))",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "center",
    backgroundColor: "#D1CFCF",
  },
  dp: {
    position: "relative",
    width: "calc(300px + (450 - 300) * ((100vw - 350px) / (2000 - 350)))",
    // width: 'calc(250px + (300 - 250) * ((100vw - 350px) / (2000 - 350)))',
    height: "calc(350px + (500 - 350) * ((100vw - 350px) / (2000 - 350)))",
    // marginBottom: "calc(20px + (70 - 20) * ((100vw - 350px) / (2000 - 350)))",
  },
  dpImage: {
    width: "100%",
    height: "100%",
  },
  dpName: {
    position: "absolute",
    bottom: "calc(30px + (45 - 30) * ((100vw - 350px) / (2000 - 350)))",
    left: 0,
    right: 0,
    zIndex: 2,
    textAlign:'center',
  },
  profileCont: {
    width: "calc(300px + (450 - 300) * ((100vw - 350px) / (2000 - 350)))",
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    alignItems: "flex-start",
  },
  contTitle: {
    margin: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  viewedWrapper: {
    width: "calc(300px + (450 - 300) * ((100vw - 350px) / (2000 - 350)))",
    display: "flex",
    justifyContent: "center",
    alignItems: "stretch",
    // padding:'calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))',
    textAlign: "center",
  },
  viewed: {
    flex: 1,
  },
  features: {
    width: "calc(300px + (450 - 300) * ((100vw - 350px) / (2000 - 350)))",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    // padding:'calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))',
    textAlign: "left",
  },

  userWrapper: {
    flex: 1,
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    flexDirection: "column",
    marginTop: "calc(10px + (50 - 10) * ((100vw - 350px) / (2000 - 350)))",
    marginLeft: "calc(20px + (70 - 20) * ((100vw - 350px) / (2000 - 350)))",
  },
  tabs: {
    marginBottom: "calc(30px + (45 - 30) * ((100vw - 350px) / (2000 - 350)))",
  },
  videoWrapper: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "flex-start",
    flexWrap: "wrap",
  },
}));

function UserHome() {
  const classes = useStyles();
  const data = useSelector((state) => state.data.streams);
  const user = useSelector((state) => state.user.user);
  const plan = useSelector((state) => state.plan.plans);
  const [tab, setTab] = useState(0);
  

  const hangleTab = (event, value) => {
    setTab(value);
  };

  const free = ["Free Account Login", "New video stream surfing"];
  const pre = ["Explore Business Streams", "HD Video Quality"];
  const vip = [
    "Explore Business Streams",
    "Premium Business Streams",
    "Explore Case studies",
    "Watch Expert Interviews",
    "All Live sessions (Trainings, Consultations, Events)",
    "Adfree Streams",
    "HD Video Quality",
  ];

  const features = () => {
    switch (user.plan) {
      case "free":
        return free.map((feature) => (
          <Typography variant="subtitle1">
            <CheckIcon style={{ color: "primary", fontSize: 14 }} /> {feature}
          </Typography>
        ));
      case "PRE":
        return pre.map((feature) => (
          <Typography variant="subtitle1">
            <CheckIcon style={{ color: "primary", fontSize: 14 }} /> {feature}
          </Typography>
        ));
      case "VIP":
        return vip.map((feature) => (
          <Typography variant="subtitle1">
            <CheckIcon style={{ color: "primary", fontSize: 14 }} /> {feature}
          </Typography>
        ));
    }
  };

  return (
    <UserLayout>
      <div className={classes.root}>
        <Hidden smDown>
          <div className={classes.userProfile}>
            <div className={classes.dp}>
              <img className={classes.dpImage} src={user.image} />
              <div className={classes.dpName}>
                <Typography variant="h6">{user.name}</Typography>
              </div>
            </div>
            <div className={classes.profileCont}>
              <div>
                <Typography variant="h6" className={classes.contTitle}>
                  Watched
                </Typography>
                <div className={classes.viewedWrapper}>
                  <div className={classes.viewed}>
                    <Typography variant="h6">
                      {user.bookMarked ? user.bookMarked.length : 0}
                    </Typography>
                    <Typography variant="subtitle1">Videos</Typography>
                  </div>
                  <div className={classes.viewed}>
                    <Typography variant="h6">0</Typography>
                    <Typography variant="subtitle1">Live Streams</Typography>
                  </div>
                </div>
              </div>
              <div>
                <Typography variant="h6" className={classes.contTitle}>
                  Subscription
                </Typography>
                <div className={classes.viewedWrapper}>
                  <div>
                    <Typography
                      color="primary"
                      variant="h4"
                      style={{ textTransform: "uppercase" }}
                    >
                      {user.plan}
                    </Typography>
                    <Typography variant="subtitle1">
                      for {user.duration}{" "}
                      {user.duration == 1 ? "month" : "months"}
                    </Typography>
                  </div>
                </div>
              </div>
              <div>
                <Typography variant="h6" className={classes.contTitle}>
                  Your Plan Includes:
                </Typography>
                <div className={classes.features}>
                  <div>{features()}</div>
                </div>
              </div>
            </div>
            <Button
              style={{
                marginTop:
                  "calc(30px + (100 - 30) * ((100vw - 350px) / (2000 - 350)))",
                marginBottom:
                  "calc(20px + (90 - 20) * ((100vw - 350px) / (2000 - 350)))",
              }}
              variant="contained"
              color="primary"
            >
              {" "}
              Upgrade
            </Button>
          </div>
        </Hidden>
        <div className={classes.userWrapper}>
          <Tabs
            className={classes.tabs}
            value={tab}
            indicatorColor="primary"
            textColor="secondary"
            onChange={hangleTab}
          >
            <Tab label="Continue Watching" />
            <Tab label="Popular" />
            <Tab label="Recent Videos" />
          </Tabs>
          {tab == 0 && (
            <div className={classes.videoWrapper}>
              {data
                .filter((dat) => user.bookMarked.includes(dat.id))
                .map((dat) => {
                  return (
                    <div>
                      <Carding stream={dat} />
                    </div>
                  );
                })}
            </div>
          )}
          {tab == 1 && (
            <div className={classes.videoWrapper}>
              {data
                .sort((a, b) => Number(b.likes) - Number(a.likes))
                .map((dat) => {
                  return (
                    <div>
                      <Carding stream={dat} />
                    </div>
                  );
                })}
            </div>
          )}
          {tab == 2 && (
            <div className={classes.videoWrapper}>
              {data
                .sort((a, b) => new dayjs(b.uploaded) - new dayjs(a.uploaded))
                .map((dat) => {
                  return (
                    <div>
                      <Carding stream={dat} />
                    </div>
                  );
                })}
            </div>
          )}
        </div>
      </div>
    </UserLayout>
  );
}

export default UserHome;

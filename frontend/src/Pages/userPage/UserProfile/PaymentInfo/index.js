import React,{ useState } from "react";
import { useSelector } from "react-redux";

import { Button, Divider, makeStyles, Typography } from "@material-ui/core";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import CheckIcon from '@material-ui/icons/Check';
import AlertDialogInfo from "./AlertDialog";
import UpgradeModal from "./UpgradeModal";
import Invitation from "../../../Signup/InviteFriend";

const useStyles = makeStyles((theme) => ({
  planWrapper: {
      width:'calc(300px + (900 - 300) * ((100vw - 350px) / (2000 - 350)))',
    display: "flex",
    flexDirection: "row",
    alignItems:'stretch',
    justifyContent:'stretch',

  },
  plan:{
    margin:'calc(15px + (45 - 15) * ((100vw - 350px) / (2000 - 350)))',
    display: "flex",
    flexDirection: "column",
    alignItems:'flex-start',
    justifyContent:'space-around',
    // textTransform:'uppercase',
  },
  feature:{
    margin:'calc(15px + (45 - 15) * ((100vw - 350px) / (2000 - 350)))',
    display: "flex",
    flexDirection: "column",
    alignItems:'flex-start',
    justifyContent:'space-around',
  },
  divider:{
    margin:'calc(15px + (45 - 15) * ((100vw - 350px) / (2000 - 350))) 0 calc(15px + (45 - 15) * ((100vw - 350px) / (2000 - 350))) 0'
  }
}));

function PaymentInfo() {
  const classes = useStyles();
  const user = useSelector((state) => state.user.user);
  const plans = useSelector((state) => state.plan.plans);
  const payment = useSelector(state => state.payment)
  const [modalOpen, setModalOpen] = useState(false)
  const userPlan = plans.find(plan => plan.name == user.planName);

  const handleModal = (view) => {
    setModalOpen(view);
  }
  

  // const features = () => {
  //   switch (user.plan) {
  //     case "free":
  //       return plan.free.map((feature) => (
            
  //         <Typography variant="body1"><CheckIcon/> {feature}</Typography>
  //       ));
  //     case "premium":
  //       return plan.premium.map((feature) => (
  //         <Typography variant="body1"><CheckIcon/> {feature}</Typography>
  //       ));
  //     case "vip":
  //       return plan.vip.map((feature) => (
  //         <Typography variant="body1">{feature}</Typography>
  //       ));
  //   }
  // };

  return (
    <div>
      <div>
        <Typography variant="h6">Current Plan</Typography>
        <div className={classes.planWrapper}>
          <div className={classes.plan}>
            <Typography variant="h4" color='primary'>{user.planType}<span><Typography style={{display:'inline-block',marginLeft:'10px'}} variant="subtitle2"> plan</Typography></span></Typography>
           {user.planType == 'free'?null: <Typography variant="subtitle1">for<span><Typography style={{display:'inline-block',marginLeft:'10px'}} variant="h4" color='primary'> {userPlan.duration}</Typography></span> months</Typography>}
          </div>
          {/* <div className={classes.feature}>
            <Typography variant="h6">Plan Features</Typography>
            <div>{features()}</div>
          </div> */}
        </div>
        {/* {user.plan === "vip" ? ( */}
          <Button variant="contained" color="primary" onClick={()=>handleModal(true)}>
           
            Upgrade
          </Button>
         
      </div>
      <Divider className={classes.divider} />
      <div>
        <Typography variant="h6"> Refer a Friend</Typography>

        <Typography variant="h6"> </Typography>
        <Invitation path='profile'/>        
      </div>
      <Divider className={classes.divider} />
      <div>
        <Typography className={classes.divider} varient="h6"> Delete Account</Typography>

        <Typography varient="h6"> </Typography>
        <Button variant="contained" color="primary">        
          Delete My Account
        </Button>
      </div>
      <Divider className={classes.divider}/>
      <Backdrop  className={classes.backdrop} open={payment.isLoadingCreate || payment.isLoadingVerify} >
        <CircularProgress color="inherit" onBackdropClick="false"/>
      </Backdrop>
      {modalOpen && <div> <UpgradeModal view={true} handleModal={handleModal}/></div>}
      {payment.isSuccessUpdate && <AlertDialogInfo view={true} dialogContent={'Transaction Successful.Account is Upgraded'}/>}
      {payment.isErrorUpdate && <AlertDialogInfo view={true} dialogContent={'Error Occured.Contact Admin.'} />}
    </div>
  );
}

export default PaymentInfo;

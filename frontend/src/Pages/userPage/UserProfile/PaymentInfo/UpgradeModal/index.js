import React,{ useState } from "react";
import { Typography,Modal,makeStyles, Card } from "@material-ui/core";
import PlanCard from "../../../../Signup/ChoosePlan/PlanCard";
import { useSelector } from "react-redux";

const useStyles = makeStyles((theme)=>({
  modal: {
    
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    zIndex:2,
  },
  modalContent:{
    backgroundColor:theme.palette.background.default,
  }
}))

const UpgradeModal = (props) => {
  const classes = useStyles();
  const plans = useSelector((state) => state.plan.plans);
  const [modalOpen, setModalOpen] = useState(props.view)

  const onClickModalClose = ()=>{
    setModalOpen(false);
    props.handleModal(false)
  }


  return (
    <div>
      <Modal
      disableEnforceFocus
        open={modalOpen}
        onClose={onClickModalClose}
        className={classes.modal}
        
      >
        <Card>
          <div
            style={{
              margin:
                "calc(15px + (30 - 15) * ((100vw - 350px) / (2000 - 350)))",
            }}
          >
            <Typography variant="h4" gutterBottom>
              Premium Plans
            </Typography>
            <div style={{ display: "flex" }}>
              {plans
                .filter((plan) => plan.type === "PRE")
                .map((planItem) => (
                  <PlanCard plan={planItem} />
                ))}
            </div>
          </div>
          <div
            style={{
              margin:
                "calc(15px + (30 - 15) * ((100vw - 350px) / (2000 - 350)))",
            }}
          >
            <Typography variant="h4" gutterBottom>
              VIP Plans
            </Typography>
            <div style={{ display: "flex" }}>
              {plans
                .filter((plan) => plan.type === "VIP")
                .map((planItem) => (
                  <PlanCard plan={planItem} />
                ))}
            </div>
          </div>
        </Card>
      </Modal>
    </div>
  );
};

export default UpgradeModal;

import React,{ useState } from 'react'
import AccountInfo from './AccountInfo';
import PaymentInfo from './PaymentInfo';

import { makeStyles,Tabs,Tab } from '@material-ui/core'
import UserLayout from '../../../Components/UserLayout';

const useStyles = makeStyles((theme)=>({
    contWrapper:{
        margin: "calc(20px + (70 - 20) * ((100vw - 350px) / (2000 - 350)))",
        marginTop: "calc(10px + (50 - 10) * ((100vw - 350px) / (2000 - 350)))",
    },
    tab:{
        marginBottom: "calc(10px + (50 - 10) * ((100vw - 350px) / (2000 - 350)))",
    }
     
}))

function UserProfile() {
    
    const classes = useStyles();
    
    const [tab, setTab] = useState(0);

    const hangleTab = (event, value) => {
        setTab(value)
    }

    
    
    return (
<UserLayout>

        <div className={classes.contWrapper}>
                <Tabs
                    value={tab}
                    indicatorColor="primary"
                    textColor="secondary"
                    onChange={hangleTab}
                    className={classes.tab}
                >
                    <Tab label="Account Settings" />
                    <Tab label="Payment Information" />                    
                </Tabs>
                {tab == 0 && (
                    <AccountInfo/>
                )}
                {tab == 1 && (
                    <PaymentInfo/>
                )}
                


            </div>
</UserLayout>
    )
}

export default UserProfile

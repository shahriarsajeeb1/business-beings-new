import React,{ useState,useEffect } from 'react'
import { useSelector,useDispatch } from 'react-redux'
import { Button, InputBase, makeStyles, Typography,FormControl } from '@material-ui/core'
import { updatedUser } from '../../../../services/Userdata/action'
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import storage from "../../../../firebase"
import { ref,uploadBytesResumable,getDownloadURL } from "firebase/storage";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
        justifyContent: 'stretch',
        alignItems: 'stretch',

    },
    userProfile: {
        width: 'calc(300px + (450 - 300) * ((100vw - 350px) / (2000 - 350)))',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
    },
    dp: {
        width: 'calc(250px + (300 - 250) * ((100vw - 350px) / (2000 - 350)))',
        height: 'calc(300px + (400 - 300) * ((100vw - 350px) / (2000 - 350)))',
        marginBottom: "calc(20px + (70 - 20) * ((100vw - 350px) / (2000 - 350)))",
    },
    buttonWrapper:{
        width: 'calc(250px + (300 - 250) * ((100vw - 350px) / (2000 - 350)))',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputWrapper:{
        width: 'calc(200px + (700 - 200) * ((100vw - 350px) / (2000 - 350)))',
        marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",  
    
    },
    text:{
        marginBottom: "calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))",  
    },
    subText:{
        marginBottom: "calc(3px + (10 - 3) * ((100vw - 350px) / (2000 - 350)))",  
    },
    input: {
        borderRadius: 4,
        position: 'relative',
        backgroundColor: theme.palette.common.white,
        border: '1px solid #ced4da',
        fontSize: 16,
       width:'100%',
       height: 'calc(30px + (50 - 30) * ((100vw - 350px) / (2000 - 350)))',
        padding: '10px 12px',
        color:theme.palette.text.secondary,
        transition: theme.transitions.create(['border-color', 'box-shadow']),
        '&:focus': {
            // boxShadow: `${alpha(theme.palette.primary.main, 0.25)} 0 0 0 0.2rem`,
            borderColor: theme.palette.primary.main,
            color:'black',
        },
    },


}))

const AccountInfo = ()=> {

    const classes = useStyles();
    const users = useSelector(state => state.user); 
    const user = useSelector(state => state.user.user); 
    const dispatch = useDispatch()
    const [updatedData, setUpdatedData] = useState(
        {
            name:user.name,
            mobile:user.mobile,
            pincode:user.pincode,
            password:'',

        }
    )

    const [image, setImage] = useState(null);
    const [imageurl, setImageurl] = useState('');
    const [imageLoading, setImageLoading] = useState(false);

    useEffect(() => {
        if(imageurl){
            dispatch(updatedUser({
                image:imageurl,
                password:'',
            }))
        }
        
    }, [imageurl])


    // const onChangeImage = (event) => {        
    //     setImage(event.target.files[0]);     
    // }

    const onChangeFields = (event) => {        
        setUpdatedData(prevData => ({
            ...prevData,
           [event.target.name] :event.target.value,
        }))          
    }

    const onChangeImage = (e) =>{
        setImage(e.target.files[0]);
        console.log(image)
        if(image){
            setImageLoading(true)
            console.log("tttttt",image)
            // e.preventDefault();
        const imageRef = ref(storage,`/images/${image.name}`);
        const uploadTask = uploadBytesResumable(imageRef,image);   
        uploadTask.on("state_changed", 
        (snapshot) => {
            // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
            const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
            console.log('Upload is ' + progress + '% done');
            switch (snapshot.state) {
              case 'paused':
                console.log('Upload is paused');
                break;
              case 'running':
                console.log('Upload is running');
                break;
            }
          }, 
          (error) => {        
            switch (error.code) {
              case 'storage/unauthorized':
                // User doesn't have permission to access the object
                break;
              case 'storage/canceled':
                // User canceled the upload
                break;
        
              // ...
        
              case 'storage/unknown':
                // Unknown error occurred, inspect error.serverResponse
                break;
            }
          }, 
          () => {
            getDownloadURL(uploadTask.snapshot.ref)
            .then((url) => {
              setImageurl(url)
              setImageLoading(false)
            });
        });
           }
    }

   const handleSaveButton = () => {
       dispatch(updatedUser(updatedData));
   }
   


    return (
        <div className={classes.root}>
            <div className={classes.userProfile}>
                <img src={user.image} className={classes.dp} />
                <div className={classes.buttonWrapper}>                
                <label htmlFor='dp' className='MuiButton-root MuiButton-containedPrimary' >
                Change Picture
                </label>
                <InputBase id='dp' onChange={(e)=>onChangeImage(e)} name='image' type='file' style={{display: 'none'}}/> 
                </div>
            </div>
            <div>
                <Typography className={classes.text} varient='h6'>Account Information</Typography>
                <Typography className={classes.text} varient='h6'></Typography>
                <div className={classes.inputWrapper}>
                <FormControl >
                    <Typography varient='subtitle1' color='textSecondary' className={classes.subText}>                    
                        Name
                    
                    </Typography>
                    <InputBase onChange={onChangeFields} name="name" defaultValue={user.name} className={classes.input} />
                </FormControl>
                </div>
                <div className={classes.inputWrapper}>
                <FormControl className={classes.margin}>
                    <Typography varient='subtitle1' color='textSecondary' className={classes.subText}>
                        Mobile
                    </Typography>
                    <InputBase onChange={onChangeFields} name="mobile" defaultValue={user.mobile} className={classes.input} />
                </FormControl>
                </div>
                <div className={classes.inputWrapper}>
                <FormControl className={classes.margin}>
                    <Typography varient='subtitle1' color='textSecondary' className={classes.subText}>
                        Pincode
                    </Typography>
                    <InputBase onChange={onChangeFields} name="pincode" defaultValue={user.pincode} className={classes.input} />
                </FormControl>
                </div>
                <Typography className={classes.text} varient='h6'>Password Change</Typography>
                <div className={classes.inputWrapper}>
                <FormControl className={classes.margin}>
                    <Typography varient='subtitle1' color='textSecondary' className={classes.subText}>
                        New Password
                    </Typography>
                    <InputBase onChange={onChangeFields} name="password" type='password' className={classes.input} />
                </FormControl>
                </div>              
                <Button style={{margin:"calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))"}} variant='contained' color='primary' onClick={handleSaveButton}> Save Change</Button>
            </div>
            <Backdrop className={classes.backdrop} open={users.isLoadingGet || users.isLoadingUpdate || imageLoading}>
        <CircularProgress color="inherit" />
      </Backdrop>
        </div>
    )
}

export default AccountInfo

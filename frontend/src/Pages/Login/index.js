import firebase from "../../firebase";
import { useFormik } from "formik";
import * as yup from "yup";
import {
  getAuth,
  signInWithPhoneNumber,
  RecaptchaVerifier,
} from "firebase/auth";
import {
  FormControl,
  Input,
  InputAdornment,
  Typography,
  Button,
  makeStyles,
  FormHelperText,
} from "@material-ui/core";
import Backdrop from "@material-ui/core/Backdrop";
import CircularProgress from "@material-ui/core/CircularProgress";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import {
  updateMobileLocally,
  updateModalOpen,
} from "../../services/Styles/action";
import { findUser, loginUser } from "../../services/Authentication/action";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "calc(300px + (700 - 300) * ((100vw - 350px) / (2000 - 350)))",
    height: "auto",
    padding: "calc(30px + (70 - 30) * ((100vw - 350px) / (2000 - 350)))",
    // height: "calc(300px + (500 - 300) * ((100vw - 350px) / (2000 - 350)))",
    backgroundColor:
      theme.palette.type === "light"
        ? theme.palette.common.white
        : theme.palette.common.black,
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-start",
    flexDirection: "column",
    borderRadius: "20px",
  },
  title: {
    margin: "calc(30px + (70 - 30) * ((100vw - 350px) / (2000 - 350)))",
    marginTop: "calc(5px + (15 - 5) * ((100vw - 350px) / (2000 - 350)))",
  },
  input: {
    width: "calc(250px + (550 - 250) * ((100vw - 350px) / (2000 - 350)))",
    margin: "calc(20px + (10 - 20) * ((100vw - 350px) / (2000 - 350)))",
  },
  buttonWrapper: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: "calc(30px + (70 - 30) * ((100vw - 350px) / (2000 - 350)))",
    marginBottom: "calc(10px + (15 - 10) * ((100vw - 350px) / (2000 - 350)))",
  },
  textWrapper: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    textAlign: "center",
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: "#fff",
  },
}));

const validate = yup.object({
  mobile: yup
    .string("Enter your Number")
    .min(10, "Enter 10 digit valid number")
    .max(10, "You have exeeded 10 digits")
    .required("Mobile Number is required"),
  password: yup
    .string("Enter your password")
    .min(8, "Password should be of minimum 8 characters length")
    .required("Password is required"),
});

const Login = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const [mobile, setMobile] = useState("");
  const [password, setPassword] = useState("");
  const [otpCode, setOtpCode] = useState("");
  const [otpSendError, setOtpSendError] = useState(false);
  const [otpCodeError, setOtpCodeError] = useState(false);
  const [otpLoading, setOtpLoading] = useState(false);
  const auth = useSelector((state) => state.auth);
  const user = useSelector((state) => state.user.user);
  const authFire = getAuth();

  const formik = useFormik({
    initialValues: {
      mobile: "",
      password: "",
    },
    validationSchema: validate,
    validateOnMount:true,
    onSubmit: () => {},
  });

  const setUpRecaptcha = () => {
    window.recaptchaVerifier = new RecaptchaVerifier(
      "recaptcha",
      {
        size: "normal",
      },
      authFire
    );
  };

  useEffect(() => {
    if (!auth.isUser && auth.newUser) {
      setUpRecaptcha();
      const recaptcha = window.recaptchaVerifier;
      const number = `+91${formik.values.mobile}`;
      signInWithPhoneNumber(authFire, number, recaptcha)
        .then(function (confirmationResult) {
          window.confirmationResult = confirmationResult;
        })
        .catch(function (error) {
          setOtpSendError(true);
          console.error(error);
        });
    }
    if (user._id) {
      dispatch(updateModalOpen(false));
      history.push("/user");
    }
  }, [auth.isUser, auth.newUser, user._id]);

  // const onChangeMobile = (event) => {
  //   setMobile(event.target.value);
  // };

  const handleMobile = (e) => {
    e.preventDefault();
    dispatch(findUser(formik.values.mobile));
  };

  // const onChangePassword = (event) => {
  //   setPassword(event.target.value);
  // };
  const onChangeOtpCode = (event) => {
    setOtpCode(event.target.value);
  };
  const handleOtp = (e) => {
    e.preventDefault();
    setOtpLoading(true);
    if (otpCode === null) return;
    let optConfirm = window.confirmationResult;
    optConfirm
      .confirm(otpCode)
      .then((result) => {
        // User signed in successfully.
        dispatch(updateMobileLocally(formik.values.mobile));
        history.push("/signup");
      })
      .catch((error) => {
        // User couldn't sign in (bad verification code?)
        setOtpCodeError(true);
      });
  };
  const handlePassword = () => {
    dispatch(loginUser(formik.values.mobile, formik.values.password));
  };

  return (
    <div>
      {!auth.isUser && !auth.newUser && (
        <div className={classes.root}>
          <Typography variant="h4" className={classes.title}>
            Login to continue
          </Typography>
          <div className={classes.input}>
            <FormControl fullWidth>
              <Input
                id="mobile"
                name="mobile"
                value={formik.values.mobile}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                placeholder="Enter your mobile Number"
                startAdornment={
                  <InputAdornment position="start">+ 91 | </InputAdornment>
                }
              />
              {Boolean(formik.errors.mobile) && formik.touched.mobile && (
                <FormHelperText error id="standard-weight-helper-text">
                  {formik.errors.mobile}
                </FormHelperText>
              )}
            </FormControl>
          </div>
          <div className={classes.buttonWrapper}>
            <Button
              disabled={Boolean(formik.errors.mobile)}
              onClick={handleMobile}
              variant="contained"
              color="primary"
            >
              CONTINUE
            </Button>
          </div>
          <div className={classes.textWrapper}>
            <Typography variant="subtitle1">
            By proceeding,you agree to the <span><Link to='terms'>terms  <br/> of use </Link></span> 
              and <span><Link to='/privacy'>privacy policy</Link></span> 
            </Typography>
          </div>
        </div>
      )}

      {auth.isUser && (
        <div className={classes.root}>
          <Typography variant="h4" className={classes.title}>
            Login to continue
          </Typography>
          <div className={classes.input}>
            <FormControl fullWidth>
              <Input
                type="password"
                id="password"
                name="password"
                value={formik.values.password}
                onChange={formik.handleChange}
                onFocus={formik.handleBlur}
                placeholder="Enter your Password"
              />
              {Boolean(formik.errors.password) && formik.touched.password && (
                <FormHelperText error id="standard-weight-helper-text">
                  {formik.errors.password}
                </FormHelperText>
              )}
            </FormControl>
          </div>
          <div className={classes.buttonWrapper}>
            <Link to="/user">
              <Button
                disabled={Boolean(formik.errors.password)}
                onClick={handlePassword}
                variant="contained"
                color="primary"
              >
                LOGIN
              </Button>
            </Link>
          </div>
          <div className={classes.textWrapper}>
            <Typography variant="subtitle1">
              By proceeding,you agree to the <span><Link to='terms'>terms  <br/> of use </Link></span> 
              and <span><Link to='/privacy'>privacy policy</Link></span> 
            </Typography>
          </div>
        </div>
      )}

      {auth.newUser && (
        <div className={classes.root}>
          <Typography variant="h4" className={classes.title}>
            Register to continue
          </Typography>
          <div id="recaptcha"></div>
          <div className={classes.input}>
            <FormControl fullWidth>
              <Input
                value={otpCode}
                required
                onChange={onChangeOtpCode}
                placeholder="Enter your OTP"
              />
              {otpCodeError && (
                <FormHelperText id="standard-weight-helper-text">
                  Please enter correct OTP
                </FormHelperText>
              )}
              {otpSendError && (
                <FormHelperText id="standard-weight-helper-text">
                  OTP cannot able to send
                </FormHelperText>
              )}
            </FormControl>
          </div>
          <div className={classes.buttonWrapper}>
            <Button onClick={handleOtp} variant="contained" color="primary">
              CONTINUE
            </Button>
          </div>
          <div className={classes.textWrapper}>
            <Typography variant="subtitle1">
              By proceeding,you agree to the terms
              <br /> of use and privacy policy
            </Typography>
          </div>
        </div>
      )}
      <Backdrop
        className={classes.backdrop}
        open={auth.isLoading || otpLoading}
      >
        <CircularProgress color="inherit" />
      </Backdrop>
    </div>
  );
};

export default Login;

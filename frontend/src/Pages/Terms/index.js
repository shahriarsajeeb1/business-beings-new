import React from "react";
import { makeStyles, Typography } from "@material-ui/core";
import Header from '../../Components/Header'

const useStyles = makeStyles((theme)=>({
  root:{
    width:'100%',
    padding:'calc(30px + (70 - 30) * ((100vw - 350px) / (2000 - 350)))',
    paddingTop:'calc(10px + (30 - 10) * ((100vw - 350px) / (2000 - 350)))',

  },
  heading:{
    textTransform: 'uppercase',
    fontWeight:'bold',
    marginTop: 'calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))',
    
  },
  para:{
    textIndent:'calc(30px + (70 - 30) * ((100vw - 350px) / (2000 - 350)))',
    textAlign:'justify',
    textJustify:"inter-word",
    marginTop: 'calc(5px + (20 - 5) * ((100vw - 350px) / (2000 - 350)))',
  }
}))

const Terms = ()=> {

  const classes = useStyles()

  return (
    <>
     <div className={classes.root}>
      <Typography  className={classes.para} variant='subtitle1'>
        Welcome to Business Beings, we provide you both free and subscription
        based service that allows our end users to access informational content
        including business ideas and professional courses streamed over the
        internet to certain Devices. These Terms of Use (&#8220;Terms of
        Use&#8221;) govern your use of our service. This document is an
        electronic record in terms of Information Technology Act, 2000 and rules
        thereunder pertaining to electronic records in various statutes as
        amended by the Information Technology Act, 2000.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        PLEASE READ THE TERMS CAREFULLY BEFORE USE OF THE BUSINESS BEINGS
        SERVICES. THESE TERMS CONSTITUTE A LEGAL AND BINDING AGREEMENT BETWEEN
        YOU (THE END USER OF BUSINESS BEINGS) AND BUSINESS BEINGS. BY
        CHECKING/CLICKING ON THE `I AGREE` LINK AND/OR BY DOWNLOADING AND/OR
        INSTALLING AND/OR BROWSING OR USING BUSINESS BEINGS, YOU EXPRESSLY
        ACCEPT THE TERMS. IF YOU DO NOT ACCEPT TO THE TERMS, PLEASE DO NOT
        BROWSE AND/OR DOWNLOAD AND/OR INSTALL AND/OR USE BUSINESS BEINGS.
      </Typography >

      <Typography  className={classes.para} variant='subtitle1'>
        For the purpose of these Terms of Use, the terms &#8220;You&#8221;
        &#8220;Your&#8221; or &#8220;User&#8221; shall mean any person, natural
        or legal who uses or browses BUSINESS BEINGS. The term &#8220;We&#8221;,
        &#8220;Us&#8221;, &#8220;Our&#8221; shall mean BUSINESS BEINGS.
      </Typography >

      <Typography className={classes.heading} variant='h6' >
      Description of Service and Acceptance of Terms of Use
      </Typography >
      
      <Typography  className={classes.para} variant='subtitle1'>
        Business Beings provides an online website
        &#8216;www.businessbeings.com&#8217; as an interactive platform for its
        viewers and any associated mobile sites, applications. Business Beings
        may enable an interactive chat feature which may provide various
        features to the User(s) including but not limited to posting of
        comments, videos, pictures and participation in contests. The Site may
        also provide you content by integrating services and application of
        other audio video streaming platforms (“Partner Content”) and/or provide
        hosting service to third parties to display and allow access to their
        content and interactive games etc. (“Third Party Content”) (all such
        features of the Site collectively referred to as
        &#8220;Services&#8221;). &#8216;Content&#8217; will hence consist of
        Business Beings Content&#8217;, Partner Content and &#8216;Third Party
        Content&#8217;.
      </Typography >

      <Typography  className={classes.para} variant='subtitle1'>
        The Content will be made available to You through different modes, at
        the discretion of Business Beings, including via video on demand basis
        for viewing at the User&#8217;s discretion at a time chosen by You and
        download for offline viewing by You. In the case of downloadable
        Content, please note that: (a) the downloads are temporary in order to
        allow you to view the Content within a specified period of time and you
        shall not attempt to or actually make a permanent copy of the Content in
        any manner or form, and (b) not every Content may be available for
        download for offline viewing. Please note that the availability of, and
        Your ability to access, the Content or some part of Services, (a) is
        subject to Business Beings&#8217;s sole discretion and (b) may be
        dependent upon Your geographical location and (c) is on the condition
        that not all Content or Services will be available to all viewers. On
        account of the nature of the Internet, this Site and the Services may
        also be accessed in various geographical locations; and You hereby agree
        and acknowledge that You are accessing this Site and availing of the
        Services, at Your own risk, choice and initiative and You agree and
        undertake to ensure that Your use of the Site and the Services complies
        with all applicable laws including the local laws in Your jurisdiction.
        Further, such Services and Content may vary from place to place, time to
        time and device to device and would be subject to various parameters
        such as specifications, device, Internet availability and speed,
        bandwidth, etc. You agree and acknowledge that Business Beings may
        monitor or use certain technologies for monitoring of activities, as
        separately explained in Business Beings’s Privacy Policy. To facilitate
        Your viewership and access, the Services can be packaged by Business
        Beings through different models such as, Content or Services may be
        accessible (a) free of charge which may include advertisements or
        commercials or (b) via subscription through payment of a subscription
        fee as per the relevant subscription plan selected or (c) a pay-per-view
        model with or without advertisements/commercials or (d) with a
        combination of the foregoing on the Site.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        The Service can be availed through platforms, software and devices,
        which Business Beings approves from time to time, the current list for
        which is as set out below (&#8220;Compatible System&#8221;). In some
        cases, whether a device is (or remains) a Compatible System may depend
        on software or platform provided or maintained by the device
        manufacturer or other third parties. As a result, devices that are
        Compatible System at one time may cease to be Compatible System in the
        future.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>The Business Beings website works best with:</Typography >
      <ul>
        <li>Latest version of Google Chrome</li>
        <li>Latest version of Firefox</li>
        <li>Latest version of Safari</li>
        <li>Microsoft Windows XP or above / MAC OS 10.2 or above</li>
        <li>Latest version of Adobe Flash Player</li>
        <li>JavaScript and cookies enabled</li>
      </ul>
      <Typography  className={classes.para} variant='subtitle1'>
        Subject to compatibility, You may be able to access the Services through
        mobile phones, tablets and other IP based connected devices or any
        Compatible System offered by a partner authorized by Business Beings
        (&#8220;Partner&#8221;).To access the Services at each instance, you
        would be required to undergo a two-step verification process to verify
        your login credentials such as your phone number, by way of a one-time
        password which will be sent to you simultaneously.
      </Typography >

      <Typography className={classes.heading} variant='h6' >Accessing the Service through VPN</Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        You should not access or use the Service or the content therein by means
        of any mechanism or technology which conceals your actual geo-location
        or provides incorrect details of your location (for example, use a
        virtual private network (VPN)).
      </Typography >

      <Typography className={classes.heading} variant='h6' >Electronic Communications</Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        When You use / avail the Services or send any data, information or
        communication to Business Beings, You agree and understand that You are
        communicating with Business Beings through electronic media/ records and
        You consent to receive communications via electronic records from
        Business Beings periodically and as and when required. Business Beings
        will communicate with You by email or any push or other message or
        electronic records on the email address and or mobile number available
        with Business Beings or made available to Business Beings through a
        Partner which will be deemed adequate service of notice / electronic
        record.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        You acknowledge and specifically consent to Business Beings or our
        respective affiliates and partners contacting you using the contact
        information you have provided to us at any time during your association
        with us for any purpose including the following purposes, which will
        enable Business Beings to deliver the Services to you and/or enhance the
        delivery of the Services to you:
      </Typography >

      <ul>
        <li>To obtain feedback regarding the Service;</li>
        <li>
          To contact you for offering new products or services, whether offered
          by us, or our respective affiliates or partners.
        </li>
        <li>
          You acknowledge and agree that you may be contacted for the above
          purposes, using any of the contact details provided by You including
          via emails or other electronic and internet-based means.
        </li>
      </ul>
      <Typography  className={classes.para} variant='subtitle1'>
        In addition, you will be sent a one-time password via SMS to your
        registered mobile number, to verify your login on the Site using a
        mobile number.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        The Content contained in the Services including but not limited to on
        the Site is protected by copyright, trademark, patent, trade secret and
        other laws, and shall not be used except as provided in these Terms and
        Conditions, without the written permission of Business Beings.
      </Typography >

      <Typography className={classes.heading} variant='h6' >Subscription Services</Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Some of the Content made available on the Site can be availed only by
        registered Users who have registered under a subscription model
        (&#8220;Subscription User&#8221;). You may need to register Yourself on
        the Site to avail these services on a subscription model
        (&#8220;Subscription&#8221;). To become a Subscription User, You may be
        required to provide certain personal information to Business Beings and
        may be required to make payments (&#8220;Subscription Fee&#8221;) as per
        the applicable membership plan through a debit card, credit card,
        internet banking or through, e- prepaid wallet or other payment method
        accepted by Business Beings (&#8220;Payment Method(s)&#8221;) that You
        opted for during registration. Business Beings will be using third party
        payment gateways to process and facilitate the payment of Your
        Subscription Fee to Business Beings. For Business Beings’ Subscription
        proposed to be activated through Your registered mobile number, in
        addition to the above Payment Methods, cash on delivery payment option
        would be available to You, subject to the terms and conditions set out
        herein. On successful registration to the Subscription, Business Beings
        will intimate You of the same. Business Beings may offer different kinds
        of Subscription plans, each of these Subscription plans will be subject
        to different limitations and restrictions and the cost of each of these
        Subscription plans may vary. You may choose more than 1 Subscription
        plan during a given period.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Business Beings shall have the discretion to make certain Content that
        is a part of the Subscription available to You on more than one end user
        Compatible System concurrently. Business Beings shall also have the
        discretion to make certain Content that is a part of the Subscription
        available to You on not more than one end user device concurrently.
      </Typography >

      <Typography className={classes.heading} variant='h6' >Payments</Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Business Beings will automatically start billing as per Your selected
        Payment Method for the relevant Subscription plan(s) selected by You at
        the time of registration through the third party payment gateway at the
        end of the free trial period, if applicable unless You cancel a
        Subscription(s) before the end of the free trial period. The
        cancellation of a Subscription can be done through the &#8220;My
        Account&#8221; section of the Site. It may take a few days for Your
        payment made to Business Beings to be reflected in your &#8220;My
        Account&#8221; section.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        You cannot change your Payment Method during the tenure of your
        Subscription(s). If Your selected Payment Method is no longer available
        or expires or Your Payment Method fails for whatsoever reason, You will
        be responsible and liable for any uncollected amounts and Business
        Beings reserves the right to terminate the Subscription(s) offered to
        You.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Business Being will be automatically billing as per Your Payment Method
        for each Subscription period on a monthly basis. However, if You change
        Your membership plan, this could result in changing the day on which You
        are billed and the amount which You are billed. Further, on changing
        your existing Subscription plan to another, you would be required to pay
        the difference in rates (on a pro rata basis) for the new Subscription
        plan selected and would be billed as per the new Subscription plan from
        the next billing / renewal cycle. In case the rate for the new
        Subscription plan is less than that of the existing Subscription plan,
        the difference in rates would be adjusted (on a pro rata basis) and you
        will be billed accordingly from the next billing / renewal cycle.
        Business Being will notify you in advance of the change in the
        Subscription Fee payable.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        The new Subscription will be effective from the date on which you select
        such plan and make appropriate payments as necessary.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        In case of change in the Subscription Fee for a particular membership
        plan that is already opted by You, Business Beings will give You an
        advance notice of these changes.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        The Subscription Fees specified for each Subscription plan are inclusive
        of applicable taxes.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        The Subscription Fees billed are non-refundable irrespective of whether
        the Subscription have been availed by You or not.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Any request for change or cancellation in any Subscription plan prior to
        the expiration of the current Subscription plan period will not entail
        You with a refund for any portion of the Subscription Fee paid by You
        for the unexpired period.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        You also understand and acknowledge that Business Being only facilitates
        the third-party payment gateway for processing of payments. This
        facility is managed by the third-party payment gateway provider and You
        are required to follow all the terms and conditions of such third-party
        payment gateway providers.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        You are responsible for the accuracy and authenticity of the payment
        information provided by You, including the bank account number/credit
        card details and any other information requested during the subscription
        process. You represent and warrant that you have the right to use any
        credit card or other payment information that you submit. You agree and
        acknowledge that Business Being shall not be liable and in no way be
        held responsible for any losses whatsoever, whether direct, indirect,
        incidental or consequential, including without limitation any losses due
        to delay in processing of payment instruction or any credit card fraud.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        You can access the status of Your Subscription anytime through the
        &#8220;My Account&#8221; section on Your profile page.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        You can file any complaint related to payment processing on the Site and
        the same will be forwarded to the concerned third-party payment gateway
        provider for redressal.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Upon payment through any of the Payment Modes and confirmation of
        receipt of such payment from the payment gateway provider, an invoice
        will be made available to You in the &#8220;MY Account&#8221; section
        which can be downloaded by You in pdf format.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Upon payment being received by Business Being through any of the Payment
        Method, Business Being shall make the relevant Subscription available to
        You through any of the approved modes of delivery that Business Being
        adopts.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        It is hereby clarified that the Subscription(s) offered to You are
        offered by Business Being and not by any third party including any
        payment gateway service providers.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Business Being reserves the right to change, terminate or otherwise
        amend the Subscription plans, Subscription Fees for the Subscription and
        billing cycles at its sole discretion and at any time. Such amendments
        shall be effective upon posting on the Site and Your continued use of
        the Service shall be deemed to be Your conclusive acceptance of such
        amendments.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Business Being reserves the right to change, supplement, alter or remove
        any of the Content that is subject to a Subscription Fee as it deems
        fit. Business Being does not guarantee the availability of a specific
        Content or a minimum Content that is subject to a Subscription Fee.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Post cancellation of Your Subscription, if you wish to re-subscribe to
        the Subscription, you may do so from Your &#8220;My Account&#8221; page.
      </Typography >
      <Typography className={classes.heading} variant='h6' >Access and Use of the Services</Typography >
      <Typography  className={classes.para} variant='subtitle1'>Limited use:</Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        You are given a limited, non-exclusive, non-transferable (except in the
        case of a parent/legal guardian-minor relationship), non-sub-licensable,
        revocable permission to view the Site and avail the Service during the
        subsistence of Your Account with Business Beings for the territory of
        the world or limited territories as applicable in respect of specified
        Content and as set forth in these Terms and Conditions and no right,
        title or interest in any Content will be deemed transferred to You.
      </Typography >
      <Typography className={classes.heading} variant='h6' >By accessing or using the Services</Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        You confirm and warrant that all the data and information You supply to
        Business Beings and or any Business Being’s affiliates
        (&#8220;Registration Data&#8221;) is accurate in all respects.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        You agree to promptly update Your Registration Data as necessary, so
        that Your Registration Data remains accurate at all times.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Business Beings may temporarily suspend your access to the whole or any
        part of the Services, where Business Beings in its discretion considers
        that it is reasonably necessary. Where possible, Business Beings will
        give You as much notice of any interruption of access to the Services as
        is reasonably practicable. Business Beings will use reasonable efforts
        to restore access to the Services as soon as reasonably practicable
        after temporary suspension.
      </Typography >

      <ul>
        <li>
          You agree that Business Beings shall be under no liability whatsoever
          to You in the event of non-availability of the Site or any portion
          thereof occasioned by Act of God, war, disease, revolution, riot,
          civil commotion, strike, lockout, flood, fire, satellite failure,
          failure of any public utility, man-made disaster, satellite failure or
          any other cause whatsoever beyond the control of Business Beings.
        </li>
        <li>
          You acknowledge and agree not to either directly or through the use of
          any device, software, internet site, web-based service, or other means
          remove, alter, bypass, avoid, interfere with, violate, or circumvent
          any patent; trade secret; copyright, trademark, or other proprietary
          notice marked on the Content or any digital rights management
          mechanism or device; any content protection or access control measure
          associated with the Content, including geo-filtering mechanisms;
          privacy; publicity; or other proprietary right under applicable law.
        </li>
        <li>
          You agree not to either directly or through the use of any device,
          software, internet site, web-based service, or other means copy,
          download, capture, reproduce, duplicate, archive, distribute, upload,
          publish, modify, translate, broadcast, perform, display, sell,
          transmit or retransmit the Content or create any work or material that
          is derived from or based on the Content. This prohibition applies
          regardless of whether such derivative works or materials are sold,
          bartered, or given away.
        </li>
        <li>
          You agree that Business Beings and/or its affiliates or licensors owns
          and/or retains all rights to the Services and the Content throughout
          the territory of the world in perpetuity. You further agree that the
          Content You access and view as part of the Services is owned or
          controlled by Business Beings, its affiliates and or its licensors.
          The Services and the Content are protected by copyright, trademark,
          design, patent, trade secret, and other intellectual property laws.
        </li>
        <li>
          You agree that third parties and/or its affiliates or licensors owns
          and/or retains all rights to the Third Party Content. The Third Party
          Content is protected by copyright, trademark, design, patent, trade
          secret, and other intellectual property laws.
        </li>
        <li>
          Except as provided in these Terms (or otherwise expressly provided by
          Business Beings), You may not copy, download, stream capture,
          reproduce, duplicate, archive, upload, modify, translate, publish,
          broadcast, transmit, retransmit, distribute, perform, display, sell,
          frame or deep-link, make available, or otherwise use any Content
          contained in the Services. You further agree not to use the Services
          to:
        </li>
      </ul>
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Post, upload or otherwise transmit or link to Content that is:
        unlawful; threatening; abusive; obscene; vulgar; sexually explicit;
        pornographic or inclusive of nudity; offensive; excessively violent;
        invasive of another&#8217;s privacy, publicity, contract or other
        rights; tortious; false or misleading; defamatory; libelous; hateful; or
        discriminatory;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>&#8211; Harass or harm another person;</Typography >
      <Typography  className={classes.para} variant='subtitle1'>&#8211; Exploit or endanger a minor;</Typography >
      <Typography  className={classes.para} variant='subtitle1'>&#8211; Impersonate or attempt to impersonate any person or entity;</Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Violate any patent, trademark, trade secret, copyright, privacy,
        publicity or other proprietary right;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Introduce or engage in activity that involves the use of
        viruses, software disabling codes, bots, worms, or any other computer
        code, files or programs that interrupt, destroy, or limit the
        functionality of any computer software or hardware or telecommunications
        equipment, or otherwise permit the unauthorized use of or access to a
        computer or a computer network;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Attempt to decipher, decompile, disassemble or reverse engineer
        any of the software, applications and/or any element comprising the
        Site, the application for accessing the Content, or the Services;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Interfere with, damage, disable, disrupt, impair, create an
        undue burden on, or gain unauthorized access to the Services, including
        Business Beings&#8217;s servers, networks or accounts;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Cover, remove, disable, manipulate, block or obscure
        advertisements or other portions of the Services or the Content;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Delete or revise any information provided by or pertaining to
        any other user of the Services.
      </Typography >
      <ul>
        <li>
          Use technology or any automated system such as scripts, spiders,
          offline readers or bots in order to collect or disseminate usernames,
          passwords, email addresses or other data from the Services, or to
          circumvent, delete, deactivate, decompile, reverse engineer,
          disassemble or modify any security technology or software that is part
          of the Services or the Site;
        </li>
      </ul>
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Send or cause to send (directly or indirectly) unsolicited bulk
        messages or other unsolicited bulk communications of any kind through
        the Services. If You do so, You acknowledge You will have caused
        substantial harm to Business Beings, but that the amount of harm would
        be extremely difficult to measure. As a reasonable estimation of such
        harm, and by way of liquidated damages and not as a penalty, You agree
        to pay Company INR 5000 (Indian rupees five thousand only) for each
        actual or intended recipient of such communication without prejudice to
        any other rights of action that Business Beings may have against You in
        law or equity;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Solicit, collect or request any personal information for
        commercial or unlawful purposes;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Post, upload or otherwise transmit an image or video of another
        person without that person&#8217;s consent;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Engage in commercial activity (including but not limited to
        advertisements or solicitations of business; sales; contests;
        sweepstakes; creating, recreating, distributing or advertising an index
        of any significant portion of the Content; or building a business using
        the Content) without Business Beings&#8217;s prior written consent;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Use technology or other means to access, index, frame, or link
        to the Site (including the Content) that is not authorized by Business
        Beings;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Access the Site/application/Service (including the Content)
        through any automated means, including &#8220;robots,&#8221;
        &#8220;spiders,&#8221; or &#8220;offline readers&#8221; (other than by
        individually performed searches on publicly accessible search engines
        for the sole purpose of, and solely to the extent necessary for,
        creating publicly available search indices – but not caches or archives
        – of the Site and excluding those search engines or indices that host,
        promote, or link primarily to infringing or unauthorized content);
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Use the Services to advertise or promote competing services;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Use the Services in a manner inconsistent with any and all
        applicable law;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        &#8211; Attempt, facilitate, induce, aid and abet, or encourage others
        to do any of the foregoing.
      </Typography >
      <ul>
        <li>
          You are responsible for the security and proper use of Your unique
          user ID and password that You enter while registering on the Site or
          on the Partner&#8217;s website or application and must take all
          necessary steps to ensure that they are kept confidential, used
          properly, and not disclosed to unauthorized people. You are
          responsible for any use of your user ID and password. You must
          promptly notify Business Beings if you become aware of any
          confidentiality breach or unauthorized use of your user account..
        </li>
        <li>
          You agree that Your use of the Services and the Content may give You
          the opportunity to view, publish, transmit, submit posts and/or
          comment in accordance with these Terms and Conditions. You agree that
          You will not misuse the Content and/or other user material You access
          and view as part of the Service, including without limitation,
          download/store the Content by illegal/non-permitted means, or infringe
          any of Business Beings&#8217;s/ or its licensor&#8217;s / or third
          party’s copyright, trademark design, patent, and other intellectual
          property including any rights of Business Beings or its licensors or
          any third party subsisting in the Content. You further agree that You
          will not copy/record/edit/modify any of the Content You access and
          view as part of the Service, for any purpose, and/or
          copy/record/edit/modify any Content including video, images etc., by
          using any means including software, spyware, etc., for any purpose
          whatsoever.
        </li>
        <li>
          You further agree that You shall not post, publish, transfer, upload,
          transmit or submit any of the Content, including without limitation
          videos, images, comments, User Material (defined below) or articles,
          of the Site on any other website, webpage or software, whatsoever.
        </li>
        <li>
          You agree that the Content provided on the Site is strictly for Your
          private viewing only and not for public exhibition irrespective of
          whether the public is charged for such exhibition or not. You hereby
          agree to refrain from further broadcasting or making available the
          Content provided on the Site to any other person(s), or in any manner
          communicate such Content to the public.
        </li>
      </ul>
      <Typography  className={classes.para} variant='subtitle1'>
        In case the Site allows You to download or stream any of the Content
        therein, You agree that You will not use, sell, transfer or transmit the
        Content to any person or upload the Content on any other website,
        webpage or software, which may violate/cause damages or injuries to any
        rights of Business Beings or its affiliates or any third party including
        privacy rights, publicity rights, and intellectual property rights.
      </Typography >
     
      <Typography  className={classes.para} variant='subtitle1'>
        You may encounter third party applications while using the Site
        including, without limitation, websites, widgets, software, services
        that interact with the Site. Your use of these third-party applications
        shall be subject to such third-party terms of use or license terms.
        Business Beings shall not be liable for any representations or
        warranties or obligations made by such third-party applications to You
        under contract or law.
      </Typography >
      <Typography className={classes.heading} variant='h6' >Termination of Service</Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Business Beings reserves the right to immediately terminate, suspend,
        limit, or restrict Your account or Your use of the Services or access to
        Content at any time, without notice or liability, if Business Beings so
        determines in its sole discretion, for any reason whatsoever, including
        that You have breached these Terms and Conditions, the Privacy Policy,
        violated any law, rule, or regulation, engaged in any inappropriate
        conduct, provided false or inaccurate information, or for any other
        reason. You hereby agree and consent to the above and agree and
        acknowledge that Business Beings can, at its sole discretion, exercise
        its right in relation to any or all of the above, and that Business
        Beings, its directors, officers, employees, affiliates, agents,
        contractors, principals or licensors shall not be liable in any manner
        for the same; and You hereby agree, acknowledge and consent to the same.
        You hereby also agree that Business Beings shall not refund any amounts
        that you may have paid to access and/or use the Services.
      </Typography >
      <Typography className={classes.heading} variant='h6'  c>User Reviews, Comments and Other Material</Typography >

      <Typography  className={classes.para} variant='subtitle1'>
        You may have an opportunity to publish, transmit, submit, or otherwise
        post (collectively, &#8220;Post&#8221;) only reviews or comments or
        photos or videos on the Site (collectively, &#8220;User
        Material&#8221;). As it concerns User Material, without prejudice to
        Your obligation to otherwise comply with applicable laws during the
        course of using the Services, You agree to hereby comply with any and
        all applicable laws, as well as any other rules and restrictions that
        may be set forth herein or on the Site or Services.
      </Typography >

      <Typography  className={classes.para} variant='subtitle1'>
        You agree that Business Beings shall have the right but have no
        obligation, to monitor User Material and to restrict or remove User
        Material that Business Beings may determine, in its sole discretion, is
        inappropriate or for any other reason. You acknowledge that Business
        Beings reserves the right to investigate and take appropriate legal
        action against anyone who, in Business Beings&#8217;s sole discretion,
        violates these Terms, including, but not limited to, terminating their
        account, terminate, suspend, limit, or use of the Services or access to
        Content and/or reporting such User Material, conduct, or activity, to
        law enforcement authorities, in addition to any other available remedies
        under law or equity. In addition to the restrictions set forth above,
        You must adhere to the below terms and conditions and not host, display,
        upload, modify, publish, transmit, update or share any information
        /material/User Material that:
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        (a) belongs to another person and to which You do not have any right to;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        (b) is grossly harmful, harassing, blasphemous, defamatory, obscene,
        pornographic, pedophilic, libelous, invasive of another&#8217;s privacy,
        hateful, or racially, ethnically objectionable, disparaging, relating or
        encouraging money laundering or gambling, or otherwise unlawful in any
        manner whatsoever;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>(c) harms minors in any way;</Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        (d) infringes any patent, trademark, design, copyright or other
        proprietary rights;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>(e) violates any law for the time being in force;</Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        (f) deceives or misleads the addressee about the origin of such messages
        or communicates any information which is grossly offensive or menacing
        in nature;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>(g) impersonates another person;</Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        (h) contains software viruses or any other computer code, files or
        programs designed to interrupt, destroy or limit the functionality of
        any computer resource;
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        (i) threatens the unity, integrity, defense, security or sovereignty of
        India, friendly relations with foreign states, or public order or causes
        incitement to the commission of any cognizable offence or prevents
        investigation of any offence or is insulting any other nation. You also
        represent and warrant that the Posting of Your User Material does not
        violate any right of any party, including privacy rights, publicity
        rights, and intellectual property rights.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        You acknowledge that the User Material is not confidential and that You
        have no expectation of privacy in it.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        In no event does Business Beings assume any responsibility or liability
        whatsoever for any User Material, and You agree to waive any legal or
        equitable rights or remedies You may have against Business Beings with
        respect to such User Material.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        This Site may contain articles contributed by several individuals. The
        views are exclusively their own and do not represent the views of
        Business Beings, affiliates and/ or its management. All liability in
        respect of the above is excluded to the extent permitted by law
        including any implied terms. Indian law and jurisdiction apply with
        respect to the contents of this Site.
      </Typography >

      <Typography className={classes.heading} variant='h6' >Linked Destinations</Typography >

      <Typography  className={classes.para} variant='subtitle1'>
        Part of this Site/Service contains links to third party sites and
        third-party content. You agree and acknowledge that Business Beings does
        not endorse or sponsor such third-party sites, content, advertising or
        other material on such third-party sites.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        If Business Beings provides links or pointers to other websites or
        destinations, You should not infer or assume that Business Beings
        operates, controls, or is otherwise connected with these other websites
        or destinations. When You click on a link within the Services, Business
        Beings will not warn You that You have left the Services and are subject
        to the terms and conditions (including privacy policies) of another
        website or destination. In some cases, it may be less obvious than
        others that You have left the Services and reached another website or
        destination. Please be careful to read the terms of use and privacy
        policy of any other website or destination before You provide any
        confidential information or engage in any transactions. You should not
        rely on these Terms and Conditions and/or Privacy Policy to govern Your
        use of another website or destination.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Business Beings is not responsible for the content or practices of any
        website or destination other than the Site, even if it links to the Site
        and even if the website or destination is operated by a company
        affiliated or otherwise connected with Business Beings. By using the
        Services, You acknowledge and agree that Business Beings is not
        responsible or liable to You for any content or other materials hosted
        and served from any website or destination other than the Site.
      </Typography >

      <Typography className={classes.heading} variant='h6' >Advertising</Typography >

      <Typography  className={classes.para} variant='subtitle1'>
        Business Beings expressly disclaim any liability arising out of the
        advertisements, usage or viewing of these products or services
        advertised on our Site or the (third party) content made available /
        hosted on the third-party sites.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Business Beings takes no responsibility for advertisements or any
        third-party material posted on the Site nor does it take any
        responsibility for the products or services provided by advertisers. Any
        dealings You have with advertisers found while using the Services are
        between You and the advertiser, and You agree that Business Beings is
        not liable for any loss or claim that You may have against an
        advertiser.
      </Typography >

      <Typography className={classes.heading} variant='h6' >Third Party Content</Typography >

      <Typography  className={classes.para} variant='subtitle1'>
        For Third Party Content, Business Being’s role is limited to providing a
        communication platform along with hosting services to third parties, to
        enable the transmission of the Third Party Content directly from third
        parties to You. The Third Party Content on the Site is directly uploaded
        onto the Site by third parties who avail of Business Beings’s hosting
        services, without any intervention from Business Beings in the uploading
        / transmission process. Business Beings’s role is that of an
        ‘intermediary’ as defined under the Information Technology Act, 2000 and
        the rules thereunder, with regard to the Third Party Content. Being an
        intermediary, Business Beings has no responsibility and / or liability
        in respect of any Third Party Content on the Site, including for
        intellectual property rights infringement, defamation, obscenity or any
        other violation under applicable law.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Third Party Content on the Site may include audio/video content and/or
        interactive content such as free-to-play games of skill that are
        casual/hyper-casual online games, developed or owned by the third
        parties (&#8220;Online Games&#8221;).The Online Games will be run and
        made available over the Site and do not have to be separately downloaded
        or installed.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Business Beings does not endorse, market, advertise or publicize any
        Third Party Content on the Site and is not responsible or liable for any
        Third Party Content.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Business Beings does not pre-screen the Third Party Content and has no
        obligation to monitor any Third Party Content. Hence, Business Beings
        does not have actual or specific knowledge of any Third Party Content on
        the Site. However, Business Beings at its discretion and in accordance
        with applicable law may monitor any Third Party Content and may remove
        any Third Party Content from the Site if Business Beings determines in
        its sole discretion that such Third Party Content is in violation of
        this Agreement or any applicable law. Business Beings, at its
        discretion, may review the Third Party Content when, and only when,
        complaints are received from You. Such actions do not in any manner
        negate or dilute Business Beings’s position as an intermediary or impose
        any liability on Business Beings with respect to Third Party Content.
      </Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        Business Beings will consider all communications, requests and
        suggestions sent by You and other members of the public provided that
        such communications are sent in a bona fide manner in good faith in the
        interest of the Site and public good. However, Business Beings is not
        under any obligation to act on any such communications, requests and
        suggestions or respond to anyone. Business Beings’s decision in this
        respect shall be final. Specifically, if any such request relates to a
        request / demand to take down/ disable/ remove/ delete any Third Party
        Content in the Site, Business Beings is under no legal obligation to
        respond to or act on such requests. Business Beings will take such
        action as Business Beings is required to take under applicable law. If
        there is any valid court order or administrative order issued requiring
        Business Beings to take any action, then Business Beings will comply
        with such court order or administrative order.
      </Typography >

      <Typography className={classes.heading} variant='h6' >Limitation of Liability</Typography >

      <Typography  className={classes.para} variant='subtitle1'>
        IN NO CASE SHALL BUSINESS BEINGS, ITS DIRECTORS, OFFICERS, EMPLOYEES,
        AFFILIATES, AGENTS, CONTRACTORS, PRINCIPALS, OR LICENSORS BE LIABLE FOR
        ANY DIRECT, INDIRECT, INCIDENTAL, PUNITIVE, SPECIAL, OR CONSEQUENTIAL
        DAMAGES ARISING FROM YOUR USE OF THE SERVICES OR ANY CONTENT OR MATERIAL
        THEREOF OR FOR ANY OTHER CLAIM RELATED IN ANY WAY TO YOUR ACCESS OF THE
        SITE OR USE OF THE SERVICES OR ANY CONTENT OR MATERIAL THEREOF,
        INCLUDING, BUT NOT LIMITED TO, ANY ERRORS OR OMISSIONS IN ANY CONTENT OR
        MATERIAL, OR ANY LOSS OR DAMAGE OF ANY KIND INCURRED AS A RESULT OF THE
        USE OF THE SERVICES OR ANY CONTENT OR MATERIAL THEREOF, EVEN IF ADVISED
        OF THEIR POSSIBILITY. BUSINESS BEINGS’S MONETARY LIABILITY FOR THE
        SERVICES OR THE CONTENT OR MATERIAL THEREOF ON THE SITE SHALL BE LIMITED
        TO INR 1000. WHERE THE LAWS DO NOT ALLOW THE EXCLUSION OR THE LIMITATION
        OF LIABILITY FOR CONSEQUENTIAL OR INCIDENTAL DAMAGES, IN SUCH
        JURISDICTIONS, Business Beings&#8217;S LIABILITY SHALL BE LIMITED TO THE
        EXTENT PERMITTED BY LAW.
      </Typography >

      <Typography className={classes.heading} variant='h6' >Indemnity</Typography >

      <Typography  className={classes.para} variant='subtitle1'>
        You agree to defend, indemnify and hold harmless BUSINESS BEINGS, its
        affiliates, officers, directors, employees and agents, from and against
        any and all claims, damages, obligations, losses, liabilities, costs or
        debt, and expenses (including but not limited to attorneys&#8217; fees)
        arising from: (i) Your use of and access to the Services; (ii) Your
        violation of any term of these Terms and Conditions; (iii) Your
        violation of any third party right, including without limitation any
        publicity, privacy, or intellectual property right; (iv) Your breach of
        any applicable laws; and (v) any unauthorized, improper, illegal or
        wrongful use of Your Account by any person, including a third party,
        whether or not authorized or permitted by You. This indemnification
        obligation will survive the expiry or termination of these Terms and
        Conditions and Your use of the Service.
      </Typography >

      <Typography className={classes.heading} variant='h6' >Governing Law and Jurisdiction</Typography >

      <Typography  className={classes.para} variant='subtitle1'>
        The relationship between You and BUSINESS BEINGS shall be governed by
        the laws of India without regard to its conflict of law provisions and
        for resolution of any dispute arising out of Your use of the Services.
        Notwithstanding the foregoing, You agree that (i) BUSINESS BEINGS has
        the right to bring any proceedings before any court/forum of competent
        jurisdiction and You irrevocably submit to the jurisdiction of such
        courts or forum; and (ii) any proceeding brought by You shall be
        exclusively before the courts at Mumbai, India.
      </Typography >
      <Typography className={classes.heading} variant='h6' >Severability</Typography >
      <Typography  className={classes.para} variant='subtitle1'>
        If any provision of these Terms and Conditions is held invalid, void, or
        unenforceable, then that provision shall be considered severable from
        the remaining provisions, and the remaining provisions given full force
        and effect.
      </Typography >

      <Typography className={classes.heading} variant='h6' >Changes</Typography >

      <Typography  className={classes.para} variant='subtitle1'>
        BUSINESS BEINGS reserves the right, at any time and from time to time,
        without prior notice to You, to update, revise, supplement, and
        otherwise modify these Terms of Use and to impose new or additional
        rules, policies, terms, or conditions on Your use of the Service. Any
        updates, revisions, supplements, modifications, and additional rules,
        policies, terms, and conditions (collectively referred to in this Terms
        of Use as &#8220;Revised Terms&#8221;) will be posted on the Site and
        will be effective immediately after such posting. We recommend that you
        periodically check the Site for the Revised Terms Your continued use of
        the Services will be deemed to constitute Your acceptance of any and all
        such Revised Terms.
      </Typography >

      <Typography className={classes.heading} variant='h6' >Survival</Typography >

      <Typography  className={classes.para} variant='subtitle1'>
        You acknowledge that Your representations, undertakings, and warranties
        and the clauses relating to indemnities, limitation of liability, grant
        of license, governing law, confidentiality shall survive the efflux of
        time and the termination of these Terms and Conditions.
      </Typography >

      <Typography className={classes.heading} variant='h6' >Entire Agreement</Typography >

      <Typography  className={classes.para} variant='subtitle1'>
        These Terms and Conditions and Privacy Policy constitute the entire
        agreement between You and BUSINESS BEINGS governing Your use of the
        Services, superseding any prior agreements between You and BUSINESS
        BEINGS regarding such use. Further, Your acceptance of these Terms
        constitutes a valid and binding agreement between You and Business
        Beings. Should these Terms require to be stamped under any applicable
        stamp duty laws, You should bear such stamp duty payable and comply with
        relevant stamping obligations.
      </Typography >
    </div>
  </>
  );
}

export default Terms;
